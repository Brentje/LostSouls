﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/Water" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap1("Normal1 (RGB)", 2D) = "bump" {}
		_BumpMap2("Normal2 (RGB)", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_ScrollX1("Scroll X1", Range(-1, 1)) = 0.0
		_ScrollY1("Scroll Y1", Range(-1, 1)) = 0.0
		_ScrollX2("Scroll X2", Range(-1, 1)) = 0.0
		_ScrollY2("Scroll Y2", Range(-1, 1)) = 0.0
		_Amplitude("Amplitude", Range(0, 5)) = 0.0
		_Speed("Speed", Range(0, 5)) = 0.0
		_Frequency("Frequency", Range(0, 5)) = 0.0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha vertex:vert
		#pragma target 4.0

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap1;
			float2 uv_BumpMap2;
		};
		
		sampler2D _MainTex;
		sampler2D _BumpMap1;
		sampler2D _BumpMap2;

		fixed _ScrollX1;
		fixed _ScrollY1;
		fixed _ScrollX2;
		fixed _ScrollY2;

		float _Amplitude;
		float _Speed;
		float _Frequency;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_full v) {
			half offsetvert = v.vertex.x + v.vertex.y;

			half valuesin = _Amplitude * sin(_Time.w * _Speed + offsetvert * _Frequency);
			
			v.vertex.z += valuesin;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed2 scrolledUV1 = IN.uv_BumpMap1;
			fixed2 scrolledUV2 = IN.uv_BumpMap2;

			fixed X1 = _ScrollX1 * _Time;
			fixed Y1 = _ScrollY1 * _Time;

			fixed X2 = _ScrollX2 * _Time;
			fixed Y2 = _ScrollY2 * _Time;

			scrolledUV1 += fixed2(X1, Y1);
			scrolledUV2 += fixed2(X2, Y2);

			fixed3 n1 = UnpackNormal(tex2D(_BumpMap1, scrolledUV1));
			fixed3 n2 = UnpackNormal(tex2D(_BumpMap2, scrolledUV2));

			fixed3 n = fixed3(n1.r + n2.r, n1.g + n2.g, n1.b);

			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			o.Normal = n;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
