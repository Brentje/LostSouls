﻿using StateMachine;

public class BigBossStageOneDiagramm : StateDiagram<BigBoss>
{
    public override void Init(StateMachine<BigBoss> stateMachine)
    {
        StateMachine = stateMachine;

        LayEggsState layEgg = new LayEggsState(stateMachine);
        layEgg.TargetInAttackRange += () => StateMachine.SetState(1);

        AttackState<BigBoss> attack = new AttackState<BigBoss>(stateMachine, new AttackAbility(stateMachine.Holder, "BasicAttack", ActorManager.GetActivePlayer()));
        attack.OnTargetOutOfRange += () => stateMachine.SetState(0);

        States.Add(0, layEgg);
        States.Add(1, attack);

        stateMachine.SetState(0);
    }
}
