﻿using StateMachine;

/// <summary>
/// Sit idle and lay some eggs
/// </summary>
public class LayEggsState : IdleState<BigBoss>
{
    public LayEggsState(StateMachine<BigBoss> fsm) : base(fsm)
    {

    }

    public override void Update()
    {
        base.Update();
        LayEgg();
    }

    private void LayEgg()
    {
        Holder.LayEgg();
    }
}
