﻿using StateMachine;
using UnityEngine;

/// <summary>
/// Class for the final boss at the spider den
/// </summary>
public class BigBoss : BaseEnemy<BigBoss>
{
    // Bosses current stage
    public int currentStage;
    // This keeps track of when it needs to go to the next stage
    public int spawnedEggs;
    // Specifies the duration of the third stage
    public float StagethreeDuration;

    // Prefab for the eggs
    public GameObject eggPrefab;
    // The locations where the eggs can spawn
    [Tooltip("Locations from where the eggs spawn (within a range)")]
    public Transform[] spawnLocations;

    public GameObject PortalBack;

    // The different stages (stage 1, 2 and 3)
    private readonly StateDiagram<BigBoss>[] diagrams =
    {
        new BigBossStageOneDiagramm(),
        new BigBossStageTwoDiagramm(),
        new BigBossStagethreeDiagramm()
    };

    // The time that the current stage started
    private float startOfStage;
    [SerializeField] private int maxEggs = 5;

    // Set the square attack range to 16 and the square detect range to 100
    public BigBoss() : base(4 * 4, 10 * 10)
    {
    }

    protected override void Awake()
    {
        base.Awake();
        // Create an instance of a statemachine with the first stage as diagram
        StateMachine = new StateMachine<BigBoss>(this) { Diagram = diagrams[0] };

        // Init all diagrams
        for (int i = 0; i < diagrams.Length; i++)
            diagrams[i].Init(StateMachine);

        // Set the player as target
        SetTarget(ActorManager.GetActivePlayer());

        OnDeath += () => { PortalBack.SetActive(true); };
    }

    protected override void Start()
    {
        base.Start();
        // Set health to 1000
        Stats[StatType.Health].MaxValue = 1000;
        Stats[StatType.Health].SetValueWithoutTrigger(1000);
    }

    protected override void Update()
    {
        // Check if the statemachine needs to go to the next stage

        base.Update();

        if (SmallSpider.killedSpiders > maxEggs && currentStage == 1)
            GoToStage(2);

        if (currentStage == 2)
            if (Time.time - startOfStage > StagethreeDuration)
                GoToStage(0);
    }

    /// <summary>
    /// Go to the given stage
    /// </summary>
    public void GoToStage(int stage)
    {
        if (stage >= 0 && stage < diagrams.Length)
        {
            StateMachine.Diagram = diagrams[stage];
            currentStage = stage;
            startOfStage = Time.time;
            switch (stage)
            {
                case 0:
                {
                    spawnedEggs = 0;
                    break;
                }
                case 1:
                {
                    Target.AddStatusEffect(new SlownessEffect(Target, -5, 3));
                    break;
                }
                case 2:
                {
                    SmallSpider.killedSpiders = 0;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Lay an egg somewhere in the scene
    /// </summary>
    public void LayEgg()
    {
        if (spawnLocations.Length > 0)
        {
            Vector3 spawnlocation =
                GetRandomPointWithinRange(spawnLocations[Random.Range(0, spawnLocations.Length)].position, 3);
            Instantiate(eggPrefab, spawnlocation, Quaternion.identity);
            spawnedEggs++;
            if (spawnedEggs > maxEggs)
                GoToStage(1);
        }
    }
}