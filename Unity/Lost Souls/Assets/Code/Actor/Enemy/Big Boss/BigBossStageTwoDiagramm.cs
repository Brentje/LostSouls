﻿using StateMachine;

public class BigBossStageTwoDiagramm : StateDiagram<BigBoss>
{
    private AttackState<BigBoss> attackState;

    public override void Init(StateMachine<BigBoss> stateMachine)
    {
        StateMachine = stateMachine;

        MoveToTargetState<BigBoss> moveToTarget = new MoveToTargetState<BigBoss>(stateMachine);
        moveToTarget.OnTargetInAttackRange += () => stateMachine.SetState(1);

        attackState = new AttackState<BigBoss>(stateMachine, new AttackAbility(stateMachine.Holder, "BasicAttack", ActorManager.GetActivePlayer()));
        attackState.OnTargetOutOfRange += () => stateMachine.SetState(0);
        
        States.Add(0, moveToTarget);
        States.Add(1, attackState);

        stateMachine.SetState(0);
    }

    public override void Update()
    {
        attackState.Update();
    }
}
