﻿using StateMachine;

public class BigBossStagethreeDiagramm : StateDiagram<BigBoss>
{
    public override void Init(StateMachine<BigBoss> stateMachine)
    {
        StateMachine = stateMachine;

        IdleState<BigBoss> idle = new IdleState<BigBoss>(stateMachine);
        idle.TargetInDetectRange += () => StateMachine.SetState(1);

        MoveToTargetState<BigBoss> moveToTarget = new MoveToTargetState<BigBoss>(stateMachine);
        moveToTarget.OnTargetOutOfRange += () => stateMachine.SetState(0);
        moveToTarget.OnTargetInAttackRange += () => stateMachine.SetState(2);

        AttackState<BigBoss> attack = new AttackState<BigBoss>(stateMachine, new AttackAbility(stateMachine.Holder, "BasicAttack", ActorManager.GetActivePlayer()));
        attack.OnTargetOutOfRange += () => stateMachine.SetState(1);

        States.Add(0, idle);
        States.Add(1, moveToTarget);
        States.Add(2, attack);

        stateMachine.SetState(0);
    }
}
