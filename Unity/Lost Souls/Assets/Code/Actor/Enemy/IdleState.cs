﻿using StateMachine;

/// <summary>
/// Idle State for the state machine
/// </summary>
public class IdleState<TActor> : State<TActor> where TActor : BaseEnemy<TActor>
{
    public IdleState(StateMachine<TActor> fsm) : base(fsm) { }

    public event VoidDelegate TargetInDetectRange;
    public event VoidDelegate TargetInAttackRange;

    public override void Update()
    {
        // check if the target is within range
        if (Holder.Target != null)
        {
            float distn = (Fsm.Holder.Target.transform.position - Fsm.Holder.transform.position).sqrMagnitude;
            if (Fsm.Holder.Target && distn < Holder.DetectRangeSqr)
            {
                TargetInDetectRange?.Invoke();
            }
            else if (Fsm.Holder.Target && distn < Holder.AttackRangeSqr)
            {
                TargetInAttackRange?.Invoke();
            }
        }
    }
}
