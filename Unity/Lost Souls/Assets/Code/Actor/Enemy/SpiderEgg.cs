﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Hatches the egg when the target comes close
/// </summary>
public class SpiderEgg : MonoBehaviour
{
    private bool hasSpawned;
    public GameObject Prefab;

    public float Range;

    private Transform target;

    private float timer;

    private void Start()
    {
        target = ActorManager.GetActivePlayer().transform;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > 1)
        {
            if (!hasSpawned)
            {
                if ((transform.position - target.position).sqrMagnitude < Range * Range)
                    Hatch();
            }
            else
            {
                Prefab.GetComponent<NavMeshAgent>().Warp(Actor.GetRandomPointWithinRange(transform.position, 1));
                enabled = false;
            }
            timer -= 1;
            DebugUtil.DrawCircle(transform.position, Vector3.up, Vector3.forward, Range, (int) Range * 2, Color.blue,
                1);
        }
    }

    [ContextMenu("Hatch")]
    public void Hatch()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = transform.GetChild(i).gameObject;
            child.SetActive(!child.activeSelf);
        }
        Prefab = Instantiate(Prefab, transform.GetChild(0).position, transform.rotation);
        Destroy(GetComponent<NavMeshObstacle>());
        hasSpawned = true;
        timer = 10;
        Destroy(gameObject, 5);
    }
}