﻿using StateMachine;
using UnityEngine;

public class MediumSpiderDiagram : StateDiagram<MediumSpider>
{
    public override void Init(StateMachine<MediumSpider> stateMachine)
    {
        StateMachine = stateMachine;

        IdleState<MediumSpider> idle = new IdleState<MediumSpider>(stateMachine);
        idle.TargetInDetectRange += () => StateMachine.SetState(1);

        MoveToTargetState<MediumSpider> moveToTarget = new MoveToTargetState<MediumSpider>(stateMachine);
        moveToTarget.OnTargetOutOfRange += () => stateMachine.SetState(0);
        moveToTarget.OnTargetInAttackRange += () => stateMachine.SetState(2);

        AttackState<MediumSpider> attack = new AttackState<MediumSpider>(stateMachine, new AttackAbility(stateMachine.Holder, "BasicAttack", ActorManager.GetActivePlayer()));
        attack.OnTargetOutOfRange += () => stateMachine.SetState(1);

        States.Add(0, idle);
        States.Add(1, moveToTarget);
        States.Add(2, attack);

        stateMachine.SetState(0);
    }
}