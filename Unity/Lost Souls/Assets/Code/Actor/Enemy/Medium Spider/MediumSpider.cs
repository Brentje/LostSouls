﻿using StateMachine;

public class MediumSpider : BaseEnemy<MediumSpider>
{
    protected override void Awake()
    {
        base.Awake();
        StateMachine = new StateMachine<MediumSpider>(this) { Diagram = new MediumSpiderDiagram() };
        StateMachine.Diagram.Init(StateMachine);
    }
}
