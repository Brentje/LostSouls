﻿using StateMachine;

/// <summary>
///     Base class for the enemies
/// </summary>
/// <typeparam name="TEnemyType">
///     This one might be confusing, but it is so the states know what they control. example:
///     SmallSpider : baseEnemy^SmallSpider^
/// </typeparam>
public abstract class BaseEnemy<TEnemyType> : Actor where TEnemyType : BaseEnemy<TEnemyType>
{
    public readonly float AttackRangeSqr = 4 * 4;
    public readonly float DetectRangeSqr = 8 * 8;

    private GameManager manager;

    protected StateMachine<TEnemyType> StateMachine;

    protected BaseEnemy()
    {
    }

    protected BaseEnemy(float attackRangeSqr, float detectRangeSqr)
    {
        AttackRangeSqr = attackRangeSqr;
        DetectRangeSqr = detectRangeSqr;
    }

    public Actor Target { get; private set; }

    protected virtual void Start()
    {
        Target = ActorManager.GetActivePlayer();
        manager = GameManager.Instance;
        OnDeath += () => Destroy(gameObject);
    }

    protected override void Update()
    {
        // don't update when the game is paused
        base.Update();
        if (!manager.Paused)
            StateMachine?.Update();
    }

    protected void SetTarget(Actor actor)
    {
        Target = actor;
    }
}