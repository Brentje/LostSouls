﻿using StateMachine;

/// <summary>
/// In this state the holder attacks the target
/// </summary>
public class AttackState<TActor> : State<TActor> where TActor : BaseEnemy<TActor>
{
    public event VoidDelegate OnTargetOutOfRange;

    private AttackAbility attack;

    public AttackState(StateMachine<TActor> fsm, AttackAbility ability) : base(fsm)
    {
        attack = ability;
        attack.IsToggle = true;
    }

    public override void OnEnter()
    {
        // Set the target on the behaviour tree
        attack.SetActor("Target", ActorManager.GetActivePlayer());
        // Start Attacking
        attack.CastAbility();
    }

    public override void Update()
    {
        // Update the attack
        attack.RunAbilityBehaviour();

        // Calculate if the target is still within range
        float sqrDistance = (Fsm.Holder.Target.transform.position - Fsm.Holder.transform.position).sqrMagnitude;

        if (sqrDistance > Holder.AttackRangeSqr)
        {
            OnTargetOutOfRange?.Invoke();
        }
    }

    public override void OnExit()
    {
        // stop casting
        attack.CastAbility();
    }
}
