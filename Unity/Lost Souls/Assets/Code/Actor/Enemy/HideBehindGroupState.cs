﻿using StateMachine;
using UnityEngine;

public class HideBehindGroupState<TActor> : State<TActor> where TActor : GroupedEnemy<TActor>
{
    public HideBehindGroupState(StateMachine<TActor> fsm) : base(fsm)
    {

    }

    // This state does not heal and never exits this state

    public override void OnEnter()
    {
        // Hide behind the group in order to heal a little
        Vector3 target = (Holder.Group.Position - ActorManager.GetActivePlayer().transform.position).normalized;
        Holder.MoveTowards(Actor.GetRandomPointWithinRange(target * 10 + Holder.Group.Position, 3));
    }
}