﻿using StateMachine;

/// <summary>
/// Moves the actor to the target
/// </summary>
public class MoveToTargetState<TActor> : State<TActor> where TActor : BaseEnemy<TActor>
{
    public event VoidDelegate OnTargetOutOfRange;
    public event VoidDelegate OnTargetInAttackRange;

    public MoveToTargetState(StateMachine<TActor> fsm) : base(fsm) { }

    public override void Update()
    {
        Holder.Agent.SetDestination(Holder.Target.transform.position);

        float sqrDistance = (Fsm.Holder.Target.transform.position - Fsm.Holder.transform.position).sqrMagnitude;

        if (sqrDistance > Holder.DetectRangeSqr)
        {
            OnTargetOutOfRange?.Invoke();
        }
        else if (sqrDistance < Holder.AttackRangeSqr)
        {
            OnTargetInAttackRange?.Invoke();
        }
    }

    public override void OnExit() => Holder.Agent.ResetPath();
}