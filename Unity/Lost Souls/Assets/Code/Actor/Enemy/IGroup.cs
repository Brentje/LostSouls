﻿using UnityEngine;

/// <summary>
/// Abstract non generic group class
/// </summary>
public abstract class IGroup
{
    private Vector3 position;
    public float Range;

    public delegate void PositionDelegate(Vector3 position);
    public event PositionDelegate OnPositionChanged;

    public abstract int GetChildrenSize();
    public abstract void Clear();
    public abstract void Update();

    public Vector3 Position
    {
        get { return position; }
        set
        {
            position = value;
            OnPositionChanged?.Invoke(position); 
        }
    }
}
