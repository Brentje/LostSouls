﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This class contains the functionality required for group behaviour
/// </summary>
public class Group<TEnemyType> : IGroup where TEnemyType : GroupedEnemy<TEnemyType>
{
    private List<GroupedEnemy<TEnemyType>> children;

    public Group(Vector3 position, float range)
    {
        Position = position;
        Range = range;

        children = new List<GroupedEnemy<TEnemyType>>();
        GroupManager.Instance.Register(this);

        // Initial Update
        Update();
    }

    public Group(Vector3 position, float range, TEnemyType[] enemies)
    {
        Position = position;
        Range = range;

        GroupManager.Instance.Register(this);

        children = new List<GroupedEnemy<TEnemyType>>();

        // Tell them they are in interactionRange
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].Group != null && enemies[i].Group != this)
            {
                Group<TEnemyType> group = enemies[i].Group;
                if ((group.Position - Position).sqrMagnitude < Range * Range + group.Range * group.Range)
                {
                    for (int j = 0; j < group.children.Count; j++)
                    {
                        group.children[j].OnGroupDestroyed();
                        group.children[j].OnGroupInRange(this);
                    }
                    GroupManager.Instance.UnRegister(group);
                }
            }
            enemies[i].OnGroupInRange(this);
        }
    }

    // called once a second
    public override int GetChildrenSize()
    {
        return children.Count;
    }

    public override void Clear()
    {
        foreach (GroupedEnemy<TEnemyType> enemy in children)
        {
            enemy.OnGroupDestroyed();
        }
    }

    public override void Update()
    {
        // Get a list of nearby enemies
        TEnemyType[] enemies = GetEnemiesWithinRange();

        // Tell them they are in interactionRange
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].Group != null && enemies[i].Group != this)
            {
                Group<TEnemyType> group = enemies[i].Group;
                if ((group.Position - Position).sqrMagnitude < Range * Range + group.Range * group.Range)
                {
                    for (int j = 0; j < group.children.Count; j++)
                    {
                        group.children[j].OnGroupDestroyed();
                        group.children[j].OnGroupInRange(this);
                    }
                    GroupManager.Instance.UnRegister(group);
                }
            }
            enemies[i].OnGroupInRange(this);
        }

        // Calculate the center of the group
        Vector3 center = Vector3.zero;
        int count = 0;
        for (int i = 0; i < children.Count;)
        {
            if (!children[i])
            {
                children.RemoveAt(i);
                continue;
            }

            float sqrRange = (children[i].transform.position - Position).sqrMagnitude;
            if (sqrRange < Range * Range)
            {
                center += children[i].transform.position;
                count++;
            }
            else if (sqrRange < Range * Range * 1.3f)
            {
                children[i].MoveTowards(Actor.GetRandomPointWithinRange(Position, Range / 2f));
            }

            i++;
        }
        Position = center / count;

        if ((ActorManager.GetActivePlayer().transform.position - Position).sqrMagnitude > Range * Range)
        {
            for (int i = 0; i < children.Count; i++)
            {
                if ((children[i].transform.position - Position).sqrMagnitude > Range * Range)
                    children[i].MoveTowards(Actor.GetRandomPointWithinRange(Position, Range));
            }
        }
    }

    /// <summary>
    /// Returns a list with all enemies within range
    /// </summary>
    /// <returns></returns>
    public TEnemyType[] GetEnemiesWithinRange()
    {
        Collider[] colliders = Physics.OverlapSphere(Position, Range)
            .Where(col => col.gameObject.GetComponent<TEnemyType>()).ToArray();
        TEnemyType[] enemies = new TEnemyType[colliders.Length];
        for (int i = 0; i < colliders.Length; i++)
        {
            enemies[i] = colliders[i].gameObject.GetComponent<TEnemyType>();
        }
        return enemies;
    }

    /// <summary>
    /// Register an enemie with the group
    /// </summary>
    public bool Register(GroupedEnemy<TEnemyType> enemie)
    {
        children.Add(enemie);
        enemie.OnDeath += () => UnRegister(enemie);
        return true;
    }

    /// <summary>
    /// Unregister an enemie with the group
    /// </summary>
    public void UnRegister(GroupedEnemy<TEnemyType> enemie)
    {
        if (children.Contains(enemie))
            children.Remove(enemie);
    }
}
