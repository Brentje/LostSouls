﻿using System.Linq;
using StateMachine;
using UnityEngine;

/// <summary>
/// Idle state with group functionality
/// </summary>
public class GroupIdleState<TActor> : IdleState<TActor> where TActor : GroupedEnemy<TActor>
{
    /// <summary>
    /// Check if there are enemies without group in range and make a group with them
    /// </summary>
    public void GetEnemiesWithinRange()
    {
        Collider[] colliders = Physics.OverlapSphere(Holder.transform.position, 8)
            .Where(col => col.gameObject.GetComponent<TActor>() && col.gameObject.GetComponent<TActor>().Group == null).ToArray();
        Vector3 middle = Holder.transform.position;
        TActor[] actors = new TActor[colliders.Length];
        for (int i = 0; i < colliders.Length; i++)
        {
            middle += colliders[i].transform.position;
            actors[i] = colliders[i].gameObject.GetComponent<TActor>();
        }
        if (actors.Length > 1)
        {
            GroupManager.Instance.Register(new Group<TActor>(Holder.transform.position, 10, actors));
        }
    }

    public override void Update()
    {
        base.Update();
        if (Holder.Group == null)
            GetEnemiesWithinRange();
    }

    public override void OnEnter()
    {
        base.OnEnter();
        // Move to a base position where the actor will stay
        if (Holder.Group != null)
            if ((Holder.transform.position - Holder.Group.Position).sqrMagnitude > Holder.Group.Range * Holder.Group.Range)
                Holder.MoveTowards(Actor.GetRandomPointWithinRange(Holder.Group.Position, 2f));
    }

    public GroupIdleState(StateMachine<TActor> fsm) : base(fsm)
    {
    }
}