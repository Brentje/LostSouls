﻿using StateMachine;

public class SmallSpiderDiagram : StateDiagram<SmallSpider>
{
    public override void Init(StateMachine<SmallSpider> stateMachine)
    {
        StateMachine = stateMachine;

        GroupIdleState<SmallSpider> idle = new GroupIdleState<SmallSpider>(stateMachine);
        idle.TargetInDetectRange += () => StateMachine.SetState(2);

        HideBehindGroupState<SmallSpider> hide = new HideBehindGroupState<SmallSpider>(stateMachine);

        MoveToTargetState<SmallSpider> moveToTarget = new MoveToTargetState<SmallSpider>(stateMachine);
        moveToTarget.OnTargetOutOfRange += () => stateMachine.SetState(0);
        moveToTarget.OnTargetInAttackRange += () => stateMachine.SetState(3);

        AttackState<SmallSpider> attack = new AttackState<SmallSpider>(stateMachine, new AttackAbility(stateMachine.Holder, "BasicAttack", ActorManager.GetActivePlayer()));
        attack.OnTargetOutOfRange += () => stateMachine.SetState(2);

        stateMachine.Holder.Stats.GetStat(StatType.Health).OnValueChanged += x =>
        {
            if (x < 0)
                return;
            if (!(x < stateMachine.Holder.Stats.GetStat(StatType.Health).MaxValue / 3f))
                return;
            if (stateMachine.Holder.Group != null)
                stateMachine.SetState(1);
        };

        States.Add(0, idle);
        States.Add(1, hide);
        States.Add(2, moveToTarget);
        States.Add(3, attack);

        stateMachine.SetState(0);
    }
}
