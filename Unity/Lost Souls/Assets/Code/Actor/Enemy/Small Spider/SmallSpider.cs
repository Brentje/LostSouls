﻿using StateMachine;

public class SmallSpider : GroupedEnemy<SmallSpider>
{
    public static int killedSpiders;

    protected override void Awake()
    {
        base.Awake();
        StateMachine = new StateMachine<SmallSpider>(this) { Diagram = new SmallSpiderDiagram() };
        StateMachine.Diagram.Init(StateMachine);
        OnDeath += () => killedSpiders++;
    }
}
