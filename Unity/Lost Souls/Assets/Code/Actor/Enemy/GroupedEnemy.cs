﻿using UnityEngine;

/// <summary>
/// This class contains the functionality required to be part of a group
/// </summary>
public abstract class GroupedEnemy<TEnemyType> : BaseEnemy<TEnemyType> where TEnemyType : GroupedEnemy<TEnemyType>
{
    public Group<TEnemyType> Group;

    protected GroupedEnemy() { }

    protected GroupedEnemy(float attackRangeSqr, float detectRangeSqr) : base(attackRangeSqr, detectRangeSqr) { }

    /// <summary>
    /// Register that the group has moved
    /// </summary>
    public void OnGroupMoved(Vector3 newPos)
    {
        if (StateMachine.CurrentState == 0)
            MoveTowards(GetRandomPointWithinRange(newPos, Group.Range / 1.5f));
    }

    /// <summary>
    /// Callback for when there is a group in range
    /// </summary>
    public void OnGroupInRange(Group<TEnemyType> group)
    {
        if (Group == null)
            if (group.Register(this))
            {
                Group = group;
                if (StateMachine.CurrentState == 0)
                    MoveTowards(GetRandomPointWithinRange(group.Position, group.Range / 2f));
            }
    }

    /// <summary>
    /// Set the group to null to remove all references to the group
    /// </summary>
    public void OnGroupDestroyed()
    {
        Group = null;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        DebugUtil.DrawCircle(transform.position, transform.up, transform.forward, 8, 24, Color.black);
    }
}
