﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Events;

public enum StatType
{
    Health = (1 << 0),
    Mana = (1 << 1),
    PhysicalDamage = (1 << 2),
    MagicalDamage = (1 << 3),
    PhysicalArmor = (1 << 4),
    MagicalArmor = (1 << 5),
    Vitality = (1 << 6),
    Intelligence = (1 << 7),
    Dexterity = (1 << 8),
    Strength = (1 << 9),
    AttackSpeed = (1 << 10),
    MovementSpeed = (1 << 11),
    Memory = (1 << 12)
}

[Serializable]
public class StatData
{
    [SerializeField] private float value;
    public float MaxValue;
    public bool IsPercentage;
    [XmlIgnore] public UnityAction<float> OnValueChanged;

    public StatData() { }

    public StatData(float value, float maxValue, bool isPercentage)
    {
        this.value = value;
        MaxValue = maxValue;
        IsPercentage = isPercentage;
    }

    public float Value
    {
        get { return value; }
        set
        {
            this.value = value;
            OnValueChanged?.Invoke(value);
        }
    }

    public void SetValueWithoutTrigger(float newValue)
    {
        value = newValue;
    }

    public void AddListener(UnityAction<float> listener)
    {
        OnValueChanged += listener;
    }
}
[Serializable]
public class StatDataPair : IXmlSerializable
{
    public StatType Key;
    public StatData Value;

    public override string ToString()
    {
        return $"<{(int)Key}|{(Value.IsPercentage ? 1 : 0)}|{Value.Value}|{Value.MaxValue}>";
    }

    public static StatDataPair FromString(string dataString)
    {
        StatDataPair pair = new StatDataPair();
        pair.Value = new StatData();
        dataString = dataString.Trim('<', '>');
        string[] data = dataString.Split('|');
        if (data.Length == 4)
        {
            pair.Key = (StatType)int.Parse(data[0]);
            pair.Value.IsPercentage = int.Parse(data[1]) == 1;
            pair.Value.Value = float.Parse(data[2]);
            pair.Value.MaxValue = float.Parse(data[3]);
            return pair;
        }
        return null;
    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void ReadXml(XmlReader reader)
    {
        StatDataPair pair = StatDataPair.FromString(reader.ReadString());
        Value = pair.Value;
        Key = pair.Key;
    }

    public void WriteXml(XmlWriter writer)
    {
        writer.WriteString(ToString());
    }
}

[Serializable]
public class StatList
{
    public List<StatDataPair> Data;

    public void Add(StatType key, StatData value)
    {
        foreach (StatDataPair pair in Data)
        {
            if (pair.Key == key)
                return;
        }
        StatDataPair data = new StatDataPair() { Key = key, Value = value };
        Data.Add(data);
    }

    public StatData this[StatType key]
    {
        get
        {
            foreach (StatDataPair dataPair in Data)
            {
                if (dataPair.Key == key)
                    return dataPair.Value;
            }
            throw new KeyNotFoundException((int)key + " not found!");
        }
        set
        {
            foreach (StatDataPair t in Data)
            {
                if (t.Key == key)
                {
                    t.Value = value;
                    return;
                }
            }
            Add(key, value);
        }
    }

    public bool ContainsStat(StatType key)
    {
        foreach (StatDataPair dataPair in Data)
        {
            if (dataPair.Key == key)
                return true;
        }
        return false;
    }

    public StatList()
    {
        Data = new List<StatDataPair>();
    }

    public void AddStat(StatType type, StatData statData)
    {
        Add(type, statData);
    }

    public StatData GetStat(StatType stat)
    {
        return this[stat];
    }
}