﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using StateMachine;
using UnityEngine;

[Serializable]
public class ItemContainer
{
    public List<Item> items;
    private int weight;
    private int maxWeight;

    private bool overMax;

    public event VoidDelegate OnOverMaxWeight;
    public event VoidDelegate OnUnderMaxWeight;

    public ItemContainer()
    {
        items = new List<Item>();
    }
    [XmlIgnore]
    public int Weight
    {
        get { return weight; }
        private set { weight = value; }
    }
    [XmlIgnore]
    public int MaxWeight
    {
        get { return maxWeight; }
        private set { maxWeight = value; }
    }

    public void ChangeMaxWeight(int newMax)
    {
        if (newMax < 0)
            return;
        MaxWeight = newMax;
        if (Weight > MaxWeight)
        {
            if (!overMax)
                OnOverMaxWeight?.Invoke();
            overMax = true;
        }
        else
        {
            if (overMax)
                OnUnderMaxWeight?.Invoke();
            overMax = false;
        }
    }

    public virtual bool CheckForSpaceForItem(Item item)
    {
        return weight + item.Weight < maxWeight;
    }

    public virtual void AddItemToInventory(Item item)
    {
        items.Add(item);
        Weight += item.Weight;

        if (Weight > MaxWeight)
        {
            if (!overMax)
                OnOverMaxWeight?.Invoke();
            overMax = true;
        }
    }

    public void RemoveItemFromInventory(Item item)
    {
        items.Remove(item);
        Weight -= item.Weight;

        if (Weight <= MaxWeight)
        {
            if (overMax)
                OnUnderMaxWeight?.Invoke();
            overMax = false;
        }
    }

    public Item GetItem(string name)
    {
        foreach (Item item in items)
            if (item.Name == name)
                return item;
        return null;
    }

    public Item GetItem(string name, out bool foundItem)
    {
        foreach (Item item in items)
            if (item.Name == name)
            {
                foundItem = true;
                return item;
            }
        foundItem = false;
        return null;
    }

    public Item[] GetItems()
    {
        return items.ToArray();
    }

    public string SaveToString()
    {
        return "";
    }

    public void LoadFromString(string data)
    {

    }
}