﻿public class ItemChest : Interactable
{
    public int[] InitItems = new int [0];
    public ItemContainer Items;

    private void Start()
    {
        Items = new ItemContainer();
        for (int i = 0; i < InitItems.Length; i++)
        {
            Items.AddItemToInventory(ItemManager.ItemList[InitItems[i]]);
        }
        SubscribeToInteraction(OnInteract);
    }

    public void OnInteract(Actor actor)
    {
        InventoryUI.OpenContainer(Items);

    }
}
