﻿using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Makes the item into a weapon when equiped
/// </summary>
public class WeaponModifier : EquipmentModifier
{
    [XmlIgnore]
    private AttackAbility spell;

    private string abilityName;

    // Attack
    public virtual void Attack(Actor target)
    {
        if (spell != null)
        {
            spell.SetActor("Target", target);
            spell.CastAbility();
            spell.RunAbilityBehaviour();
        }
    }

    protected WeaponModifier(string abilityName)
    {
        this.abilityName = abilityName;
    }

    public WeaponModifier()
    {
    }

    public override void OnEquip(Player equipedActor)
    {
        base.OnEquip(equipedActor);
        spell = new AttackAbility(ActorManager.GetActivePlayer(), abilityName, null);
    }

    public override string ToString()
    {
        return base.ToString() + abilityName;
    }

    public override void SetDataFromString(string data)
    {
        abilityName = data;
    }
#if UNITY_EDITOR
    public override void GUIDraw(Rect rect)
    {
        Rect textRect = rect;
        textRect.height -= 2;
        if (rect.width > 370) 
        {
            textRect.x = rect.width - 151;
            textRect.width = 126;
        }
        else
        {
            textRect.x = 220;
            textRect.width = rect.width - 242;
        }
        base.GUIDraw(rect);
        string[] keys = AbilityBehaviourDatabase.Database.Abilities.Keys.ToArray();
        int i = 0;
        for (int j = 0; j < keys.Length; j++)
        {
            if (keys[j] == abilityName)
            {
                i = j;
                break;
            }
        }
        i = UnityEditor.EditorGUI.Popup(textRect, i, keys);
        abilityName = keys[i];
    }
#endif
}

public class TwoHandedModifier : WeaponModifier
{
    public TwoHandedModifier(string abilityName) : base(abilityName)
    {
    }

    public TwoHandedModifier()
    {
    }

    public override void OnEquip(Player equipedActor)
    {
        Debug.Log("Wat nou");
        if (equipedActor.Equipment.Slots[(int)Equipment.EquipmentSlotType.Weapon1].IsOccupied() || equipedActor.Equipment.Slots[(int)Equipment.EquipmentSlotType.Weapon2].IsOccupied())
            base.OnEquip(equipedActor);
    }

    // TODO: Check if actor has 2 weapons equiped and if so unequip the other weapon when this weapon is equiped
}

// TODO: Decide if you need different behavior for these. If not remove them and modify EquipmentModifier to have a slot type enum.

public class RingModifier : EquipmentModifier
{

}

public class BeltModifier : EquipmentModifier
{

}

public class HelmModifier : EquipmentModifier
{

}

public class ChestModifier : EquipmentModifier
{

}

public class LeggingModifier : EquipmentModifier
{

}

public class BootsModifier : EquipmentModifier
{

}

public class GuantletModifier : EquipmentModifier
{

}

public class NecklaceModifier : EquipmentModifier
{

}