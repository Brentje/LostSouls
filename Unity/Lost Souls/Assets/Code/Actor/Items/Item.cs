﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Item
/// </summary>
[Serializable]
public class Item : ISerializationCallbackReceiver
{
    //private const string Regex = @"Item\n{\n\tvalue=(\d*)\n\tweight=(\d*)\n\tname=(.*)\n\tprefab=(.*)\n\ttexture(.*)\n\tmodifiers={\n((\t\t\[([A-z0-9-]*):([A-z0-9]*):([A-z0-9-|]*)\],\n)*)\t}\n}";

    // they are all nonSerialized because I have a to and from string function to override the serialization

    /// <summary> List of modifiers that add functionality to the item</summary>
    [NonSerialized] public List<Modifier> Modifiers;
    /// <summary> Item name</summary>
    [NonSerialized] public string Name;
    /// <summary> Prefab key</summary>
    [NonSerialized] public string Prefab;
    /// <summary> Texture Key</summary>
    [NonSerialized] public string Texture;
    /// <summary>Item value</summary>
    [NonSerialized] public int Value;
    /// <summary>Item Weight</summary>
    [NonSerialized] public int Weight;

    /// <summary>This is there to fix serialization</summary>
    [SerializeField] private string serializedItem;

    public Item()
    {
        // Create a modifiers list
        Modifiers = new List<Modifier>();
    }

    public void OnBeforeSerialize()
    {
        // Convert this item to a string
        serializedItem = SaveDataToString();
    }

    public void OnAfterDeserialize()
    {
        // Convert the string back to an item
        LoadFromString(serializedItem);
        // clear the string
        serializedItem = "";
    }

    /// <summary>
    /// Get the texture from the texture database
    /// </summary>
    public Texture2D GetTexture()
    {
        // Check if the key is not empty
        if (string.IsNullOrEmpty(Texture))
            return null;
        // Check if the database exists
        if (ItemManager.TextureDatabase == null)
            return null;
        // Check if the key exists in the database and return the value as texture
        return ItemManager.TextureDatabase.Textures.ContainsKey(Texture)
            ? ItemManager.TextureDatabase.Textures[Texture].texture
            : null;
    }

    /// <summary>
    /// Get the sprite from the texture database
    /// </summary>
    public Sprite GetSprite()
    {
        // Check if the key is not empty
        if (string.IsNullOrEmpty(Texture))
            return null;
        // Check if the database exists
        if (ItemManager.TextureDatabase == null)
            return null;
        // Check if the key exists in the database and return the value
        return ItemManager.TextureDatabase.Textures.ContainsKey(Texture)
            ? ItemManager.TextureDatabase.Textures[Texture]
            : null;
    }

    /// <summary>
    /// Get the prefab from the prefab database
    /// </summary>
    public GameObject GetPrefab()
    {
        // Check if the key is not empty
        if (string.IsNullOrEmpty(Prefab))
            return null;
        // Check if the database exists
        if (ItemManager.PrefabDatabase == null)
            return null;
        // Check if the key exists in the database and return the value
        return ItemManager.PrefabDatabase.Prefabs.ContainsKey(Prefab)
            ? ItemManager.PrefabDatabase.Prefabs[Prefab]
            : null;
    }

    /// <summary>
    /// Instantiate the prefab - This doesn't remove the item from any inventory of list
    /// </summary>
    public void Drop()
    {
        UnityEngine.Object.Instantiate(GetPrefab()).AddComponent<DroppedItem>().Item = ToString();
    }

    /// <summary>
    /// Returns a modifier of the type if its found
    /// </summary>
    /// <typeparam name="T">Generic modifier type</typeparam>
    public T GetModifier<T>() where T : Modifier
    {
        foreach (Modifier t in Modifiers)
            if (typeof(T) == t.GetType())
                return (T)t;
        return null;
    }

    /// <summary>
    /// Returns a modifier of the type if its found
    /// </summary>
    /// <typeparam name="T">Generic modifier type</typeparam>
    public T[] GetModifiers<T>() where T : Modifier
    {
        List<T> types = new List<T>();
        foreach (Modifier t in Modifiers)
            if (t.GetType().IsSubclassOf(typeof(T)) || t.GetType() == typeof(T))
                types.Add((T)t);
        return types.ToArray();
    }

    /// <summary>
    /// Returns a modifier of the type if its found
    /// </summary>
    /// <param name="type">Generic modifier type</param>
    public Modifier GetModifier(Type type)
    {
        foreach (Modifier t in Modifiers)
            if (type == t.GetType())
                return t;
        return null;
    }

    /// <summary>
    /// Convert to a string
    /// </summary>
    public string SaveDataToString()
    {
        string data = "Item\n{\n";
        // Add item variables to the string
        data += "\tvalue=" + Value + "\n";
        data += "\tweight=" + Weight + "\n";
        data += "\tname=" + Name + "\n";
        data += "\tprefab=" + Prefab + "\n";
        data += "\ttexture=" + Texture + "\n";
        data += "\tmodifiers={\n";
        // Add all modifiers to the string
        foreach (Modifier modifier in Modifiers)
        {
            data += "\t\t[" + modifier.ToString() + "],\n";
        }
        data += "\t}\n";
        data += "}";
        return data;
    }

    /// <summary>
    /// Convert string to an Item
    /// </summary>
    public static Item CreateItemFromString(string itemData)
    {
        // currently the regex isn't working TODO: fix
        //if (System.Text.RegularExpressions.Regex.IsMatch(itemData, Regex))
        {
            Item item = new Item();
            string[] lines = itemData.Split('\n');
            for (int i = 2; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("\tvalue="))
                {
                    item.Value = int.Parse(lines[i].Replace("\tvalue=", ""));
                    continue;
                }
                if (lines[i].StartsWith("\tweight="))
                {
                    item.Weight = int.Parse(lines[i].Replace("\tweight=", ""));
                    continue;
                }
                if (lines[i].StartsWith("\tname="))
                {
                    item.Name = lines[i].Replace("\tname=", "");
                    continue;
                }
                if (lines[i].StartsWith("\tprefab="))
                {
                    item.Prefab = lines[i].Replace("\tprefab=", "");
                    continue;
                }
                if (lines[i].StartsWith("\ttexture="))
                {
                    item.Texture = lines[i].Replace("\ttexture=", "");
                    continue;
                }
                if (lines[i].StartsWith("\t\t["))
                {
                    string[] data = lines[i].Replace("\t", "").Remove(0, 1).Split(':');
                    Assembly assembly = AppDomain.CurrentDomain.GetAssemblies()
                        .SingleOrDefault(temp => temp.GetName().Name == data[0]);
                    Modifier modifier = (Modifier)Activator.CreateInstanceFrom(assembly.CodeBase, data[1])
                        .Unwrap();
                    modifier.SetDataFromString(data[2].Replace("],", ""));
                    item.Modifiers.Add(modifier);
                }
            }
            return item;
        }
        //Debug.Log(itemData);
        //return null;
    }

    /// <summary>
    /// Load variables from a string
    /// </summary>
    public void LoadFromString(string data)
    {
        Item item = CreateItemFromString(data);
        if (item == null)
            return;
        Value = item.Value;
        Weight = item.Weight;
        Name = item.Name;
        Prefab = item.Prefab;
        Texture = item.Texture;
        Modifiers = item.Modifiers;
    }
}