﻿using System;
using UnityEngine;

[Serializable]
public abstract class Modifier
{
    [NonSerialized] protected Item Item;

    public virtual void OnInit(Item item)
    {
        Item = item;
    }

    public override string ToString()
    {
        Type type = GetType();
        return type.Assembly.GetName().Name + ':' + type.FullName + ':';
    }

    public virtual void SetDataFromString(string data) { }

#if UNITY_EDITOR
    public virtual void GUIDraw(Rect rect)
    {
        rect.height = 17;
        rect.width -= 45;
        GUI.Label(rect, GetType().FullName);

    }
#endif

}

[Serializable]
public abstract class EquipmentModifier : Modifier
{
    public virtual void OnEquip(Player equipedActor)
    {
        // TODO: Recalculate actor stats
    }

    public virtual void OnUnequip(Player equipedActor)
    {
        // TODO: Recalculate actor stats
    }
}

[Serializable]
public class StatModifier : Modifier
{
    public StatDataPair data;

    public StatDataPair GetStat()
    {
        return data;
    }

    public StatModifier()
    {
        data = new StatDataPair();
        data.Value = new StatData();
    }

    public virtual void OnProtect() { }

    public override string ToString()
    {
        return base.ToString() + data.ToString(); 
    }

    public override void SetDataFromString(string data)
    {
        this.data = StatDataPair.FromString(data);
    }

#if UNITY_EDITOR
    public override void GUIDraw(Rect rect)
    {
        Rect protectionRect = rect;
        protectionRect.height -= 2;
        if (rect.width > 370)
        {
            protectionRect.x = rect.width - 151;
            protectionRect.width = 126;
        }
        else
        {
            protectionRect.x = 220;
            protectionRect.width = rect.width - 242;
        }
        base.GUIDraw(rect);
        protectionRect.width /= 2f;
        data.Value.Value = UnityEditor.EditorGUI.FloatField(protectionRect, data.Value.Value);
        protectionRect.x += protectionRect.width;
        data.Value.MaxValue = UnityEditor.EditorGUI.FloatField(protectionRect, data.Value.MaxValue);

        protectionRect.x -= 140;
        protectionRect.width = 76;
        protectionRect.y += 2;
        data.Value.IsPercentage = UnityEditor.EditorGUI.Popup(protectionRect, data.Value.IsPercentage ? 1 : 0, new[] { "Normal", "Percentage" }) == 1;
        protectionRect.x -= 78;
        data.Key = (StatType)UnityEditor.EditorGUI.EnumFlagsField(protectionRect, data.Key);
    }
#endif
}

public abstract class UsableModifier : Modifier
{
    public abstract void OnUse(Actor user);
}