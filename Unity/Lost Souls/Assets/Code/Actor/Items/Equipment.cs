﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

[System.Serializable]
public class Equipment : IXmlSerializable
{
    public enum EquipmentSlotType
    {
        Helm,
        Chest,
        Legging,
        Boots,

        Necklace,
        Belt,
        Guantlet,
        Ring1,
        Ring2,

        Potion1,
        Potion2,
        Potion3,
        Potion4,
        Potion5,

        Weapon1,
        Weapon2,
    }

    public StatList armorstats;
    public StatList armorstatsPercent;
    public bool StatsAreDirty;

    public ItemSlotWithEvent[] Slots =
    {
        // Armor
        new EquipmentSlot<HelmModifier>(),
        new EquipmentSlot<ChestModifier>(),
        new EquipmentSlot<LeggingModifier>(),
        new EquipmentSlot<BootsModifier>(),
        // Equipment
        new EquipmentSlot<NecklaceModifier>(),
        new EquipmentSlot<BeltModifier>(),
        new EquipmentSlot<GuantletModifier>(),
        new EquipmentSlot<RingModifier>(),
        new EquipmentSlot<RingModifier>(),
        // Potions
        new EquipmentSlot<PotionModifier>(),
        new EquipmentSlot<PotionModifier>(),
        new EquipmentSlot<PotionModifier>(),
        new EquipmentSlot<PotionModifier>(),
        new EquipmentSlot<PotionModifier>(),
        // Weapons
        new EquipmentSlot<WeaponModifier>(),
        new EquipmentSlot<WeaponModifier>()
    };

    public Equipment()
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            Slots[i].OnSwitch += (x) =>
            {
                RecalculateEquipmentStats();
            };
        }
        armorstats = new StatList();
        armorstatsPercent = new StatList();
    }

    private void RecalculateEquipmentStats()
    {
        armorstats = new StatList();
        armorstatsPercent = new StatList();
        for (int i = 0; i < Slots.Length; i++)
        {
            if (!Slots[i].IsOccupied())
                continue;
            StatModifier[] stats = Slots[i].GetCurrentItem().GetModifiers<StatModifier>();
            foreach (StatModifier stat in stats)
            {
                if (stat != null)
                {
                    StatDataPair data = stat.data;
                    if (data.Value.IsPercentage)
                    {
                        if (!armorstatsPercent.ContainsStat(data.Key))
                            armorstatsPercent[data.Key] = new StatData();
                        armorstatsPercent[data.Key].Value += data.Value.Value;
                        armorstatsPercent[data.Key].MaxValue += data.Value.MaxValue;
                    }
                    else
                    {
                        if (!armorstats.ContainsStat(data.Key))
                            armorstats[data.Key] = new StatData();
                        armorstats[data.Key].Value += data.Value.Value;
                        armorstats[data.Key].MaxValue += data.Value.MaxValue;
                    }
                }
            }
        }
        StatsAreDirty = true;
    }

    public EquipmentSlot<T> GetEquipmentSlot<T>() where T : Modifier
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            if (Slots[i].CheckForModifier<T>())
                return (EquipmentSlot<T>)Slots[i];
        }
        return null;
    }

    public ItemSlot Equip(Item item)
    {
        if (item == null)
            return null;
        for (int i = 0; i < Slots.Length; i++)
        {
            if (item.GetModifier(Slots[i].GetRequiredModifier()) != null)
            {
                if (!Slots[i].IsOccupied())
                {
                    Slots[i].SetItem(item);

                    RecalculateEquipmentStats();
                    return Slots[i];
                }
            }
        }
        return null;
    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void ReadXml(XmlReader reader)
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            string item = reader.GetAttribute(((EquipmentSlotType)i).ToString());
            if (!string.IsNullOrEmpty(item) && item != "NULL")
                Slots[i].SwitchItem(Item.CreateItemFromString(item));
        }
    }

    public void WriteXml(XmlWriter writer)
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            if (Slots[i].IsOccupied())
                writer.WriteAttributeString(((EquipmentSlotType)i).ToString(), Slots[i].GetCurrentItem().SaveDataToString());
            else
                writer.WriteAttributeString(((EquipmentSlotType)i).ToString(), "NULL");
        }
    }
}

[Serializable]
public class ItemSlot
{
    private Item item;

    public Item GetCurrentItem()
    {
        return item;
    }

    public virtual void SetItem(Item item)
    {
        this.item = item;
    }

    public bool IsOccupied()
    {
        return !string.IsNullOrEmpty(item?.Name);
    }

    public virtual Item SwitchItem(Item newItem)
    {
        Item oldItem = item;
        item = newItem;
        return oldItem;
    }

    public virtual bool CheckIfItemFits<TModifier>()
    {
        return true;
    }

    public virtual bool CheckIfItemFits(Modifier modifier)
    {
        return true;
    }
    /// <summary>
    /// Only works if the method is overwritten
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    /// <returns></returns>
    public virtual bool CheckForModifier<ModifierType>() where ModifierType : Modifier
    {
        return false; // prevent other classes from thinking that is does have it so they wont try and use it
    }

    /// <summary>
    /// Only works if the method is overwritten
    /// </summary>
    /// <returns></returns>
    public virtual bool CheckForModifier(Modifier modifier)
    {
        return false; // prevent other classes from thinking that is does have it so they wont try and use it
    }

    public virtual Type GetRequiredModifier()
    {
        return null;
    }
}

public class ItemSlotWithEvent : ItemSlot
{
    public delegate void ItemDelegate(ItemSlot item);

    public event ItemDelegate OnSwitch;

    public override Item SwitchItem(Item newItem)
    {
        Item item = base.SwitchItem(newItem);
        OnSwitch?.Invoke(this);
        return item;
    }

    public override void SetItem(Item item)
    {
        base.SetItem(item);
        OnSwitch?.Invoke(this);
    }
}

public class EquipmentSlot<T> : ItemSlotWithEvent where T : Modifier
{
    public override Item SwitchItem(Item newItem)
    {
        Item oldItem = GetCurrentItem();
        if (newItem == null)
        {
            SetItem(null);
            return oldItem;
        }
        T modi = newItem.GetModifier<T>();
        if (modi != null)
        {
            SetItem(newItem);
            return oldItem;
        }
        return newItem;
    }

    public override bool CheckIfItemFits<TModifier>()
    {
        return typeof(TModifier).IsSubclassOf(typeof(T));
    }

    public override bool CheckForModifier(Modifier modifier)
    {
        return modifier.GetType() == typeof(T);
    }

    public override bool CheckForModifier<TModifier>()
    {
        return typeof(TModifier) == typeof(T);
    }

    public override bool CheckIfItemFits(Modifier modifier)
    {
        return modifier.GetType().IsSubclassOf(typeof(T));
    }

    public override Type GetRequiredModifier()
    {
        return typeof(T);
    }

    public override void SetItem(Item item)
    {
        base.SetItem(item);
        if (GetCurrentItem() != null)
            GetCurrentItem().GetModifiers<EquipmentModifier>().All(modifier =>
            {
                modifier.OnEquip(ActorManager.GetActivePlayer());
                return true;
            });
    }
}