﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Utility;
using Object = UnityEngine.Object;

[CustomEditor(typeof(ItemList))]
public class ItemListEditor : Editor
{

    //##############################################
    //#  You don't wanna see the code!           ###
    //#  trust me, it is code for a costum       ###
    //#  editor and you can see it by selecting  ###
    //#  an itemlist but I see no reason you     ###
    //#  would ever want to see this code...     ###
    //##############################################


    private ItemList itemList;
    private Vector2 itemScrollView;

    private int selectedItem = -1;

    private int currentWindow;

    private string itemPrefab;
    private string itemTexture;
    private int itemValue;
    private int currentModifier;
    private int itemWeight;
    private string itemName;
    private List<Modifier> itemStats;

    private Type[] modifiersTypes;
    private string[] modifierTypeStrings;

    private bool showModifiers;


    public void OnEnable()
    {
        itemList = (ItemList)target;
        itemStats = new List<Modifier>();
        ReloadModifierTypes();
    }

    public override void OnInspectorGUI()
    {
        DrawItemList();

        GUIStyle styleWhite = new GUIStyle(GUI.skin.button);
        styleWhite.normal = styleWhite.onActive;
        GUIStyle styleGray = new GUIStyle(GUI.skin.button);

        GUILayout.Space(10); 

        Rect buttonRect = GUILayoutUtility.GetRect(Screen.width, 20);
        buttonRect.width = buttonRect.width / 2f - 12;

        GUILayout.Space(10);

        if (GUI.Button(buttonRect, "Create item", currentWindow == 0 ? styleWhite : styleGray))
        {
            currentWindow = 0;
        }
        buttonRect.x = buttonRect.width + 20;
        if (GUI.Button(buttonRect, "Edit item", currentWindow == 1 ? styleWhite : styleGray))
        {
            currentWindow = 1;
        }

        switch (currentWindow)
        {
            case 0:
            {
                GUILayout.Label("Item Creation");
                DrawItemCreation();
                break;
            }
            case 1:
            {
                GUILayout.Label("Item Editor");
                DrawItemEditor();
                break;
            }
        }

        DrawItemPreview();
    }

    private Editor gameObjectEditor;

    private void ReloadModifierTypes()
    {
        modifiersTypes = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type.IsSubclassOf(typeof(Modifier)) && !type.IsAbstract).ToArray();

        modifierTypeStrings = new string[modifiersTypes.Length];

        for (int i = 0; i < modifiersTypes.Length; i++)
        {
            modifierTypeStrings[i] = modifiersTypes[i].FullName;
        }
    }

    private void DrawItemPreview()
    {
        if (selectedItem < 0 || selectedItem >= itemList.Length)
            return;
        Object prefab = itemList[selectedItem].GetPrefab();

        GUILayout.Box(AssetPreview.GetAssetPreview(prefab));
    }

    private void DrawItemList()
    {
        Rect scrollRect = GUILayoutUtility.GetRect(Screen.width, 300);
        scrollRect.width -= 18;
        GUI.Box(scrollRect, "");
        scrollRect.width += 18;

        GUIStyle buttonStyle = new GUIStyle(GUI.skin.label);

        itemScrollView = GUI.BeginScrollView(scrollRect, itemScrollView, new Rect(-5, 0, Screen.width - 45, itemList.Items.Count * 20 + 5));
        if (itemList != null)
        {
            for (int i = 0; i < itemList.Length; i++)
            {
                if (i == selectedItem)
                {
                    buttonStyle = new GUIStyle(GUI.skin.box)
                    {
                        alignment = TextAnchor.UpperLeft
                    };
                }
                Rect textureRect = new Rect(0, i * 20 + 5, 30, 20);
                Texture2D texture = itemList[i].GetTexture();
                if (texture)
                    GUI.Box(textureRect, texture);
                else
                    GUI.Box(textureRect, "");
                Rect nameRect = new Rect(30, i * 20 + 5, Screen.width - 100, 20);
                if (GUI.Button(nameRect, itemList[i].Name, buttonStyle))
                {
                    selectedItem = i;
                    itemPrefab = itemList[i].Prefab;
                    itemTexture = itemList[i].Texture;
                    itemValue = itemList[i].Value;
                    itemWeight = itemList[i].Weight;
                    itemName = itemList[i].Name;
                    itemStats = itemList[i].Modifiers;
                    GUI.FocusControl(null);
                }
                if (i == selectedItem)
                    buttonStyle = new GUIStyle(GUI.skin.label);
                nameRect.x = Screen.width - 65;
                nameRect.height = 17;
                nameRect.width = 18;
                if (GUI.Button(nameRect, "X"))
                {
                    itemList.Items.Remove(itemList[i]);
                    if (i == selectedItem)
                        selectedItem = -1;
                    if (i < selectedItem)
                        selectedItem--;

                    EditorUtility.SetDirty(target);
                    AssetDatabase.SaveAssets();
                    break;
                }
            }
        }
        GUI.EndScrollView();
    }

    private void DrawItemCreation()
    {
        itemPrefab = SelectFromDataBaseWithLabel(ItemManager.PrefabDatabase.Prefabs.Keys.ToArray(), itemPrefab, "Prefab:", ItemManager.PrefabDatabase);
        itemTexture = SelectFromDataBaseWithLabel(ItemManager.TextureDatabase.Textures.Keys.ToArray(), itemTexture, "Texture:", ItemManager.TextureDatabase);
        itemValue = IntFieldWithLabel(itemValue, "Value:");
        itemWeight = IntFieldWithLabel(itemWeight, "Weight:");
        itemName = StringFieldWithLabel(itemName, "Name:");

        Rect buttonRect = GUILayoutUtility.GetRect(100, 18);
        buttonRect.width = 100;
        buttonRect.x = 75;

        if (GUI.Button(buttonRect, "Add"))
        {
            Item item = new Item { Value = itemValue, Weight = itemWeight, Name = itemName, Texture = itemTexture, Prefab = itemPrefab, Modifiers = itemStats };
            itemPrefab = "";
            itemTexture = "";
            itemValue = 0;
            itemWeight = 0;
            itemName = "";
            itemStats = new List<Modifier>();
            itemList.Items.Add(item);
            GUI.FocusControl(null);

            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }
    }

    private void DrawItemEditor()
    {
        if (selectedItem < 0 || selectedItem >= itemList.Length)
            return;
        itemPrefab = SelectFromDataBaseWithLabel(ItemManager.PrefabDatabase.Prefabs.Keys.ToArray(), itemPrefab, "Prefab:", ItemManager.PrefabDatabase);
        itemTexture = SelectFromDataBaseWithLabel(ItemManager.TextureDatabase.Textures.Keys.ToArray(), itemTexture, "Texture:", ItemManager.TextureDatabase);
        itemValue = IntFieldWithLabel(itemValue, "Value:");
        itemWeight = IntFieldWithLabel(itemWeight, "Weight:");
        itemName = StringFieldWithLabel(itemName, "Name:");
        DisplayModifiers();

        Rect buttonRect = GUILayoutUtility.GetRect(100, 18);
        buttonRect.width = 100;
        buttonRect.x = 75;

        if (GUI.Button(buttonRect, "Save"))
        {
            Undo.RecordObject(itemList, "Item edited");
            itemList[selectedItem].Prefab = itemPrefab;
            itemList[selectedItem].Texture = itemTexture;
            itemList[selectedItem].Value = itemValue;
            itemList[selectedItem].Weight = itemWeight;
            itemList[selectedItem].Name = itemName;
            itemList[selectedItem].Modifiers = itemStats;
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }
    }

    private T ObjectField<T>(T obj, string label, int width, int height) where T : Object
    {
        var centeredStyle = GUI.skin.label;
        centeredStyle.alignment = TextAnchor.UpperRight;

        Rect itemRect = GUILayoutUtility.GetRect(width + 70, height);
        itemRect.y += height / 2f - 10;
        itemRect.height = height - 3;
        itemRect.width = 60;
        GUI.Label(itemRect, label);
        centeredStyle.alignment = TextAnchor.UpperLeft;
        itemRect.y -= height / 2f - 10;
        itemRect.width = width < Screen.width - 90 ? width : Screen.width - 90;
        itemRect.x = 75;
        return (T)EditorGUI.ObjectField(itemRect, obj, typeof(T), false);
    }

    private int IntFieldWithLabel(int value, string label)
    {
        var centeredStyle = GUI.skin.label;
        centeredStyle.alignment = TextAnchor.UpperRight;

        Rect itemRect = GUILayoutUtility.GetRect(Screen.width, 20);
        itemRect.height = 17;
        itemRect.width = 60;
        GUI.Label(itemRect, label);
        centeredStyle.alignment = TextAnchor.UpperLeft;

        itemRect.width = Screen.width - 90;
        itemRect.x = 75;
        return EditorGUI.IntField(itemRect, value);
    }

    private string StringFieldWithLabel(string value, string label)
    {
        var centeredStyle = GUI.skin.label;
        centeredStyle.alignment = TextAnchor.UpperRight;

        Rect itemRect = GUILayoutUtility.GetRect(Screen.width, 20);
        itemRect.height = 17;
        itemRect.width = 60;
        GUI.Label(itemRect, label);
        centeredStyle.alignment = TextAnchor.UpperLeft;

        itemRect.width = Screen.width - 90;
        itemRect.x = 75;
        return EditorGUI.TextField(itemRect, value);
    }

    private void DrawModifier(Modifier modifier)
    {
        Rect itemRect = GUILayoutUtility.GetRect(Screen.width, 20);
        modifier.GUIDraw(itemRect);

        itemRect.width = 30;
        itemRect.x = Screen.width - 40;
        if (GUI.Button(itemRect, "x"))
        {
            itemStats.Remove(modifier);
        }
    }

    private string SelectFromDataBaseWithLabel(string[] data, string selected, string label, Object database)
    {
        var centeredStyle = GUI.skin.label;
        centeredStyle.alignment = TextAnchor.UpperRight;

        Rect itemRect = GUILayoutUtility.GetRect(Screen.width, 20);
        itemRect.height = 17;
        itemRect.width = 60;
        GUI.Label(itemRect, label);
        centeredStyle.alignment = TextAnchor.UpperLeft;

        itemRect.width = Screen.width - 210;
        itemRect.x = 75;

        int current = -1;
        for (int i = 0; i < data.Length; i++)
        {
            if (data[i] == selected)
            {
                current = i;
                break;
            }
        }
        current = EditorGUI.Popup(itemRect, current, data);

        itemRect.width = 115;
        itemRect.x = Screen.width - 130;

        if (GUI.Button(itemRect, "select database"))
            Selection.activeObject = database;
        if (current > 0 && current < data.Length)
            return data[current];
        if (data.Length > 0)
            return data[0];
        return "Database is empty!";
    }

    private void DisplayModifiers()
    {
        //showModifiers = EditorGUILayout.Foldout(showModifiers, "Modifiers");
        //if (!showModifiers)
        //    return; 

        if (GUILayout.Button("Reload modifiers"))
            ReloadModifierTypes();

        Rect rect = GUILayoutUtility.GetRect(Screen.width, 20);
        rect.height = 15;
        rect.width -= 80;
        currentModifier = EditorGUI.Popup(rect, currentModifier, modifierTypeStrings);
        rect.width = 70;
        rect.x = Screen.width - 75;
        if (GUI.Button(rect, "Add"))
        {
            itemStats.Add((Modifier)Activator.CreateInstance(modifiersTypes[currentModifier].Assembly.FullName, modifiersTypes[currentModifier].FullName).Unwrap());
        }

        for (int i = 0; i < itemStats.Count; i++)
        {
            DrawModifier(itemStats[i]);
        }
    }
}
