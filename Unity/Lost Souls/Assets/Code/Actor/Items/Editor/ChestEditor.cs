﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(ItemChest))]
public class ChestEditor : Editor
{
    private ItemChest chest;

    public void OnEnable()
    {
        chest = (ItemChest)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        Undo.RecordObject(target, "Changed Item");

        for (int i = 0; i < chest.InitItems.Length; i++)
        {
            DrawItem(ref chest.InitItems[i]);
        }

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
           EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
        }

        base.OnInspectorGUI();
    }

    private void DrawItem(ref int itemIndex)
    {
        itemIndex = SelectFromDataBaseWithLabel(ItemManager.ItemList.GetItemNames(), itemIndex, "Item");
    }

    private int SelectFromDataBaseWithLabel(string[] data, int selected, string label)
    {
        var centeredStyle = GUI.skin.label;
        centeredStyle.alignment = TextAnchor.UpperRight;

        Rect itemRect = GUILayoutUtility.GetRect(Screen.width - 40, 30);
        itemRect.y += 6;
        itemRect.height = 17;
        itemRect.width = 60;
        GUI.Label(itemRect, label);
        centeredStyle.alignment = TextAnchor.UpperLeft;

        itemRect.width = Screen.width - 130;
        itemRect.x = 75;

        selected = EditorGUI.Popup(itemRect, selected, data);

        itemRect.height = 28;
        itemRect.y -= 6;
        itemRect.width = 28;
        itemRect.x = Screen.width - 50;

        GUI.Box(itemRect, ItemManager.ItemList[selected].GetTexture());

        if (selected > 0 && selected < data.Length)
            return selected;
        if (data.Length > 0)
            return 0;
        return -1;
    }
}
