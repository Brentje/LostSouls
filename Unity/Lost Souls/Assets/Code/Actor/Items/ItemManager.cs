﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class ItemManager
{
    // Keep track of the instances
    private static ItemList itemList;
    private static TextureDatabase textureDatabase;
    private static PrefabDatabase prefabDatabase;

    // Accesors for the Lists

    /// <summary>List with all predefined items</summary>
    public static ItemList ItemList => itemList ?? (itemList = Resources.Load<ItemList>("Configs/ItemDB"));
    /// <summary>List with all Textures</summary>
    public static TextureDatabase TextureDatabase => textureDatabase ?? (textureDatabase = Resources.Load<TextureDatabase>("Configs/TextureDB"));
    /// <summary>List with references to prefabs</summary>
    public static PrefabDatabase PrefabDatabase => prefabDatabase ?? (prefabDatabase = Resources.Load<PrefabDatabase>("Configs/PrefabDB"));
}