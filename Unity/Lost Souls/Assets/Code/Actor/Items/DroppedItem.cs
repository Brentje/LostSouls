﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class DroppedItem : Interactable
{
    [Multiline] public string Item;

    public void Start()
    {
        SubscribeToInteraction(OnInteract);
        Item = Item.Replace("\\n", "\n").Replace("\\t", "\t");
    }

    public void OnInteract(Actor actor)
    {
        if (actor.GetType() == typeof(Player))
        {
            Item item = global::Item.CreateItemFromString(Item);
            Player player = actor as Player;
            if (player != null && player.Inventory.CheckForSpaceForItem(item))
            {
                player.Inventory.AddItemToInventory(item);
                Destroy(gameObject);
            }
        }
    }
}
