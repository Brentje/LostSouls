﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains a list of Items that can be edited from the Editor
/// </summary>
[CreateAssetMenu(fileName = "ItemDB", menuName = "Data/ItemList")]
public class ItemList : ScriptableObject
{
    // List of Items
    public List<Item> Items;

    public ItemList()
    {
        // Create the instance of the item list
        Items = new List<Item>();
    }

    // Shortens the code line when accesing the list through this class
    /// <summary>
    /// Get an item from the list at the given index
    /// </summary>
    /// <param name="x">Item index</param>
    public Item this[int x]
    {
        get { return Items[x]; }
        set { Items[x] = value; }
    }

    public int Length => Items.Count;

#if UNITY_EDITOR
    /// <summary>
    /// Create an array containing the names of the items - EDITOR ONLY
    /// </summary>
    public string[] GetItemNames()
    {
        string[] names = new string[Items.Count];
        // Go through all items and add there name to the list
        for (var i = 0; i < Items.Count; i++)
            names[i] = Items[i].Name;

        return names;
    }
#endif
    // Quicker access to the remove function
    /// <summary>
    /// Remove the Item from the list
    /// </summary>
    public void Remove(Item item)
    {
        Items.Remove(item);
    }

    // Quicker access to the Add function
    /// <summary>
    /// Adds the Item to the list
    /// </summary>
    public void Add(Item item)
    {
        Items.Add(item);
    }
}