﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class Actor : MonoBehaviour
{
    [HideInInspector] public bool IsInteracting = false;

    public UnityAction OnDeath;

    public StatList Stats;
    protected List<StatusEffect> StatusEffects;

    public NavMeshAgent Agent { get; private set; }

    protected virtual void Awake()
    {
        ActorManager.Register(this);
        Agent = GetComponent<NavMeshAgent>();
        Stats = new StatList();

        StatData stat = new StatData(200, 200, false);
        stat.OnValueChanged += value =>
        {
            if (value <= 0)
                OnDeath?.Invoke();
        };
        Stats[StatType.Health] = stat;

        if (!Stats.ContainsStat(StatType.AttackSpeed))
            Stats.AddStat(StatType.AttackSpeed, new StatData(1, 100, false));
        StatusEffects = new List<StatusEffect>();

        Stats[StatType.MovementSpeed] = new StatData(5, 10, false);

        Stats[StatType.MovementSpeed].OnValueChanged += x =>
        {
            Agent.speed = x;
            Agent.angularSpeed = x * 100;
            Agent.acceleration = x * 4;
        };
    }

    protected virtual void Update()
    {
        HandleStatusEffects();
    }

    public virtual void TakeDamage(float damage, AttackType attackType)
    {
        Stats[StatType.Health].Value -= damage;
    }

    public void AddStatusEffect(StatusEffect statusEffectToAdd)
    {
        if (StatusEffects == null)
            return;
        StatusEffects.Add(statusEffectToAdd);
    }

    public void RemoveStatusEffect(StatusEffect statusEffectToRemove)
    {
        statusEffectToRemove.Revert();
        StatusEffects.Remove(statusEffectToRemove);
    }

    private void HandleStatusEffects()
    {
        if (StatusEffects == null)
            return;

        for (int i = 0; i < StatusEffects.Count; i++)
            StatusEffects[i].Apply();
    }

    public static Vector3 GetRandomPointWithinRange(Vector3 center, float range)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1f, NavMesh.AllAreas))
                return hit.position;
        }
        return center;
    }

    private void OnDestroy()
    {
        ActorManager.UnRegister(this);
    }

    public void MoveTowards(Vector3 targetPosition)
    {
        Agent.SetDestination(targetPosition);
    }
}

public enum AttackType
{
    Fire,
    Cold,
    Lighning,
    Earth
}