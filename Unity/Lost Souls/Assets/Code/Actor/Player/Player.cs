﻿using StateMachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public enum PlayerClass
{
    Warrior,
    Archer,
    Mage
}

public class Player : Actor
{
    public CameraController CameraController;
    public CameraController CameraPrefab;
    public PlayerClass Class;

    public Equipment Equipment;
    private float experience;
    private Interactable focussedInteractable;
    public ItemContainer Inventory;
    public int Level;
    public string Name;

    public VoidDelegate OnExperienceGained;

    private UnityEvent OnQuestChange;
    public PassiveSkillSystem PassiveSkillSystem;

    public LayerMask PointClickRaycastMask;
    private Quest quest;

    private BrindingShot test;

    public float Experience
    {
        get { return experience; }
        set
        {
            experience = value;
            if (experience > Mathf.Pow(2, Level))
            {
                experience -= Mathf.Pow(2, Level);
                Level++;
            }
            OnExperienceGained?.Invoke();
        }
    }

    public AbilitySystem AbilitySystem { get; private set; }

    public KeyMap KeyMap { get; private set; }
    public UIStack UiStack { get; private set; }

    public Quest Quest
    {
        get { return quest; }
        set
        {
            quest = value;
            OnQuestChange?.Invoke();
        }
    }


    protected override void Awake()
    {
        base.Awake();

        Stats[StatType.Strength] = new StatData(30, 30, false); // TODO: replace

        Inventory.ChangeMaxWeight((int)Stats[StatType.Strength].Value);

        SlownessEffect slowness = new SlownessEffect(this, -4);

        Inventory.OnOverMaxWeight += () => { AddStatusEffect(slowness); };
        Inventory.OnUnderMaxWeight += () => { RemoveStatusEffect(slowness); };

        AbilitySystem = new AbilitySystem();
        PassiveSkillSystem = new PassiveSkillSystem();
        Equipment = new Equipment();

        test = new BrindingShot(this, "BrindingShot");

        OnDeath += () =>
        {
            enabled = false;
            UiStack.Push("DeathScreen");
            OnDeath = null;
        };

        SceneManager.AddSceneLoadedDelegate((x, y) =>
        {
            if (x.name != "MainMenu")
            {
                SetUIStack(FindObjectOfType<UIStack>());
                UiStack.Push("HUD");
            }
        });
    }

    public void CastBaseAttackOnClosestEnemy()
    {
        Actor target = ActorManager.GetClosestActor(this, 5 * 5);
        AbilitySystem.CastBaseAttack(Equipment.EquipmentSlotType.Weapon1, target);
    }

    private void Start()
    {
        Quest MainQuestPrototype = new Quest
        {
            Name = "The old man's lost son.",
            Description = "The old man in the town lost his son. " +
                          "He said his son went into the forest and hasn't come back." +
                          "Help the old man find his son.",
            type = QuestType.MAIN
        };

        MainQuestPrototype.AddTask(new QuestTask
        {
            Description = "Go to the forest."
        });

        Quest = MainQuestPrototype;
    }

    public void SetUIStack(UIStack newUIStack)
    {
        if (newUIStack == null || UiStack != null)
            return;

        UiStack = newUIStack;
    }

    public void SetKeyMap(ref KeyMap keyMap)
    {
        if (KeyMap != null)
            KeyMap = null;

        keyMap.AssignInput(this);

        KeyMap = keyMap;
    }

    protected override void Update()
    {
        base.Update();
        KeyMap?.Update();
        AbilitySystem.UpdateAbilities();

        if (Input.GetKeyDown(KeyCode.L))
            AbilitySystem.LearnAbility(test);

        if (Input.GetKeyDown(KeyCode.M))
            AbilitySystem.AddSelectedAbility(test);

        if(Equipment.StatsAreDirty || PassiveSkillSystem.StatsAreDirty)
            RecalculateStats();
    }

    public void LeftClickDown()
    {
        Ray ray = CameraController.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, PointClickRaycastMask) && !EventSystem.current.IsPointerOverGameObject())
        {
            Interactable interactable = hit.transform.GetComponent<Interactable>();
            if (interactable != null)
                interactable.TrackActor(this);
            else if (focussedInteractable != null)
                focussedInteractable.StopTrackingActor();

            focussedInteractable = interactable;
        }
    }

    public void LeftClick()
    {
        Ray ray = CameraController.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, PointClickRaycastMask) && !EventSystem.current.IsPointerOverGameObject())
            Agent.SetDestination(hit.point);
    }


    private void OnDestroy()
    {
        if (UiStack)
            UiStack.Remove("HUD");
        if (CameraController)
            Destroy(CameraController.gameObject);
    }

    public void OpenMenu(string name)
    {
        UiStack.Push(name);
    }

    public void RecalculateStats()
    {
        for (int i = 0; i < StatusEffects.Count; i++)
        {
            StatusEffects[i].Revert();
        }

        // clear the values but dont remove the callbacks
        for (int i = 0; i < Stats.Data.Count; i++)
        {
            Stats.Data[i].Value.SetValueWithoutTrigger(0);
            Stats.Data[i].Value.MaxValue = 0;
        }

        StatList percentageValues = Equipment.armorstatsPercent;

        PassiveSkillSystem.RecalculateStats();
        foreach (var stat in Equipment.armorstats.Data)
        {
            if (!Stats.ContainsStat(stat.Key))
                Stats.AddStat(stat.Key, new StatData());
            Stats[stat.Key].Value += stat.Value.Value;
            Stats[stat.Key].MaxValue += stat.Value.MaxValue;
        }
        foreach (var stat in PassiveSkillSystem.FlatStats.Data)
        {
            if (!Stats.ContainsStat(stat.Key))
                Stats.AddStat(stat.Key, new StatData());
            Stats[stat.Key].Value += stat.Value.Value;
            Stats[stat.Key].MaxValue += stat.Value.MaxValue;
        }
        foreach (var stat in percentageValues.Data)
        {
            if (Stats.ContainsStat(stat.Key))
                Stats[stat.Key].Value += Stats[stat.Key].Value / 100 * stat.Value.Value;
                Stats[stat.Key].MaxValue += Stats[stat.Key].MaxValue / 100 * stat.Value.MaxValue;
        }
        for (int i = 0; i < StatusEffects.Count; i++)
        {
            StatusEffects[i].Apply();
        }
        Equipment.StatsAreDirty = false;
        PassiveSkillSystem.StatsAreDirty = false;
        Debug.Log("Recalculated");
    }
}