﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PassiveSkillList))]
public class PassiveSkillInspector : Editor {
    public override void OnInspectorGUI() {
        if (GUILayout.Button("Editor"))
            PassiveSkillEditor.Init(target as PassiveSkillList);
    }
}