﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEditor;

public class AbilityEditingTool : EditorWindow
{
    private List<Ability> abilities;
    private string abilityName = "";
    private string abilityDescription = "";
    private string abilityIconSourcePath;
    private float abilityCost = 0;
    private float abilityCooldown = 0;
    private bool abilityIsToggle = false;
    private AbilityWindowState currentWindowState;
    private int selectedAbility = -1;
    private bool hasSelectedAbility = false;
    private Vector2 abilityListScrollview = Vector2.zero;

    private enum AbilityWindowState
    {
        DEFAULT,
        CREATE,
        EDIT
    }

    [MenuItem("Window/Tools/AbilityEditingTool")]
    private static void Init()
    {
        //Get the editor window
        AbilityEditingTool window = (AbilityEditingTool)GetWindow(typeof(AbilityEditingTool));

        //Set the title of the window
        window.titleContent = new GUIContent("Ability Editing Tool");

        //Show the window
        window.Show();
    }

    void Awake()
    {
        abilities = DataManager.LoadXML<List<Ability>>(Application.dataPath + "AbilitiesConfig.xml");
    }

    private void OnGUI()
    {
        DrawItemList();

        switch (currentWindowState)
        {
            case AbilityWindowState.DEFAULT:
                RenderDefaultWindowLayout();
                break;
            case AbilityWindowState.CREATE:
                RenderCreateWindowLayout();
                break;
            case AbilityWindowState.EDIT:
                RenderEditWindowLayout();
                break;
        }
    }

    private void DrawItemList()
    {
        Rect scrollRect = GUILayoutUtility.GetRect(Screen.width, 300);
        GUI.Box(scrollRect, "");

        GUIStyle buttonStyle = new GUIStyle(GUI.skin.label);

        abilityListScrollview = GUI.BeginScrollView(scrollRect, abilityListScrollview, new Rect(-5, 0, Screen.width - 45, abilities.Count * 20 + 5));
        for (int i = 0; i < abilities.Count; i++)
        {
            if (i == selectedAbility)
            {
                buttonStyle = new GUIStyle(GUI.skin.box)
                {
                    alignment = TextAnchor.UpperLeft
                };
            }

            Rect nameRect = new Rect(30, i * 20 + 5, Screen.width - 100, 20);

            if (GUI.Button(nameRect, abilities[i].AbilityName, buttonStyle))
            {
                selectedAbility = i;
                hasSelectedAbility = false;
                GUI.FocusControl(null);
            }

            if (i == selectedAbility)
                buttonStyle = new GUIStyle(GUI.skin.label);

            nameRect.x = Screen.width - 65;
            nameRect.height = 17;
            nameRect.width = 18;

            if (GUI.Button(nameRect, "X"))
            {
                abilities.Remove(abilities[i]);
                if (i == selectedAbility)
                    selectedAbility = -1;
                if (i < selectedAbility)
                    selectedAbility--;
                break;
            }
        }
        GUI.EndScrollView();
    }

    private void RenderDefaultWindowLayout()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Create Ability"))
            currentWindowState = AbilityWindowState.CREATE;

        if (GUILayout.Button("Edit Ability"))
            currentWindowState = AbilityWindowState.EDIT;
        GUILayout.EndHorizontal();

    }

    private void RenderCreateWindowLayout()
    {
        GUILayout.Label("Name");
        abilityName = GUILayout.TextArea(abilityName);

        GUILayout.Label("Description");
        abilityDescription = GUILayout.TextArea(abilityDescription);

        GUILayout.Label("Ability Icon Source Path");
        abilityDescription = GUILayout.TextArea(abilityIconSourcePath);

        GUILayout.Label("Cost");
        abilityCost = float.Parse(GUILayout.TextArea(abilityCost.ToString()));

        GUILayout.Label("Cooldown");
        abilityCooldown = float.Parse(GUILayout.TextArea(abilityCooldown.ToString()));
        abilityIsToggle = GUILayout.Toggle(abilityIsToggle, "IsToggle");


        if (GUILayout.Button("Add"))
        {
            abilities.Add(new Ability()
            {
                AbilityName = abilityName,
                Description = abilityDescription,
                IconSourcePath = abilityIconSourcePath,
                Cost = abilityCost,
                Cooldown = abilityCooldown,
                IsToggle = abilityIsToggle
            });

            abilityName = "";
            abilityDescription = "";
            abilityIconSourcePath = "";
            abilityCost = 0;
            abilityCooldown = 0;
            abilityIsToggle = false;

            SaveAbilities();
        }

        if (GUILayout.Button("Back"))
            currentWindowState = AbilityWindowState.DEFAULT;
    }

    private void RenderEditWindowLayout()
    {
        if (selectedAbility != -1 && hasSelectedAbility == false)
        {
            hasSelectedAbility = true;
            abilityName = abilities[selectedAbility].AbilityName;
            abilityDescription = abilities[selectedAbility].Description;
            abilityIconSourcePath = abilities[selectedAbility].IconSourcePath;
            abilityCost = abilities[selectedAbility].Cost;
            abilityCooldown = abilities[selectedAbility].Cooldown;
            abilityIsToggle = abilities[selectedAbility].IsToggle;
        }

        GUILayout.Label("Name");
        abilityName = GUILayout.TextArea(abilityName);

        GUILayout.Label("Description");
        abilityDescription = GUILayout.TextArea(abilityDescription);

        GUILayout.Label("Ability Icon Source Path");
        abilityDescription = GUILayout.TextArea(abilityIconSourcePath);

        GUILayout.Label("Cost");
        abilityCost = float.Parse(GUILayout.TextArea(abilityCost.ToString()));

        GUILayout.Label("Cooldown");
        abilityCooldown = float.Parse(GUILayout.TextArea(abilityCooldown.ToString()));
        abilityIsToggle = GUILayout.Toggle(abilityIsToggle, "IsToggle");

        if (GUILayout.Button("Save Changes"))
        {
            abilities[selectedAbility].AbilityName = abilityName;
            abilities[selectedAbility].Description = abilityDescription;
            abilities[selectedAbility].IconSourcePath = abilityIconSourcePath;
            abilities[selectedAbility].Cost = abilityCost;
            abilities[selectedAbility].Cooldown = abilityCooldown;
            abilities[selectedAbility].IsToggle = abilityIsToggle;

            SaveAbilities();
        }

        if (GUILayout.Button("Back"))
            currentWindowState = AbilityWindowState.DEFAULT;
    }

    private void SaveAbilities()
    {
        if (abilities == null)
        {
            Debug.LogError("AbilityList is null");
            return;
        }

        DataManager.SaveXML(Application.dataPath + "AbilitiesConfig.xml", abilities);
    }
}
