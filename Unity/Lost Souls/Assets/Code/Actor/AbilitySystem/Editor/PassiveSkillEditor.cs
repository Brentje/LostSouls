﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utility;

public class PassiveSkillEditor : EditorWindow {
    public PassiveSkillList List;

    public string Search = "";

    private List<PassiveSkillNode> results;
    private Vector2 searchScrollPosition;
    private Vector2 statScrollPosition;

    [SerializeField]
    private PassiveSkillNode editPassiveSkill;

    private bool IsShowingMenu = true;
    private bool IsShowingSearch = false;
    private bool IsShowingEdit = false;

    private StatType selectedStatType = StatType.Health;

    public static void Init(PassiveSkillList list) {
        PassiveSkillEditor window = (PassiveSkillEditor)GetWindow(typeof(PassiveSkillEditor));
        window.titleContent = new GUIContent("Passive Skill Editor");
        window.List = list;

        window.Show();
    }

    private void OnGUI() {
        if (IsShowingMenu)
            ShowMenu();
        else if (IsShowingSearch)
            ShowSearchField();
        else if (IsShowingEdit)
            ShowEditWindow();
    }

    public void ShowMenu() {
        EditorGUILayout.LabelField("Options");

        Separator();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add")) {
            IsShowingMenu = false;
            IsShowingEdit = true;
            editPassiveSkill = List.AddPassiveSkill();
        }

        if (GUILayout.Button("Search")) {
            IsShowingMenu = false;
            IsShowingSearch = true;
            SearchPassiveSkill(Search, out results);
        }
        EditorGUILayout.EndHorizontal();
    }

    public void ShowSearchField() {
        PassiveSkillNode markedPassiveSkillNode = null;

        if (GUILayout.Button("Menu")) {
            IsShowingSearch = false;
            IsShowingMenu = true;
        }

        EditorGUILayout.BeginHorizontal();
        Search = GUILayout.TextField(Search, GUILayout.MinWidth(200));

        if (GUILayout.Button("Search", GUILayout.MaxWidth(78)))
            SearchPassiveSkill(Search, out results);
        EditorGUILayout.EndHorizontal();

        if (results == null)
            return;

        searchScrollPosition = EditorGUILayout.BeginScrollView(searchScrollPosition);
        foreach (PassiveSkillNode passiveSkillNode in results) {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(passiveSkillNode.PassiveSkill.Name);
            if (GUILayout.Button("Edit", GUILayout.Width(50))) {
                editPassiveSkill = passiveSkillNode;
                IsShowingSearch = false;
                IsShowingEdit = true;
            }
            if (GUILayout.Button("X", GUILayout.Width(24))) {
                markedPassiveSkillNode = passiveSkillNode;
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();

        if (markedPassiveSkillNode != null) {
            List.RemovePassiveSkill(markedPassiveSkillNode.PassiveSkill.Name);
            results.Remove(markedPassiveSkillNode);
        }
    }

    public void ShowEditWindow() {
        StatDataPair markedStat = null;

        PassiveSkillNode newPassiveSkillNode = new PassiveSkillNode(editPassiveSkill.ID);
        newPassiveSkillNode = editPassiveSkill;

        EditorGUILayout.LabelField("Passive Skill Information");
        EditorGUILayout.Separator();
        
        // Name
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Passive Skill");
        newPassiveSkillNode.PassiveSkill.Name =
            EditorGUILayout.TextField(newPassiveSkillNode.PassiveSkill.Name);
        EditorGUILayout.EndHorizontal();

        // Description
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Description");
        newPassiveSkillNode.PassiveSkill.Description =
            EditorGUILayout.TextArea(newPassiveSkillNode.PassiveSkill.Description);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Is Unlocked");
        newPassiveSkillNode.IsUnlocked = EditorGUILayout.Toggle(newPassiveSkillNode.IsUnlocked);
        EditorGUILayout.EndHorizontal();

        // Icon
        EditorGUILayout.BeginHorizontal();
        newPassiveSkillNode.PassiveSkill.Icon =
            (Sprite) EditorGUILayout.ObjectField("Icon", newPassiveSkillNode.PassiveSkill.Icon, typeof(Sprite), false);
        EditorGUILayout.EndHorizontal();

        Separator();

        // Stats
        EditorGUILayout.LabelField("Stats");

        statScrollPosition = EditorGUILayout.BeginScrollView(statScrollPosition);
        foreach (StatDataPair statDataPair in newPassiveSkillNode.PassiveSkill.Stats.Data) {
            EditorGUILayout.BeginHorizontal();

            statDataPair.Key =
                (StatType) EditorGUILayout.EnumPopup(statDataPair.Key, GUILayout.Width(75), GUILayout.MaxWidth(125));

            EditorGUILayout.LabelField("Value", GUILayout.Width(40));
            statDataPair.Value.Value = EditorGUILayout.FloatField(statDataPair.Value.Value, GUILayout.MaxWidth(50));

            EditorGUILayout.LabelField("Max Value", GUILayout.Width(75));
            statDataPair.Value.MaxValue =
                EditorGUILayout.FloatField(statDataPair.Value.MaxValue, GUILayout.MaxWidth(50));

            EditorGUILayout.LabelField("%", GUILayout.Width(15));
            statDataPair.Value.IsPercentage = EditorGUILayout.Toggle(statDataPair.Value.IsPercentage);

            if (GUILayout.Button("Delete", GUILayout.MaxWidth(55)))
                markedStat = statDataPair;

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.Separator();

        EditorGUILayout.BeginHorizontal();
        selectedStatType = (StatType)EditorGUILayout.EnumFlagsField(selectedStatType);

        if (GUILayout.Button("Add", GUILayout.MaxWidth(53))) {
            for (int i = 0; i < Enum.GetValues(typeof(StatType)).Length; i++) {
                if (selectedStatType.Contains((StatType)(1 << i))) {
                    newPassiveSkillNode.PassiveSkill.Stats.Add((StatType)(1 << i), new StatData(0, 0, false));
                }
            }
        }

        EditorGUILayout.EndHorizontal();

        Separator();

        if (GUILayout.Button("Apply")) {
            List.EditPassiveSkill(editPassiveSkill, newPassiveSkillNode);
            IsShowingEdit = false;
            IsShowingSearch = true;
        }

        if (markedStat != null) 
            newPassiveSkillNode.PassiveSkill.Stats.Data.Remove(markedStat);
    }

    private void SearchPassiveSkill(string passiveSkill, out List<PassiveSkillNode> results) {
        results = new List<PassiveSkillNode>();

        foreach (PassiveSkillNode listPassiveSkill in List.PassiveSkills)
            if (listPassiveSkill.PassiveSkill.Name.Contains(passiveSkill))
                results.Add(listPassiveSkill);
    }

    private static void Separator() => EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
}
