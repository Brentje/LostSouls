﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

[CreateAssetMenu(fileName = "PassiveSkillDB", menuName = "Data/PassiveSkills")]
public class PassiveSkillList : ScriptableObject {
    public List<PassiveSkillNode> PassiveSkills = new List<PassiveSkillNode>();
    
    private static PassiveSkillList _instance;
    public static PassiveSkillList Instance {
        get {
            if (!_instance)
                _instance = Resources.Load<PassiveSkillList>("Configs/PassiveSkillDB");
            
            return _instance;
        }
    }

    public PassiveSkillNode AddPassiveSkill() {
        PassiveSkillNode newPassiveSkill = new PassiveSkillNode((uint)Instance.PassiveSkills.Count) {
            IsUnlocked = false,
            PassiveSkill = new PassiveSkill {
                Name = "NewPassiveSkill",
                Description = "",
                Stats = new StatList()
            }
        };

        Instance.PassiveSkills.Add(newPassiveSkill);

        return newPassiveSkill;
    }

    public void RemovePassiveSkill(string passiveSkillName) {
        if (Instance.PassiveSkills.Count == 0)
            return;

        PassiveSkillNode result = FindPassiveSkill(passiveSkillName);
        PassiveSkills.Remove(result);
    }

    public void EditPassiveSkill(PassiveSkillNode replaceTarget, PassiveSkillNode replaceWith) {
        PassiveSkillNode result = FindPassiveSkill(replaceTarget.PassiveSkill.Name);

        if (result != null)
            replaceTarget = replaceWith;
    }

    public PassiveSkillNode FindPassiveSkill(string name) {
        PassiveSkillNode cache = null;

        foreach (PassiveSkillNode passiveSkillNode in PassiveSkills)
            if (passiveSkillNode.PassiveSkill.Name == name) {
                cache = new PassiveSkillNode(passiveSkillNode.ID) {
                    PassiveSkill = passiveSkillNode.PassiveSkill,
                    IsUnlocked = passiveSkillNode.IsUnlocked
                };
            }
        return cache;
    }
}