﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using UnityEngine;

/// <summary>
/// Stores the xml and an instance of all behaviour trees
/// </summary>
[CreateAssetMenu(fileName = "Abilities", menuName = "Data/Ability dictionary")]
public class AbilityBehaviourDatabase : ScriptableObject
{
    /// <summary>
    /// When the object is loaded it creates an instance of all behaviour trees
    /// </summary>
    private void OnEnable()
    {
        Load();
    }

    private static AbilityBehaviourDatabase database;
    private static Dictionary<string, BehaviourTree> nodes;

    /// <summary>
    /// Return the database and when its null it will load the database
    /// </summary>
    public static AbilityBehaviourDatabase Database
    {
        get
        {
            if (database != null)
                return database;
            Load();
            return database;
        }
    }
    /// <summary>
    /// Return the dictionary with all instances in it and when its null it will create it
    /// </summary>
    public static Dictionary<string, BehaviourTree> Nodes
    {
        get
        {
            if (nodes != null)
                return nodes;
            Load();
            return nodes;
        }
    }

    /// <summary>
    /// Get a behaviour tree from the database
    /// </summary>
    /// <param name="name">key</param>
    public static BehaviourTree GetNode(string name)
    {
        return Nodes[name];
    }

    private static void Load()
    {
        database = Resources.Load<AbilityBehaviourDatabase>("AbilitiesBehaviourTrees/Abilities");
        nodes = new Dictionary<string, BehaviourTree>();

        foreach (var keyvalue in database.Abilities)
        {
            nodes.Add(keyvalue.Key, (BehaviourTree)DataManager.DeSerializeFromXml<BTNode>(keyvalue.Value));
        }
    }

    public AbilityBehaviorDictionary Abilities;
}


[Serializable]
public class AbilityBehaviorDictionary : SerializableDictionary<string, string>
{
}