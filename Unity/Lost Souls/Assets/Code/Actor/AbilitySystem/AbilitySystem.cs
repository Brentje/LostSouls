﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilitySystem
{
    public List<Ability> SelectedAbilities { get; private set; }
    public List<Ability> LearnedAbilities { get; private set; }

    public UnityEvent OnAbilityListChange = new UnityEvent();

    public Player player;

    public AbilitySystem()
    {
        SelectedAbilities = new List<Ability>();
        LearnedAbilities = new List<Ability>();
    }

    public void LearnAbility(Ability abilityToLearn) //TODO: Make this load an ability out of a DB
    {
        if (abilityToLearn != null && !LearnedAbilities.Contains(abilityToLearn))
        {
            LearnedAbilities.Add(abilityToLearn);
            OnAbilityListChange.Invoke();
        }
    }

    public void CastBaseAttack(Equipment.EquipmentSlotType type, Actor target)
    {
        if (!player)
            player = ActorManager.GetActivePlayer();
        Item item = player.Equipment.Slots[(int)type].GetCurrentItem();
        if (item == null || target == null)
            return;
        WeaponModifier weaponModifier = item.GetModifier<WeaponModifier>();
        if (weaponModifier != null)
        {
            weaponModifier.Attack(target);
            player.Experience += 1;
        }
    }

    public void SetAbilityAtIndex(uint selectedAbilitiesindex, uint learnedAbilitiesIndex)
    {
        if (selectedAbilitiesindex >= SelectedAbilities.Count || learnedAbilitiesIndex >= LearnedAbilities.Count)
            return;

        SelectedAbilities[(int)selectedAbilitiesindex] = LearnedAbilities[(int)learnedAbilitiesIndex];
        OnAbilityListChange.Invoke();
    }

    public void AddSelectedAbility(uint abilityIndex)
    {
        if (abilityIndex >= LearnedAbilities.Count)
            return;

        SelectedAbilities.Add(LearnedAbilities[(int)abilityIndex]);
        OnAbilityListChange.Invoke();
    }

    public void AddSelectedAbility(Ability abilityToSelect)
    {
        if (!LearnedAbilities.Contains(abilityToSelect) || SelectedAbilities.Contains(abilityToSelect))
            return;

        SelectedAbilities.Add(LearnedAbilities[LearnedAbilities.IndexOf(abilityToSelect)]);
        OnAbilityListChange.Invoke();
    }

    public void RemoveSelectedAbility(Ability abilityToRemove)
    {
        if (!SelectedAbilities.Contains(abilityToRemove))
            return;

        SelectedAbilities.Remove(abilityToRemove);
        OnAbilityListChange.Invoke();
    }

    public void RemoveSelectedAbility(uint abilityToRemove)
    {
        if (abilityToRemove >= SelectedAbilities.Count)
            return;

        SelectedAbilities.RemoveAt((int)abilityToRemove);
        OnAbilityListChange.Invoke();
    }

    public void CastAbility(uint abilityIndex)
    {
        if (abilityIndex >= SelectedAbilities.Count)
            return;

        SelectedAbilities[(int)abilityIndex].CastAbility();
    }

    public void UpdateAbilities()
    {
        for (int i = 0; i < SelectedAbilities.Count; i++)
        {
            if (SelectedAbilities[i].IsActive)
                SelectedAbilities[i].RunAbilityBehaviour();

            if (SelectedAbilities[i].IsOnCooldown)
                SelectedAbilities[i].UpdateCooldown();
        }
    }
}
