﻿public class AttackAbility : Ability
{
    private Actor target;

    public AttackAbility(Actor actor, string behaviourTree, Actor target) : base(actor, behaviourTree)
    {
        this.target = target;
        IsCastable = true;
    }

    // Overrides the base behaviour by adding the target to the blackboard
    protected override BehaviourTree CreateBehaviourTree()
    {
        BehaviourTree tree = base.CreateBehaviourTree();
        var b = tree.GetBlackboard();
        b.SetActor("Target", target);
        tree.SetBlackboard(b);
        return tree;
    }

    // Overrides the base behaviour to prefent the behaviour from stopping
    public override void RunAbilityBehaviour()
    {
        for (int i = 0; i < abilityBehaviourTree.Count; i++)
        {
            abilityBehaviourTree[i].Tick();
        }
    }

    // Sets an actor for all behaviour trees
    public void SetActor(string key, Actor actor)
    {
        for (int i = 0; i < abilityBehaviourTree.Count; i++)
        {
            Blackboard b = abilityBehaviourTree[i].GetBlackboard();
            b.SetActor(key, actor);
            abilityBehaviourTree[i].SetBlackboard(b);
        }
    }
}
