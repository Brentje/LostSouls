﻿using UnityEngine;
using UnityEngine.Events;
using Utility;

[System.Serializable]
public class PassiveSkillNode {
    public PassiveSkill PassiveSkill;
    public bool IsUnlocked;

    public readonly uint ID;

    public PassiveSkillNode() { }
    public PassiveSkillNode(uint id) {
        ID = id;
        PassiveSkill = new PassiveSkill();
    }
}

[System.Serializable]
public class PassiveSkillSystem {
    public PassiveSkillNode[] PassiveSkills;
    public PassiveSkillNode ClassStats;

    public StatList FlatStats;
    public bool StatsAreDirty;

    public uint PassiveSkillPoints;
    public uint RefundTokens;

    public static UnityAction OnSkillPointUpdated;

    public PassiveSkillSystem() {
        PassiveSkills = new PassiveSkillNode[0];
        PassiveSkillPoints = 0;
        OnSkillPointUpdated = RecalculateStats;
    }

    public void Unlock(string passiveSkillName, bool useSkillPoint) {
        PassiveSkillNode node = PassiveSkillList.Instance.FindPassiveSkill(passiveSkillName);
        node.IsUnlocked = true;
        PassiveSkills = PassiveSkills.ExpandArray(ref node);

        if (!useSkillPoint)
            return;

        PassiveSkillPoints--;
        OnSkillPointUpdated?.Invoke();
    }

    public void SetClass(string playerClass) {
        PassiveSkillNode skill = PassiveSkillList.Instance.FindPassiveSkill(playerClass);

        if (skill != null)
            ClassStats = skill;
    }

    public PassiveSkillNode FindPassiveSkill(string name) {
        foreach (PassiveSkillNode passiveSkillNode in PassiveSkills) 
            if (passiveSkillNode.PassiveSkill.Name == name) 
                return passiveSkillNode;

        return null;
    }

    public void AddSkillPoint(int value) {
        PassiveSkillPoints = (uint)(PassiveSkillPoints + value);

        OnSkillPointUpdated?.Invoke();
    }

    public void RefundPassiveSkill(string name) {
        
    }

    public void RecalculateStats()
    {
        FlatStats = new StatList();
        AddNodeToStats(ClassStats);
        foreach (var node in PassiveSkills)
        {
            AddNodeToStats(node);
        }
    }

    private void AddNodeToStats(PassiveSkillNode node)
    {
        if(node?.PassiveSkill?.Stats != null)
        foreach (var stat in node.PassiveSkill.Stats.Data)
        {
            if(!FlatStats.ContainsStat(stat.Key))
                FlatStats.AddStat(stat.Key, new StatData());
            FlatStats[stat.Key].Value += stat.Value.Value;
            FlatStats[stat.Key].MaxValue += stat.Value.MaxValue;
        }
    }
}