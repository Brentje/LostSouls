﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PassiveSkill {
    public string Name;
    public string Description;
    public Sprite Icon;

    public StatList Stats;
}
