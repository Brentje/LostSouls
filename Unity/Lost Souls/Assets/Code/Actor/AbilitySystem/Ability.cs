﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class Ability
{
    protected List<BehaviourTree> abilityBehaviourTree;
    protected Actor caster;

    public string AbilityName;
    public string Description;
    public int MemoryValue;
    public string IconSourcePath;
    public float Cost;
    public bool IsToggle;
    public float Cooldown;
    protected float LastCastTime;

    [XmlIgnore]
    public bool IsActive { get; protected set; }
    [XmlIgnore]
    public bool IsCastable { get; protected set; }
    [XmlIgnore]
    public bool IsOnCooldown { get; protected set; }

    public Ability()
    {
        abilityBehaviourTree = new List<BehaviourTree>();
        IsCastable = true;
    }

    public Ability(Actor actor, string abilityName)
    {
        abilityBehaviourTree = new List<BehaviourTree>();
        caster = actor;

        this.AbilityName = abilityName;
        IsCastable = true;
    }

    public virtual void CastAbility()
    {
        if (IsCastable)
        {
            if (IsToggle)
            {
                if (IsActive)
                {
                    IsActive = false;

                    abilityBehaviourTree.Clear();
                    return;
                }
            }

            abilityBehaviourTree.Add(CreateBehaviourTree());

            IsActive = true;
            IsOnCooldown = true;
            LastCastTime = Time.time;
            IsCastable = false;
        }
    }

    protected virtual BehaviourTree CreateBehaviourTree()
    {
        if (caster != null)
            return new BehaviourTree(caster, AbilityBehaviourDatabase.GetNode(AbilityName).RootNode);

        return new BehaviourTree(AbilityBehaviourDatabase.GetNode(AbilityName).RootNode);
    }

    public virtual void RunAbilityBehaviour()
    {
        for (int i = 0; i < abilityBehaviourTree.Count;)
        {
            NodeStatus status = abilityBehaviourTree[i].Tick();

            if (status != NodeStatus.RUNNING)
            {
                Debug.Log(status);
                abilityBehaviourTree.RemoveAt(i);
                continue;
            }
            i++;
        }
    }

    public virtual void UpdateCooldown()
    {
        if (LastCastTime + Cooldown < Time.time)
        {
            IsOnCooldown = false;
            IsCastable = true;
        }
    }

    /// <summary>
    /// Returns a number from 0 to 1
    /// </summary>
    public float GetCooldownProgress()
    {
        return Mathf.Clamp01(Mathf.InverseLerp(LastCastTime, LastCastTime + Cooldown, Time.time));
    }
}

public class BrindingShot : Ability
{

    public BrindingShot(Actor actor, string behaviourTreeFilepath) : base(actor, behaviourTreeFilepath)
    {
        //Variable set
        IconSourcePath = "Textures/AbilityIcons/Brinding_Shot";
        AbilityName = "Brinding Shot";
        Description = "Very Brinding shoteru";
        Cooldown = 5;
        Cost = 10;
    }
}

