﻿using UnityEngine;

namespace StateMachine
{
    public class StateMachine<T>
    {
        private const float UpdateInterval = 0.2f;

        public StateDiagram<T> Diagram;

        private float timer;

        /// <summary>
        ///     Set the holder for the stateMachine.
        /// </summary>
        /// <param name="holder">The holder.</param>
        public StateMachine(T holder)
        {
            Holder = holder;
        }

        /// <summary>This is the class controled by the FSM.</summary>
        public T Holder { get; }

        public int CurrentState { get; private set; }

        public int LastState { get; private set; }

        /// <summary>
        ///     Update method. This is called every frame.
        /// </summary>
        public void Update()
        {
            timer += Time.deltaTime;
            if (timer > UpdateInterval && Diagram != null)
            {
                Diagram?.States?[CurrentState].Update();
                timer = 0;
            }
        }

        /// <summary>
        ///     Set the state of the state machine.
        /// </summary>
        /// <param name="state">New state.</param>
        public void SetState(int state)
        {
            //disable current state
            Diagram?.States[CurrentState].OnExit();

            //switch state
            LastState = CurrentState;
            CurrentState = state;

            //enable the new state
            Diagram?.States[CurrentState].OnEnter();
        }

        /// <summary>
        ///     Switch state with the last state.
        /// </summary>
        public void ReturnToPreviousState()
        {
            int temp = LastState;
            LastState = CurrentState;
            CurrentState = temp;
        }
    }
}