﻿namespace StateMachine
{
    /// <summary>
    ///     Base class for states.
    /// </summary>
    public abstract class State<T>
    {
        /// <summary>The state machine.</summary>
        protected StateMachine<T> Fsm;

        protected State(StateMachine<T> fsm)
        {
            Fsm = fsm;
        }

        protected T Holder => Fsm.Holder;

        /// <summary>This is called when the state is enabled.</summary>
        public virtual void OnEnter(){}
        /// <summary>This is called every update if the state is enabled.</summary>
        public virtual void Update() { }
        /// <summary>This is called when the state is disabled.</summary>
        public virtual void OnExit() { }
    }

    public delegate void VoidDelegate();
}