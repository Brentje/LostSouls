﻿using System.Collections.Generic;

namespace StateMachine
{
    public abstract class StateDiagram<T>
    {
        public Dictionary<int, State<T>> States = new Dictionary<int, State<T>>();
        protected StateMachine<T> StateMachine;

        public abstract void Init(StateMachine<T> stateMachine);

        public virtual void Update() { }
    }
}