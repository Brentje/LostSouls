﻿public class InputManager : Manager
{
    public static KeyMap KeyMap { get; private set; }

    public override void Awake()
    {
        KeyMap = new KeyMap();

        if (DataManager.Config == null)
            DataManager.LoadConfig();


        KeyMap.KeyBindings = new KeyBinding[(int)InputAction.MAX + 1];

        ResetKeys();
    }

    public static void ResetKeys()
    {
        KeyBindingData[] inputs = DataManager.Config.Input.KeyActions;
        for (int i = 0; i < inputs.Length; i++)
        {
            if (KeyMap.KeyBindings[(int)inputs[i].InputAction] == null)
                KeyMap.KeyBindings[(int)inputs[i].InputAction] = new KeyBinding
                {
                    KeyCode = inputs[i].KeyCode,
                    Axis = inputs[i].Axis
                };
            else
            {
                KeyMap.KeyBindings[(int)inputs[i].InputAction].KeyCode = inputs[i].KeyCode;
                KeyMap.KeyBindings[(int)inputs[i].InputAction].Axis = inputs[i].Axis;
            }
        }
    }
}