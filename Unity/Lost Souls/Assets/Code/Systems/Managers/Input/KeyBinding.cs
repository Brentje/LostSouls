﻿using System;
using UnityEngine;

public enum KeyState {
    Up,
    Down,
    Repeat
}

[System.Serializable]
public class KeyBinding {
    public string Axis;
    public int MouseButton;
    public KeyCode KeyCode;

    public Action Up;
    public Action Down;
    public Action Repeat;
    public Action<float> AxisAction;

    public KeyBinding() {
        Axis = "";
        MouseButton = -1;
        KeyCode = KeyCode.None;
    }

    public void Rebind(KeyCode newKey)
    {
        KeyCode = newKey;
    }

    public void HandleInput() {
        if (Up == null && Down == null && Repeat == null && AxisAction == null)
            return;

        if (!String.IsNullOrEmpty(Axis)) {
            float axis = Input.GetAxis(Axis);
            
            if (axis != 0) 
                AxisAction?.Invoke(axis);
        }
        else if (KeyCode != KeyCode.None) {
            if (Input.GetKeyDown(KeyCode)) 
                Down?.Invoke();
            else if (Input.GetKeyUp(KeyCode)) 
                Up?.Invoke();
            else if(Input.GetKey(KeyCode))
                Repeat?.Invoke();
        }
        else if (MouseButton > -1) {
            if (Input.GetMouseButtonDown(MouseButton))
                Down?.Invoke();
            else if (Input.GetMouseButtonUp(MouseButton))
                Up?.Invoke();
            else if (Input.GetMouseButton(MouseButton))
                Repeat?.Invoke();
        }
    }
}
