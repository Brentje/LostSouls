﻿using UnityEngine;

public enum InputAction
{
    LeftMouse,
    RightMouse,
    MouseScroll,

    UseSkill1,
    UseSkill2,
    UseSkill3,
    UseSkill4,
    UseSkill5,
    UseSkill6,
    UseSkill7,
    UseSkill8,
    UseSkill9,
    UseSkill10,

    UseFlask1,
    UseFlask2,
    UseFlask3,
    UseFlask4,
    UseFlask5,

    PauseMenu,
    Inventory,
    SkillBook,
    Character,
    PassiveSkillTree,
    WorldMap,


    MAX = WorldMap
}

[System.Serializable]
public class KeyMap
{
    public KeyBinding[] KeyBindings;

    public Vector3 MousePosition => Input.mousePosition;

    public void Update()
    {
        if (KeyBindings == null || KeyBindings.Length == 0)
            return;

        foreach (KeyBinding keyBinding in KeyBindings)
        {
            keyBinding.HandleInput();
        }
    }

    public void AssignInput(Player player)
    {
        KeyBindings[(int)InputAction.LeftMouse].Down = player.LeftClickDown;
        KeyBindings[(int)InputAction.LeftMouse].Repeat = player.LeftClick;
        KeyBindings[(int)InputAction.RightMouse].Down = () => { player.CastBaseAttackOnClosestEnemy(); };
        KeyBindings[(int)InputAction.MouseScroll].AxisAction = value => { player.CameraController.Zoom(value); };

        KeyBindings[(int)InputAction.UseSkill1].Down = () => { player.AbilitySystem.CastAbility(0); };
        KeyBindings[(int)InputAction.UseSkill2].Down = () => { player.AbilitySystem.CastAbility(1); };
        KeyBindings[(int)InputAction.UseSkill3].Down = () => { player.AbilitySystem.CastAbility(2); };
        KeyBindings[(int)InputAction.UseSkill4].Down = () => { player.AbilitySystem.CastAbility(3); };
        KeyBindings[(int)InputAction.UseSkill5].Down = () => { player.AbilitySystem.CastAbility(4); };
        KeyBindings[(int)InputAction.UseSkill6].Down = () => { player.AbilitySystem.CastAbility(5); };
        KeyBindings[(int)InputAction.UseSkill7].Down = () => { player.AbilitySystem.CastAbility(6); };
        KeyBindings[(int)InputAction.UseSkill8].Down = () => { player.AbilitySystem.CastAbility(7); };
        KeyBindings[(int)InputAction.UseSkill9].Down = () => { player.AbilitySystem.CastAbility(8); };
        KeyBindings[(int)InputAction.UseSkill10].Down = () => { player.AbilitySystem.CastAbility(9); };

        KeyBindings[(int)InputAction.UseFlask1].Down = () => { player.PassiveSkillSystem.AddSkillPoint(1); };
        KeyBindings[(int)InputAction.UseFlask2].Down = () => { player.PassiveSkillSystem.AddSkillPoint(-1); };
        KeyBindings[(int)InputAction.UseFlask3].Down = () => { };
        KeyBindings[(int)InputAction.UseFlask4].Down = () => { };
        KeyBindings[(int)InputAction.UseFlask5].Down = () => { };

        KeyBindings[(int)InputAction.PauseMenu].Down =        () => { GameManager.Instance.Pause(true); };
        KeyBindings[(int)InputAction.Inventory].Down =        () => { player.OpenMenu("Inventory"); };
        KeyBindings[(int)InputAction.SkillBook].Down =        () => { player.OpenMenu("SkillBook"); };
        KeyBindings[(int)InputAction.Character].Down =        () => { player.OpenMenu("CharacterPanel"); };
        KeyBindings[(int)InputAction.PassiveSkillTree].Down = () => { player.OpenMenu("PassiveSkillTree"); };
        KeyBindings[(int)InputAction.WorldMap].Down =         () => { player.OpenMenu("Map"); };
    }
}
