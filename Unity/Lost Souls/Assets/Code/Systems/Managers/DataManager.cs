﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UnityEngine;

public class DataManager : Manager
{
    private const bool EncryptGameSave = false;

    private const string ConfigFileName = "/settings.config";
    private const string GameSavesFolder = "/game_saves/";
    private const string GameSavesExtention = ".save";
    private const string EncryptionKey = "Lost Souls";

    public static GameConfig Config { get; private set; }
    private static GameSave _currentSave;

    private static string GetConfigPath() => Application.persistentDataPath + ConfigFileName;
    private static string GameSavePath(string characterName) => Application.persistentDataPath + GameSavesFolder + characterName + '/' + characterName + GameSavesExtention;

    // Save and load data

    public static void SaveConfig()
    {
        if (Config == null)
            return;

        SaveText(SerializeToXml(Config), GetConfigPath());
    }

    public static void LoadConfig()
    {
        if (File.Exists(GetConfigPath()))
            Config = DeSerializeFromXml<GameConfig>(ReadFile(GetConfigPath()));
        else
        {
            Config = new GameConfig();
            Config.LoadDefault();
        }
    }

    public static void SaveGame(string characterName)
    {
        if (_currentSave == null)
            _currentSave = new GameSave();
        SaveBytes(StringToBytes(SerializeToXml(_currentSave)), GameSavePath(characterName), EncryptGameSave);
    }

    public static void SetGameActive(GameSave activeGameSave)
    {
        _currentSave = activeGameSave;
    }

    public static GameSave[] LoadAllSaves()
    {
        string[] files = GetSavedCharacters();
        List<GameSave> validSaves = new List<GameSave>();
        if (files == null)
        {
            return validSaves.ToArray();
        }
        for (int i = 0; i < files.Length; i++)
        {
            if (File.Exists(GameSavePath(files[i])))
            {
                GameSave current = DeSerializeFromXml<GameSave>(BytesToSting(LoadBytes(GameSavePath(files[i]), EncryptGameSave)));
                validSaves.Add(current);
            }
        }
        return validSaves.ToArray();
    }

    public static string[] GetSavedCharacters()
    {
        if (!Directory.Exists(Application.persistentDataPath + GameSavesFolder))
            return null;

        string[] directories = Directory.GetDirectories(Application.persistentDataPath + GameSavesFolder);
        string[] characters = new string[directories.Length];

        for (int i = 0; i < directories.Length; i++)
        {
            string[] slittedPath = directories[i].Split('/', '\\');
            string character = slittedPath[slittedPath.Length - 1];

            characters[i] = character;
        }
        return characters;
    }

    public static void PurgeCharacter(string charactername)
    {
        string directory = Path.GetDirectoryName(GameSavePath(charactername));
        string[] files = Directory.GetFiles(directory, "*", SearchOption.AllDirectories);

        foreach (string file in files)
        {
            File.Delete(file);
        }

        Directory.Delete(directory);

        Debug.Log($"Purged {charactername}!");
    }

    // Add and Get data from save 

    public static void AddDataToSave<T>(string key, T value)
    {
        string data = String.Empty;
        if (typeof(T) == typeof(string))
            data = SerializeToXml(new Container<string>() { Data = value.ToString() });
        else
            data = SerializeToXml(value);
        if (_currentSave == null)
            _currentSave = new GameSave();
        if (!_currentSave.ContainsKey(key))
            _currentSave.Add(key, data);
        else
            _currentSave[key] = data;
    }

    public static T GetDataFromCurrentSave<T>(string key, out bool succes)
    {
        if (_currentSave.ContainsKey(key))
        {
            succes = true;
            if (typeof(T) == typeof(string))
                return DeSerializeFromXml<Container<T>>(_currentSave[key]).Data;
            return DeSerializeFromXml<T>(_currentSave[key]);
        }

        Debug.LogError($"key \"{key}\" not found in the game save!");
        succes = false;
        return default(T);
    }

    public static T GetDataFromCurrentSave<T>(string key)
    {
        if (_currentSave.ContainsKey(key))
        {
            if (typeof(T) == typeof(string))
                return DeSerializeFromXml<Container<T>>(_currentSave[key]).Data;
            return DeSerializeFromXml<T>(_currentSave[key]);
        }

        Debug.LogError($"key \"{key}\" not found in the game save!");
        return default(T);
    }

    public static T GetDataFromSave<T>(GameSave save, string key)
    {
        if (save.ContainsKey(key))
        {
            if (typeof(T) == typeof(string))
                return DeSerializeFromXml<Container<T>>(save[key]).Data;
            return DeSerializeFromXml<T>(save[key]);
        }

        Debug.LogError($"key \"{key}\" not found in the game save!");
        return default(T);
    }

    // Data convertion methods

    /// <summary>
    ///     Convert an <see cref="object" /> to xml
    /// </summary>
    /// <typeparam name="T">object type</typeparam>
    /// <param name="unSerializedObject"><see cref="object" /> to serialize</param>
    /// <returns></returns>
    public static string SerializeToXml<T>(T unSerializedObject)
    {
        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        ns.Add("", "");
        XmlSerializer serializer = new XmlSerializer(typeof(T), AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(currentType => currentType.IsSubclassOf(typeof(T))).ToArray());

        using (StringWriter textWriter = new StringWriter())
        {
            serializer.Serialize(textWriter, unSerializedObject, ns);
            string oldText = textWriter.ToString();
            int index = oldText.IndexOf(System.Environment.NewLine);
            string newText = oldText.Substring(index + System.Environment.NewLine.Length);
            return newText;
        }
    }

    /// <summary>
    ///     Convert xml to an <see cref="object" />
    /// </summary>
    /// <typeparam name="T"><see cref="object" /> to convert to</typeparam>
    /// <param name="xml">The xml string</param>
    /// <returns></returns>
    public static T DeSerializeFromXml<T>(string xml)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T), AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(currentType => currentType.IsSubclassOf(typeof(T))).ToArray());

        using (StringReader stringStream = new StringReader(xml))
        {
            return (T)serializer.Deserialize(stringStream);
        }
    }

    /// <summary>
    ///     Save a <see cref="string" /> to the given path
    /// </summary>
    /// <param name="text"><see cref="string" /> to save</param>
    /// <param name="path">File path</param>
    private static void SaveText(string text, string path)
    {
        string directory = Path.GetDirectoryName(path);
        if (!Directory.Exists(directory))
            if (directory != null)
                Directory.CreateDirectory(directory);
        using (StreamWriter writer = new StreamWriter(path))
        {
            writer.Write(text);
        }
    }

    /// <summary>
    ///     Load a <see cref="string" /> from a file
    /// </summary>
    /// <param name="path">File to read from</param>
    /// <returns></returns>
    public static string ReadFile(string path)
    {
        if (!File.Exists(path))
        {
            Debug.LogError("READ_FILE: path does not exist. \"" + path + "\"");
            return string.Empty;
        }

        using (StreamReader reader = new StreamReader(path))
        {
            return reader.ReadToEnd();
        }
    }

    /// <summary>
    ///     Save <see cref="byte" />[] to a file
    /// </summary>
    /// <param name="bytes"><see cref="byte" />[] to save</param>
    /// <param name="path">File to save it to</param>
    /// <param name="encrypt">Should the file be encrypted</param>
    private static void SaveBytes(byte[] bytes, string path, bool encrypt)
    {
        string directory = Path.GetDirectoryName(path);
        if (!Directory.Exists(directory))
            if (directory != null)
                Directory.CreateDirectory(directory);
        try
        {
            using (BinaryWriter writer = new BinaryWriter(new FileStream(path, FileMode.Create)))
            {
                writer.Write(encrypt ? Encrypt(bytes) : bytes);
            }
        }
        catch (IOException e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    /// <summary>
    ///     Load a <see cref="byte" />[] from a file
    /// </summary>
    /// <param name="path">File path to read from</param>
    /// <param name="decrypt">should the data be decrypted</param>
    /// <returns></returns>
    private static byte[] LoadBytes(string path, bool decrypt)
    {
        return decrypt ? Decrypt(File.ReadAllBytes(path)) : File.ReadAllBytes(path);
    }

    /// <summary>
    ///     Convert a <see cref="string" /> to a <see cref="byte" />[]
    /// </summary>
    /// <param name="str"><see cref="string" /> to convert to <see cref="byte" />[]</param>
    /// <returns></returns>
    private static byte[] StringToBytes(string str)
    {
        return Encoding.ASCII.GetBytes(str);
    }

    /// <summary>
    ///     Convert <see cref="byte" />[] to <see cref="string" />
    /// </summary>
    /// <param name="bytes"><see cref="byte" />[] to convert to a <see cref="string" /></param>
    /// <returns></returns>
    private static string BytesToSting(byte[] bytes)
    {
        return Encoding.ASCII.GetString(bytes);
    }

    /// <summary>
    ///     Encrypt a <see cref="byte" />[]
    /// </summary>
    /// <param name="bytes"><see cref="byte" />[] to encrypt</param>
    /// <returns></returns>
    private static byte[] Encrypt(byte[] bytes)
    {
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            if (encryptor == null)
                return bytes;
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytes, 0, bytes.Length);
                    cs.Close();
                }
                bytes = ms.ToArray();
            }
        }
        return bytes;
    }

    /// <summary>
    ///     Decrypt a <see cref="byte" />[]
    /// </summary>
    /// <param name="bytes"><see cref="byte" />[] to decrypt</param>
    /// <returns></returns>
    private static byte[] Decrypt(byte[] bytes)
    {
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            if (encryptor == null)
                return bytes;
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytes, 0, bytes.Length);
                    cs.Close();
                }
                bytes = ms.ToArray();
            }
        }
        return bytes;
    }

    public class Container<T>
    {
        public T Data;
    }

    //Save a xml file to a filepath
    public static void SaveXML<T>(string path, T type)
    {
        //Check if the string isnt empty
        if (path == null || path.IsEmptyOrWhiteSpace())
            return;

        //Create a serializer
        XmlSerializer serializer = new XmlSerializer(typeof(T), AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(currentType => currentType.IsSubclassOf(typeof(T))).ToArray());

        //Create a reader
        StreamWriter writer = new StreamWriter(path);

        //Serialize the instance of type T
        serializer.Serialize(writer, type);

        //Close the stream of the writer
        writer.Close();
    }

    //Load a xml file based on a filepath
    public static T LoadXML<T>(string path)
    {
        //Check if the file exists
        if (!File.Exists(path))
            return default(T);

        //Create a serializer
        XmlSerializer serializer = new XmlSerializer(typeof(T), AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(currentType => currentType.IsSubclassOf(typeof(T))).ToArray());

        //Create a reader
        StreamReader reader = new StreamReader(path);

        //Cache the instance that has been loaded
        T t = (T)serializer.Deserialize(reader);

        //Close the stream of the reader
        reader.Close();

        //Return the loaded type
        return t;
    }
}