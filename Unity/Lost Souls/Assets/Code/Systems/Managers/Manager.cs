﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager {
    public virtual void Awake() { }
    public virtual void Start() { }
    public virtual void Update() { }

    public virtual void Pause(bool value) { }

    public virtual void Save() { }
    public virtual void Load() { }
    public virtual void Exit() { }
}
