﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoadingScreen : UIElement
{
    public Image ProgressBar;

    public override void Enter()
    {
        StartCoroutine(LoadSceneAsync_Internal());
    }

    public override void Exit()
    {

    }

    private IEnumerator LoadSceneAsync_Internal()
    {
        yield return new WaitForEndOfFrame();
        AsyncOperation currentOperation = SceneManager.GetCurrentAsyncOperation();
        if (currentOperation == null)
            yield break;

        currentOperation.allowSceneActivation = false;
        yield return new WaitForEndOfFrame();

        while (!currentOperation.isDone)
        {
            ProgressBar.fillAmount = currentOperation.progress;
            yield return new WaitForEndOfFrame();

            if (currentOperation.progress >= 0.9f)
                currentOperation.allowSceneActivation = true;

        }

        uiStack.Remove(this);
    }
}