﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneManager : Manager
{
    private static Scene currentActiveScene;
    private static AsyncOperation currentAsync;

    public SceneManager()
    {
        currentAsync = new AsyncOperation();
    }

    public static Scene GetActiveScene()
    {
        return currentActiveScene;
    }

    public static bool SetActiveScene(Scene scene)
    {
        return scene != null && UnityEngine.SceneManagement.SceneManager.SetActiveScene(scene);
    }

    public static AsyncOperation GetCurrentAsyncOperation()
    {
        return currentAsync;
    }

    public static void LoadSceneAsync(int buildIndex, LoadSceneMode loadSceneMode = LoadSceneMode.Single, ForceInstanceMode forceInstanceMode = ForceInstanceMode.DontForce)
    {
        GameObject.FindObjectOfType<UIStack>().Push("LoadingScreen");
        currentAsync = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(buildIndex, loadSceneMode);
    }

    public static void LoadSceneAsync(string name, LoadSceneMode loadSceneMode = LoadSceneMode.Single, ForceInstanceMode forceInstanceMode = ForceInstanceMode.DontForce)
    {
        GameObject.FindObjectOfType<UIStack>().Push("LoadingScreen");
        currentAsync = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(name, loadSceneMode);
    }

    public static void LoadScene(int buildIndex, LoadSceneMode mode = LoadSceneMode.Single)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(buildIndex, mode);
    }

    public static void LoadScene(string name, LoadSceneMode mode = LoadSceneMode.Single)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name, mode);
    }

    public static void UnloadSceneAsync(int name)
    {
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(name);
    }

    public static void UnloadSceneAsync(string name)
    {
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(name);
    }

    public static void UnloadSceneAsync(Scene scene)
    {
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(scene);
    }

    public static void AddSceneLoadedDelegate(UnityAction<Scene, LoadSceneMode> callback) {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += callback;
    }

    public static void RemoveSceneLoadedDelegate(UnityAction<Scene, LoadSceneMode> callback) {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= callback;
    }
}

public enum GameScenes
{
    MainMenu,
    ForestEncampment,
    Forest,
    SpiderDen,
    SpiderQueensLair
}

public enum ForceInstanceMode
{
    Force,
    DontForce
}