﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
    //public string Title;
    //public string Description;
    //public int Experience;
    //public bool MustChoose;
    //public List<Item> RewardItems = new List<Item>();

    public string Name;
    public string Description;
    public QuestType type;
    private Queue<QuestTask> tasks;
    private List<QuestTask> completedTasks;

    public Quest()
    {
        tasks = new Queue<QuestTask>();
        completedTasks = new List<QuestTask>();
    }

    public QuestTask GetCurrentTask()
    {
        if (tasks.Count == 0)
            return null;

        return tasks.Peek();
    }

    public void NextTask()
    {
        if (tasks.Peek() == null)
            return;

        completedTasks.Add(tasks.Dequeue());
    }

    public void AddTask(QuestTask taskToAdd)
    {
        if (taskToAdd == null)
            return;

        tasks.Enqueue(taskToAdd);
    }
}

public class QuestTask
{
    public string Description;
}

public enum QuestType
{
    MAIN,
    SIDE
}