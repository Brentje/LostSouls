﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : Manager {
    public static Quest ActiveQuest { get; private set; }

    public override void Awake() {
        base.Awake();
    }

    public override void Pause(bool value) {
        base.Pause(value);
    }

    public override void Save() {
        base.Save();
    }

    public override void Start() {
        base.Start();
    }

    public override void Update() {
        base.Update();
    }

    public static Quest GetActiveQuest() {
        return ActiveQuest;
    }
}
