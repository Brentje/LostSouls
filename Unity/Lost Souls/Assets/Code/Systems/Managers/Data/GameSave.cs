﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class DataPair
{
    public string Key;
    public string Value;
}

public class GameSave
{
    public List<DataPair> Data;

    public GameSave()
    {
        Data = new List<DataPair>();
    }

    public void Add(string key, string value)
    {
        foreach (DataPair pair in Data)
        {
            if (pair.Key == key)
            {
                Debug.Log("Key allready prestent in dictionary");
                return;
            }
        }
        DataPair data = new DataPair { Key = key, Value = value };
        Data.Add(data);
    }

    public string this[string key]
    {
        get {
            foreach (DataPair dataPair in Data)
            {
                if (dataPair.Key == key)
                    return dataPair.Value;
            }
            throw new KeyNotFoundException("Given key not found!");
        }
        set
        {
            for (int i = 0; i < Data.Count; i++)
            {
                if (Data[i].Key == key)
                {
                    Data[i].Value = value;
                }
            }
        }
    }

    public bool ContainsKey(string key)
    {
        foreach (DataPair dataPair in Data)
        {
            if (dataPair.Key == key)
                return true;
        }
        return false;
    }
}