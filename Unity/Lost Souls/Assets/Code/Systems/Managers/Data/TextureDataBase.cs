﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "TextureDatabase", menuName = "Data/Texture database")]
public class TextureDatabase : ScriptableObject
{
    public TextureDictionary Textures;
}

[Serializable]
public class TextureDictionary : SerializableDictionary<string, Sprite>
{
}