﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "PrefabDatabase", menuName = "Data/Prefab database")]
public class PrefabDatabase : ScriptableObject
{
    public PrefabDictionary Prefabs;
}

[Serializable]
public class PrefabDictionary : SerializableDictionary<string, GameObject>
{
}