﻿using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.PostProcessing;

public class GameConfig
{
    public InputSettings Input;
    public VideoSettings Video;
    public AudioSettings Audio;

    public void LoadDefault()
    {
        Input.KeyActions = new[] {
            new KeyBindingData { KeyCode = KeyCode.Mouse0, InputAction = InputAction.LeftMouse},
            new KeyBindingData { KeyCode = KeyCode.Mouse1, InputAction = InputAction.RightMouse },
            new KeyBindingData { Axis = "Mouse ScrollWheel", InputAction = InputAction.MouseScroll },

            new KeyBindingData { KeyCode = KeyCode.Q, InputAction = InputAction.UseSkill1 },
            new KeyBindingData { KeyCode = KeyCode.W, InputAction = InputAction.UseSkill2 },
            new KeyBindingData { KeyCode = KeyCode.E, InputAction = InputAction.UseSkill3 },
            new KeyBindingData { KeyCode = KeyCode.R, InputAction = InputAction.UseSkill4 },
            new KeyBindingData { KeyCode = KeyCode.T, InputAction = InputAction.UseSkill5 },
            new KeyBindingData { KeyCode = KeyCode.A, InputAction = InputAction.UseSkill6 },
            new KeyBindingData { KeyCode = KeyCode.S, InputAction = InputAction.UseSkill7 },
            new KeyBindingData { KeyCode = KeyCode.D, InputAction = InputAction.UseSkill8 },
            new KeyBindingData { KeyCode = KeyCode.F, InputAction = InputAction.UseSkill9 },
            new KeyBindingData { KeyCode = KeyCode.G, InputAction = InputAction.UseSkill10 },

            new KeyBindingData { KeyCode = KeyCode.Alpha1, InputAction = InputAction.UseFlask1 },
            new KeyBindingData { KeyCode = KeyCode.Alpha2, InputAction = InputAction.UseFlask2 },
            new KeyBindingData { KeyCode = KeyCode.Alpha3, InputAction = InputAction.UseFlask3 },
            new KeyBindingData { KeyCode = KeyCode.Alpha4, InputAction = InputAction.UseFlask4 },
            new KeyBindingData { KeyCode = KeyCode.Alpha5, InputAction = InputAction.UseFlask5 },

            new KeyBindingData { KeyCode = KeyCode.Escape, InputAction = InputAction.PauseMenu },
            new KeyBindingData { KeyCode = KeyCode.I, InputAction = InputAction.Inventory },
            new KeyBindingData { KeyCode = KeyCode.O, InputAction = InputAction.SkillBook },
            new KeyBindingData { KeyCode = KeyCode.C, InputAction = InputAction.Character },
            new KeyBindingData { KeyCode = KeyCode.P, InputAction = InputAction.PassiveSkillTree },
            new KeyBindingData { KeyCode = KeyCode.M, InputAction = InputAction.WorldMap },
        };
        Video = new VideoSettings
        {
            AntiAliasingLevel = 1,
            QualityLevel = 4,
            ShadowQuality = ShadowQuality.All,
            HideBlockingTerrain = true,
            VSync = true
        };
        Audio = new AudioSettings
        {
            MasterVolume = 0,
            MusicVolume = 0,
            SFXVolume = 0
        };
    }
}

public struct KeyBindingData
{
    public KeyCode KeyCode;
    public InputAction InputAction;
    public KeyState KeyState;
    public string Axis;
}

public struct InputSettings
{
    public KeyBindingData[] KeyActions;
}

public struct VideoSettings
{
    public int QualityLevel;
    public ShadowQuality ShadowQuality;
    /// <summary>0 is off after that come the <see cref="AntialiasingModel.FxaaPreset"/> settings (there is an extra value at the beginning)</summary>
    public int AntiAliasingLevel;
    public bool HideBlockingTerrain;
    public bool VSync;
}

public struct AudioSettings
{
    public float MasterVolume;
    public float MusicVolume;
    public float SFXVolume;
}