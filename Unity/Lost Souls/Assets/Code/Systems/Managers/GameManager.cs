﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utility;

public class GameManager : Singleton<GameManager>
{
    //public List<ActData> Acts;

    public bool Paused => _isPaused;

    public Player Player;
    public Transform PlayerSpawnTransform;

    private readonly Manager[] _managers;

    private bool _isPaused;
    private bool _inGame;

    public GameManager()
    {
        _managers = new Manager[] {
            new InputManager(),
            new ActorManager(),
            new QuestManager(),
            new SceneManager(),
        };

        //Acts = new List<ActData>();
    }

    protected override void Awake()
    {
        base.Awake(); // This is important for the intialization for the Singleton<T>
        if (this != Instance) return;

        foreach (Manager manager in _managers)
        {
            manager.Awake();
        }

#if TEST
        if (PlayerSpawnTransform == null)
            return;

        Player player = Instantiate(Player, PlayerSpawnTransform.position, Quaternion.identity);
        KeyMap keyMap = InputManager.KeyMap;

        player.SetKeyMap(ref keyMap);
        player.Name = "TestPlayer";
        player.Level = 1;
        player.Experience = 0;
        player.Equipment.Equip(ItemManager.ItemList[0]);

        player.CameraController = Instantiate(player.CameraPrefab).GetComponent<CameraController>();
        player.CameraController.Target = player.transform;

        player.SetUIStack(FindObjectOfType<UIStack>());
        player.UiStack.Push("HUD");

        GameSave[] saves = DataManager.LoadAllSaves();

        if (saves.Length > 0)
        {
            string playerClass = DataManager.GetDataFromSave<PlayerClass>(saves[0], "class").ToString();

            player.PassiveSkillSystem.SetClass("Base" + playerClass);
        }

        DontDestroyOnLoad(player);
        DontDestroyOnLoad(player.CameraController);

        DataManager.SaveConfig();
#endif

    }

    private void Start()
    {
        foreach (Manager manager in _managers)
        {
            manager.Start();
        }
    }

    private void Update()
    {
        if (_isPaused)
            return;

        foreach (Manager manager in _managers)
        {
            manager.Update();
        }
    }

    public void Pause(bool value)
    {
        foreach (Manager manager in _managers)
        {
            manager.Pause(value);
        }

        _isPaused = value;
        Debug.Log("pause: " + value);
        if (value)
        {
            FindObjectOfType<UIStack>().Push("Pause menu");
        }
    }

    public void SaveGame()
    {
        foreach (Manager manager in _managers)
            manager.Save();

        if (!ActorManager.GetActivePlayer())
            return;

        DataManager.SaveGame(DataManager.GetDataFromCurrentSave<string>("name"));
    }

    public void LoadGame(string characterName)
    {
        foreach (Manager manager in _managers)
            manager.Load();

    }

    private void OnApplicationQuit()
    {
        SaveGame();

        foreach (Manager manager in _managers)
            manager.Exit();
    }
}

//[System.Serializable]
//public struct ActData
//{
//    public string Name;
//    public Sprite ActMap;
//    public GameObject WaypointUI;
//    public bool Unlocked;
//    public List<Area> Areas;
//}

//[System.Serializable]
//public class Area
//{
//    public string Name;
//    public string SceneName;
//    public bool Unlocked;
//    public bool WaypointUnlocked;
//    [HideInInspector]
//    public Waypoint Waypoint;
//    public List<int> ConnectedAreas;

//    public Area()
//    {
//        SceneManager.AddSceneLoadedDelegate(WaypointCallback);
//    }
    
//    public void WaypointCallback(Scene scene, LoadSceneMode mode)
//    {
//        if (scene.name == SceneName)
//            Waypoint = GameObject.FindObjectOfType<Waypoint>();

//        ActorManager.GetActivePlayer().transform.position = Waypoint.transform.position;

//        SceneManager.RemoveSceneLoadedDelegate(WaypointCallback);
//    }
//}
