﻿using System.Collections.Generic;
using UnityEngine;

public class ActorManager : Manager {
    private static Dictionary<int, Actor> actors;
    
    private static Player _activePlayer;

    static ActorManager() {
        actors = new Dictionary<int, Actor>();
    }

    public static void Register(Actor actor) {
        if (actor.GetType() == typeof(Player))
            _activePlayer = (Player)actor;
        actors.Add(actor.GetInstanceID(), actor);
    }

    public override void Load() {
        Debug.Log("loading inventories");
        bool success;
        ItemContainer playerItems = DataManager.GetDataFromCurrentSave<ItemContainer>("PlayerInventory", out success);
        GetActivePlayer().Inventory = success ? playerItems : new ItemContainer();
    }

    public override void Save() {
        if (!GetActivePlayer())
            return;

        DataManager.AddDataToSave("name", GetActivePlayer().Name);
        DataManager.AddDataToSave("class", GetActivePlayer().Class);
        DataManager.AddDataToSave("level", GetActivePlayer().Level);
        DataManager.AddDataToSave("experience", GetActivePlayer().Experience);
        DataManager.AddDataToSave("passiveskills", GetActivePlayer().PassiveSkillSystem.PassiveSkills);
        DataManager.AddDataToSave("equipment", GetActivePlayer().Equipment);
    }

    public static Actor GetClosestActor(Actor centerActor, float sqrMaxRange)
    {
        float closestDist = sqrMaxRange;
        Actor closest = null;
        foreach (var actor in actors)
        {
            float dist = (actor.Value.transform.position - centerActor.transform.position).sqrMagnitude;
            if (dist < closestDist && actor.Value != centerActor)
            {
                closestDist = dist;
                closest = actor.Value;
            }
        }
        return closest;
    }

    public static float GetActorHeight(Actor actor) {
        Collider[] colliders = actor.GetComponents<Collider>();

        float height = 0;

        foreach (Collider collider in colliders) {
            height += collider.bounds.extents.y;
        }

        return height / colliders.Length;
    }

    public static void UnRegister(Actor actor) => actors.Remove(actor.GetInstanceID());
    public static void UnRegister(int instanceId) => actors.Remove(instanceId);
    public static Player GetActivePlayer() => _activePlayer;
}