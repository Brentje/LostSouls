﻿using System.Collections.Generic;
using UnityEngine;
using Utility;

public class GroupManager : Singleton<GroupManager>
{
    private float timer;
    private const float updateInterval = 1f;

    public List<IGroup> groups;

    protected override void Awake()
    {
        base.Awake();

        groups = new List<IGroup>();
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > updateInterval)
        {
            timer -= updateInterval;
            List<IGroup> groupsToRemove = new List<IGroup>();
            for (int i = 0; i < groups.Count; i++)
            {
                if (groups[i].GetChildrenSize() < 2)
                {
                    groupsToRemove.Add(groups[i]);
                    continue;
                }
                groups[i].Update();
                DebugUtil.DrawCircle(groups[i].Position, Vector3.up, Vector3.forward, groups[i].Range, 24, Color.red, 1);
            }
            foreach (IGroup empty in groupsToRemove)
            {
                UnRegister(empty);
            }
        }
    }

    public void Register(IGroup group)
    {
        if (!groups.Contains(group))
            groups.Add(group);
    }

    public void UnRegister(IGroup group)
    {
        if (groups.Contains(group))
        {
            group.Clear();
            groups.Remove(group);
        }
    }
}
