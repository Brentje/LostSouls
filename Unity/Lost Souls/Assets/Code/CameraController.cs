﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

//[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    public Transform Target;
    public Vector3 Offset;


    private static Camera[] targetCameras;
    private static Camera MainCamera;
    private static PostProcessingProfile profile;

    private float ZoomSpeed = 15;
    private float Smoothing = 2.5f;

    private float targetFOV;
    private float BaseFOV = 60;
    private float MaxFOV = 60;
    private float MinFOV = 20;

    private float currentFOV;

    public static bool hideBlockingTerrain;

    private void Awake()
    {
        List<Camera> cams = new List<Camera>(GetComponentsInChildren<Camera>());

        MainCamera = GetComponent<Camera>();

        for (int i = 0; i < cams.Count; i++)
        {
            if (cams[i] == MainCamera)
            {
                cams.RemoveAt(i);
            }
        }

        targetCameras = cams.ToArray();

        foreach (PostProcessingBehaviour ppb in GetComponentsInChildren<PostProcessingBehaviour>())
        {
            ppb.profile = GetPostProcessingBehaviour();
        }

        for (int i = 0; i < targetCameras.Length; i++)
        {
            targetCameras[i].fieldOfView = BaseFOV;
        }
        MainCamera.fieldOfView = BaseFOV;

        for (int i = 0; i < targetCameras.Length; i++)
        {
            targetCameras[i].gameObject.SetActive(hideBlockingTerrain);
        }
        MainCamera.enabled = !hideBlockingTerrain;

        targetFOV = BaseFOV;
        currentFOV = targetFOV;
    }

    public static void HideBlockingTerrain(bool enabled)
    {
        hideBlockingTerrain = enabled;

        if (targetCameras == null || MainCamera == null)
            return;

        for (int i = 0; i < targetCameras.Length; i++)
        {
            targetCameras[i].gameObject.SetActive(hideBlockingTerrain);
        }

        MainCamera.enabled = !hideBlockingTerrain;
    }

    public static PostProcessingProfile GetPostProcessingBehaviour()
    {
        if (!profile)
            profile = Resources.Load<PostProcessingProfile>("PostProcessingStacks/InGame");
        return profile;
    }

    public static Ray ScreenPointToRay(Vector2 pos)
    {
        if (hideBlockingTerrain)
            return targetCameras[targetCameras.Length - 1].ScreenPointToRay(pos);
        return MainCamera.ScreenPointToRay(pos);
    }

    // Update is called once per frame
    void Update()
    {
        currentFOV = Mathf.Lerp(currentFOV, targetFOV, Time.deltaTime * ZoomSpeed / Smoothing);
        currentFOV = Mathf.Clamp(currentFOV, MinFOV, MaxFOV);

        if (hideBlockingTerrain && targetCameras.Length > 0)
        {
            for (int i = 0; i < targetCameras.Length; i++)
            {
                targetCameras[i].fieldOfView = currentFOV;
            }
        }
        else
        {
            MainCamera.fieldOfView = currentFOV;
        }
    }

    public void Zoom(float value)
    {
        for (int i = 0; i < targetCameras.Length; i++)
        {
            targetFOV = targetCameras[i].fieldOfView - value * MaxFOV;
        }
    }

    private void LateUpdate()
    {
        if (Target == null)
            return;

        transform.position = Target.position + Offset;
        transform.LookAt(Target);
    }
}
