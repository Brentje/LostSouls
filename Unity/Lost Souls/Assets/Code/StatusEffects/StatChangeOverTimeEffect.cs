﻿using UnityEngine;
public class StatChangeOverTimeEffect : StatusEffect
{
    private StatType statType;
    private int statChangeAmount;
    private float frequency;
    private float amountChanged;
    private float timer;
    private float duration;

    public StatChangeOverTimeEffect(Actor actor, StatType statType, int statChangeAmount, float frequency, float duration) : base(actor)
    {
        this.statType = statType;
        this.statChangeAmount = statChangeAmount;
        this.frequency = frequency;
        this.duration = duration;
    }

    public override void Apply()
    {
        if (timer >= frequency)
        {
            target.Stats.GetStat(statType).Value += statChangeAmount;
            timer = 0.0f;

            if (duration <= 0)
                target.RemoveStatusEffect(this);
        }
        timer += Time.deltaTime;
        duration -= Time.deltaTime;
    }

    public override void Revert()
    {
    }
}
