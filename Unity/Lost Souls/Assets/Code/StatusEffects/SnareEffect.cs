﻿using UnityEngine;

public class SnareEffect : StatusEffect
{
    public SnareEffect(Actor actor) : base(actor)
    {
    }

    public override void Apply()
    {
        if (target == null)
            return;

    }

    public override void Revert()
    {
        if (target == null)
            return;

    }
}