﻿using UnityEngine;

public class StatChangeEffect : StatusEffect
{
    private StatType statType;
    private int statChangeAmount;
    public bool isApplied = false;

    public StatChangeEffect(Actor actor, StatType statType, int statChangeAmount) : base(actor)
    {
        this.statType = statType;
        this.statChangeAmount = statChangeAmount;
    }

    public override void Apply()
    {
        if (isApplied)
            return;
        target.Stats.GetStat(statType).Value += statChangeAmount;
        isApplied = true;
    }

    public override void Revert()
    {
        if (isApplied)
        {
            target.Stats.GetStat(statType).Value -= statChangeAmount;
            isApplied = false;
        }
    }
}