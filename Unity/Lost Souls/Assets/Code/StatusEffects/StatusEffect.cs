﻿public abstract class StatusEffect
{
    //The target of the Status Effect
    protected Actor target;

    //Set the target of the StatusEffect
    public StatusEffect(Actor actor)
    {
        if (actor == null)
            return;

        target = actor;
    }

    //Apply the effect, this will be called each frame if the StatusEffect is applied on a actor
    public abstract void Apply();

    //Revert the effect
    public abstract void Revert();

}
