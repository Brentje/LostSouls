﻿using UnityEngine;

public class SlownessEffect : StatChangeEffect
{
    private float duration;
    private bool hasDuration;

    public SlownessEffect(Actor actor, int statChangeAmount) : base(actor, StatType.MovementSpeed, statChangeAmount)
    {
    }

    public SlownessEffect(Actor actor, int statChangeAmount, float duration) : base(actor, StatType.MovementSpeed, statChangeAmount)
    {
        this.duration = duration;
        hasDuration = true;
    }

    public override void Apply()
    {
        base.Apply();
        if (!hasDuration)
            return;

        duration -= Time.deltaTime;
        if (duration < 0)
            target.RemoveStatusEffect(this);
    }
}
