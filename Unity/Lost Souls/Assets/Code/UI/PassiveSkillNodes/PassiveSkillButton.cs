﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class PassiveSkillButton : Button {
    public string PassiveSkill;
    public List<PassiveSkillButton> ConnectedSkillButtons;

    [Tooltip("Do not change this value or shit will break!")]
    public int ID;

    private PassiveSkillSystem passiveSkillSystem;
    private PassiveSkillNode passiveSkillNode;

    private bool isUnlocked;

    protected override void Awake() {
        base.Awake();

        passiveSkillSystem = PassiveSkillTree.Player.PassiveSkillSystem;
        
        if (PassiveSkill == "Root")
            return;

        passiveSkillNode = PassiveSkillList.Instance.FindPassiveSkill(PassiveSkill);

        if (isUnlocked) {
            ColorBlock cb = colors;
            cb.disabledColor = colors.normalColor;
            colors = cb;
        }

        interactable = isUnlocked;

        PassiveSkillSystem.OnSkillPointUpdated += () => {
            if (!passiveSkillNode.IsUnlocked)
                return;

            if (PassiveSkillTree.Player.PassiveSkillSystem.PassiveSkillPoints > 0)
                foreach (PassiveSkillButton connectedSkillButton in ConnectedSkillButtons) 
                    connectedSkillButton.interactable = true;
            else 
                foreach (PassiveSkillButton connectedSkillButton in ConnectedSkillButtons) 
                    connectedSkillButton.interactable = false;
        };
    }

    public void SetInteractable() {

    }

    public void Update() {
        foreach (PassiveSkillButton connectedSkillButton in ConnectedSkillButtons) {
            Debug.DrawLine(transform.position, connectedSkillButton.transform.position);
        }
    }

    public override void OnPointerClick(PointerEventData eventData) {
        base.OnPointerClick(eventData);

        if (eventData.button == PointerEventData.InputButton.Left) {
            switch(PassiveSkillTree.CurrentState){
                case PassiveSkillTree.EditState.Unlock:
                    
                    break;
                case PassiveSkillTree.EditState.Refund:

                    break;
            }

            passiveSkillSystem.Unlock(PassiveSkill, true);
        }
    }
}