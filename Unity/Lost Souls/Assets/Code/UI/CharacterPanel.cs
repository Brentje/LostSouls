﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterPanel : UIElement
{
    /// <summary>UI Components that take care of the slots graphics</summary>
    public ItemSlotUI[] ItemSlots;

    /// <summary>Reference to the player</summary>
    private Player player;

    public Text StatText;

    public override void Enter()
    {
        // Get the instance of the player
        player = ActorManager.GetActivePlayer();

        for (int i = 0; i < ItemSlots.Length; i++)
        {
            // Update the slots graphics
            UpdateSlotGraphics(i);
            // Create an instance of i for the lamda expression
            int j = i;
            // Add the Onclick method to the OnClick of the slot
            ItemSlots[i].onClick.AddListener(() => { OnClick(j); UpdateStatList(); });
        }
        UpdateStatList();
    }

    public void OnClick(int slot)
    {
        if (slot < player.Equipment.Slots.Length && slot >= 0)
        {
            // Switch item with the one in the cursor
            Item cursorItem = CursorItemSlot.Instance.slot.GetCurrentItem();
            CursorItemSlot.Instance.slot.SetItem(player.Equipment.Slots[slot].SwitchItem(cursorItem));
        }
        // Update the slots graphics
        UpdateSlotGraphics(slot);
    }

    /// <summary>
    /// Update the Graphics of the slot
    /// </summary>
    /// <param name="slot">Index of the slot</param>
    private void UpdateSlotGraphics(int slot)
    {
        ItemSlots[slot].UpdateGraphics(player.Equipment.Slots[slot].GetCurrentItem());
    }

    public void UpdateStatList()
    {
        StatText.text = "";
        for (int i = 0; i < player.Stats.Data.Count; i++)
        {
            StatText.text += $"{player.Stats.Data[i].Key.ToString()}: {player.Stats.Data[i].Value.Value}\n";
        }
    }
}