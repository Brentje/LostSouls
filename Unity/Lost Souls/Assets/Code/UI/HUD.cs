﻿using UnityEngine;
using UnityEngine.UI;

public class HUD : UIElement
{
    [SerializeField] private Image expBarImage;

    [SerializeField] private Image healthBarImage;

    [SerializeField] private Image manaBarImage;

    private Player player;

    [SerializeField] private Text playerLevelText;

    [SerializeField] private Image[] potionImages;

    [SerializeField] private Text questNameText;

    [SerializeField] private Text questTaskText;

    //These should be callbacks
    private void UpdateHealthBar()
    {
        if (healthBarImage)
            healthBarImage.fillAmount =
                player.Stats.GetStat(StatType.Health).Value / player.Stats.GetStat(StatType.Health).MaxValue;
    }

    private void UpdateManaBar()
    {
        if (manaBarImage)
            manaBarImage.fillAmount =
                player.Stats.GetStat(StatType.Mana).Value / player.Stats.GetStat(StatType.Mana).MaxValue;
    }

    private void UpdateExpBar()
    {
        if (expBarImage)
            expBarImage.fillAmount = player.Experience / Mathf.Pow(2, player.Level);
        if (playerLevelText)
            playerLevelText.text = player.Level.ToString();
    }

    private void UpdateQuestLog()
    {
        if (questNameText && questTaskText)
            if (player.Quest == null)
            {
                questNameText.text = string.Empty;
                questTaskText.text = string.Empty;
            }
            else if (player.Quest.GetCurrentTask() == null)
            {
                questNameText.text = player.Quest.Name;
                questTaskText.text = string.Empty;
            }
            else
            {
                questNameText.text = player.Quest.Name;
                questTaskText.text = player.Quest.GetCurrentTask().Description;
            }
    }

    private void UpdatePotions()
    {
        for (int i = 0; i < potionImages.Length; i++)
        {
            int slot = (int)Equipment.EquipmentSlotType.Potion1 + i;
            if (potionImages[i] == null)
                continue;
            if (player.Equipment.Slots[slot].IsOccupied())
                potionImages[i].sprite = player.Equipment.Slots[slot]
                    .GetCurrentItem().GetSprite();
            else
                potionImages[i].sprite = null;
        }
    }

    private void UpdateSkillHotBar()
    {
    }

    private void UpdateQuickAccessPotions()
    {
    }

    public override void Enter()
    {
        player = ActorManager.GetActivePlayer();

        UpdateHealthBar();
        //UpdateManaBar();
        UpdateQuestLog();
        UpdateExpBar();
        UpdateQuickAccessPotions();
        UpdatePotions();

        player.Stats.GetStat(StatType.Health).OnValueChanged += arg0 => { UpdateHealthBar(); };
        for (int i = 0; i < potionImages.Length; i++)
        {
            int slot = (int)Equipment.EquipmentSlotType.Potion1 + i;
            player.Equipment.Slots[slot].OnSwitch += item => { UpdatePotions(); };
        }
        player.OnExperienceGained += UpdateExpBar;
        //player.Stats.GetStat(StatType.Mana).OnValueChanged += (arg0) =>
        // {
        //    UpdateManaBar();
        //};
    }

    public override void Exit()
    {
    }
}