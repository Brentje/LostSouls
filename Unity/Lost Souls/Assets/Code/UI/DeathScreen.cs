﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathScreen : UIElement
{
    public Image ProgressBar;

    public override void Enter()
    {
        StartCoroutine(SwitchToMainMenu(5));
    }

    private IEnumerator SwitchToMainMenu(float delay)
    {
        float targetTime = Time.time + delay;
        while (Time.time < targetTime)
        {
            yield return new WaitForEndOfFrame();
            if (ProgressBar)
                ProgressBar.fillAmount = Mathf.InverseLerp(targetTime - delay, targetTime, Time.time);
        }

        ActorManager.GetActivePlayer().enabled = true;

        SceneManager.AddSceneLoadedDelegate(Callback);
        SceneManager.LoadScene(WorldConfig.Instance.Acts[0].Areas[0].SceneName);
    }

    private void Callback(Scene scene, LoadSceneMode mode) {
        Waypoint target = FindObjectOfType<Waypoint>();

        float height = ActorManager.GetActorHeight(ActorManager.GetActivePlayer());

        Vector3 final = new Vector3(target.transform.position.x + Random.Range(1, 2), target.transform.position.y + height, target.transform.position.z + Random.Range(1, 2));

        Player player = ActorManager.GetActivePlayer();
        player.transform.position = final;
        player.Agent.enabled = false;
        player.Agent.enabled = true;

        SceneManager.RemoveSceneLoadedDelegate(Callback);
    }
}