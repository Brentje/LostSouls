﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WorldMap : UIElement
{
    [SerializeField]
    private GameObject actButtonPrefab;

    [SerializeField]
    private Transform actButtonPanel;

    [SerializeField]
    private Image currentActMap;

    [SerializeField]
    private Text currentActName;

    [SerializeField]
    private GameObject areasNodeHolder;

    private GameObject currentAreaTree;

    public WorldMap()
    {
    }

    public override void Enter()
    {
        for (int i = 0; i < WorldConfig.Instance.Acts.Count; i++)
        {
            if (WorldConfig.Instance.Acts[i].Unlocked)
            {
                GameObject actButton = Instantiate(actButtonPrefab, actButtonPanel);
                
                int actID = i;
                actButton.GetComponent<Button>().onClick.AddListener(() => ShowAct(actID));
                actButton.GetComponentInChildren<Text>().text = "Act: " + (i + 1);
            }
        }

        if (WorldConfig.Instance.Acts.Count != 0)
            ShowAct(0);
    }

    public void ShowAct(int actID)
    {
        currentActMap.sprite = WorldConfig.Instance.Acts[actID].ActMap;
        currentActName.text = WorldConfig.Instance.Acts[actID].Name;

        if (currentAreaTree != null)
            Destroy(currentAreaTree);

        if (WorldConfig.Instance.Acts[actID].WaypointUI)
            currentAreaTree = Instantiate(WorldConfig.Instance.Acts[actID].WaypointUI, transform);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
            SceneManager.LoadSceneAsync(WorldConfig.Instance.Acts[0].Areas[2].Name);
    }


    public override void Exit()
    {

    }
}
