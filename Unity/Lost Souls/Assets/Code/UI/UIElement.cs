﻿using UnityEngine;

public abstract class UIElement : MonoBehaviour {
    [HideInInspector] public UIStack uiStack;

    public virtual void Enter() { }

    public virtual void Exit() { }

    public void Back() {
        uiStack.Pop();
    }

    public void CloseWindow() {
        uiStack.Remove(this);
    }

    public void OpenUIElement(string uiElement) {
        uiStack.Push(uiElement);
    }
}