﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Skillbook : UIElement
{
    private Player player;
    private AbilitySystem targetAbilitySystem;

    [SerializeField]
    private GameObject learnedAbilityPanel;
    [SerializeField]
    private GameObject selectedAbilityPanel;

    [SerializeField]
    private GameObject abilityUIObject;

    private List<GameObject> learnedAbilityUIObjects;
    private List<GameObject> selectedAbilityUIObjects;

    [SerializeField]
    private GameObject tooltip;
    private Ability ability;

    [SerializeField]
    private Image tooltipAbilityIcon;
    [SerializeField]
    private Text tooltipHeaderText;
    [SerializeField]
    private Text tooltipBodyText;
    [SerializeField]
    private Text tooltipFooterText;
    private string tooltipHeaderData;
    private string tooltipBodyData;
    private string tooltipFooterData;

    public Skillbook()
    {
        learnedAbilityUIObjects = new List<GameObject>();
        selectedAbilityUIObjects = new List<GameObject>();
    }

    private void Awake()
    {
        player = ActorManager.GetActivePlayer();
        targetAbilitySystem = player.AbilitySystem;
    }

    public override void Enter()
    {
        UpdateSpellBook();

        targetAbilitySystem.OnAbilityListChange.AddListener(UpdateSpellBook);
    }

    public void UpdateSpellBook()
    {
        for (int i = 0; i < targetAbilitySystem.LearnedAbilities.Count; i++)
        {
            if (i < learnedAbilityUIObjects.Count)
                continue;

            GameObject currentUIAbility = Instantiate(abilityUIObject, learnedAbilityPanel.transform);
            SkillbookAbility currentSkillbookAbility = currentUIAbility.GetComponent<SkillbookAbility>();
            currentSkillbookAbility.Ability = targetAbilitySystem.LearnedAbilities[i];
            currentSkillbookAbility.Skillbook = this;
            learnedAbilityUIObjects.Add(currentUIAbility);
        }

        while (selectedAbilityUIObjects.Any())
        {
            Destroy(selectedAbilityUIObjects.First());
            selectedAbilityUIObjects.Remove(selectedAbilityUIObjects.First());
        }

        selectedAbilityUIObjects.Clear();

        for (int i = 0; i < targetAbilitySystem.SelectedAbilities.Count; i++)
        {
            GameObject currentUIAbility = Instantiate(abilityUIObject, selectedAbilityPanel.transform);
            SkillbookAbility currentSkillbookAbility = currentUIAbility.GetComponent<SkillbookAbility>();
            currentSkillbookAbility.Ability = targetAbilitySystem.SelectedAbilities[i];
            currentSkillbookAbility.Skillbook = this;
            selectedAbilityUIObjects.Add(currentUIAbility);
        }
    }

    //Activates the tooltip
    public void ActivateTooltip(Ability ability)
    {
        this.ability = ability;
        ConstructTooltip();
        tooltip.SetActive(true);
    }

    public void Update()
    {
        if (tooltip.activeSelf)
            tooltip.transform.position = Input.mousePosition;
    }

    //Deactivates the tooltip
    public void DeactivateTooltip()
    {
        EmptyTooltip();
        tooltip.SetActive(false);
    }

    //Constructs the data that needs to be displayed on the tooltip
    private void ConstructTooltip()
    {
        tooltipHeaderData = "<color=#524141FF><b>" + ability.AbilityName + "</b></color>";
        tooltipHeaderText.text = tooltipHeaderData;
        tooltipBodyData = ability.Description;
        tooltipBodyText.text = tooltipBodyData;
        tooltipFooterData = "Cooldown: " + ability.Cooldown + "\t  Cost: " + ability.Cost;
        tooltipFooterText.text = tooltipFooterData;
        tooltipAbilityIcon.sprite = Resources.Load<Sprite>(ability.IconSourcePath);
    }

    //Display nothing in the text of the tooltip
    private void EmptyTooltip()
    {
        tooltipHeaderData = string.Empty;
        tooltipBodyData = string.Empty;
        tooltipHeaderText.text = string.Empty;
        tooltipBodyText.text = string.Empty;
        tooltipAbilityIcon.sprite = null;
    }

    public override void Exit()
    {

    }
}
