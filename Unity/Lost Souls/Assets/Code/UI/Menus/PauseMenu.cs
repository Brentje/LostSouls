﻿using UnityEngine;

public class PauseMenu : UIElement
{
    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public void Cancel()
    {
        uiStack.Pop();
    }

    public void ShowOptions()
    {
        uiStack.Push("Option Menu");
    }

    public void MainMenu()
    {
        GameManager.Instance.SaveGame();

        
        Destroy(ActorManager.GetActivePlayer().CameraController.gameObject);
        Destroy(ActorManager.GetActivePlayer().gameObject);

        SceneManager.LoadScene(0);
    }

    public void Unpause()
    {
        GameManager.Instance.Pause(false);
    }
}
