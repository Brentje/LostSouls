﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class MainMenu : MenuElement {
    public override void Enter() {

    }

    public override void Exit() {
        
    }

    public void Play() {
        uiStack.Pop();
        MainMenuCamera.Instance.RotateCamera(90, () => {
            uiStack.Push("CharacterSelect");
        });
    }

    public void Options() {
        uiStack.Push("OptionMenu");
    }

    public void Quit() {
        Application.Quit();
    }
}