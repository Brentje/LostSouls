﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PassiveSkillTree : UIElement {
    public static Player Player => ActorManager.GetActivePlayer();
    public static EditState CurrentState { get; private set; }

    public PassiveSkillButton Root;
    public Text SkillPoints;
    
    public void Awake(){
         PassiveSkillSystem.OnSkillPointUpdated += () => {
            SkillPoints.text = "Skill Points: " + Player.PassiveSkillSystem.PassiveSkillPoints;
        };
    }

    public override void Enter() {
        CurrentState = EditState.Unlock;
    }

    public override void Exit() {

    }

    // Enumerates over every PassiveSkillButton in the PassiveSkillTree.
    public static IEnumerable<PassiveSkillButton> EnumarateButtons(PassiveSkillButton passiveSkillButton) {
        Stack<PassiveSkillButton> buttons = new Stack<PassiveSkillButton>(new [] { passiveSkillButton });
        List<PassiveSkillButton> markedButtons = new List<PassiveSkillButton>();

        while (buttons.Count != 0) {
            PassiveSkillButton button = buttons.Pop();

            if (markedButtons.Contains(button))
                continue;

            markedButtons.Add(button);

            yield return button;

            if (button.ConnectedSkillButtons == null) 
                continue;

            foreach (PassiveSkillButton connectedButton in button.ConnectedSkillButtons)
                buttons.Push(connectedButton);
        }
    }

    public static void SetState(EditState state){
        CurrentState = state;
    }

    private void ShowUnlockable() {
        if (PassiveSkillTree.Player.PassiveSkillSystem.PassiveSkillPoints > 0) {
            
        }
    }

    private void SetInteractable(PassiveSkillButton passiveSkillButtonton) {

    }

    public enum EditState{
        Unlock,
        Refund
    }
}