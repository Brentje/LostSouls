﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterCreation : MenuElement {
    private CharacterInfo newCharacter;

    public List<CharacterInfo> BaseClasses;

    public override void Enter() {
        newCharacter = new CharacterInfo {
            Name = "TestPlayer",
            Class = PlayerClass.Mage,
            Equipment = new Equipment(),
            PassiveSkills = new PassiveSkillNode[0],
            ClassModel = null
        };
    }

    public override void Exit() {
        
    }

    public void Play() {
        GameSave save = new GameSave();

        DataManager.SetGameActive(save);

        DataManager.AddDataToSave("name", newCharacter.Name);
        DataManager.AddDataToSave("class", newCharacter.Class);
        DataManager.AddDataToSave("level", newCharacter.Level);
        DataManager.AddDataToSave("experience", newCharacter.Experience);
        DataManager.AddDataToSave("passiveskills", newCharacter.PassiveSkills);
        DataManager.AddDataToSave("equipment", newCharacter.Equipment);

        DataManager.SaveGame(newCharacter.Name);

        SceneManager.AddSceneLoadedDelegate(CreateCharacter);
        SceneManager.LoadSceneAsync((int)GameScenes.ForestEncampment);
    }

    public void CreateCharacter(Scene scene, LoadSceneMode loadSceneMode) {
        KeyMap activeKeyMap = InputManager.KeyMap;
        PassiveSkillNode baseStats =
            PassiveSkillList.Instance.FindPassiveSkill(
                $"Base{DataManager.GetDataFromCurrentSave<PlayerClass>("class")}");

        Player player = Instantiate(GameManager.Instance.Player, new Vector3(0, 1.3f, 0),
            Quaternion.identity);
        player.SetKeyMap(ref activeKeyMap);
        player.PassiveSkillSystem.ClassStats = baseStats;

        player.CameraController = Instantiate(player.CameraPrefab).GetComponent<CameraController>();
        player.CameraController.Target = player.transform;

        player.SetUIStack(FindObjectOfType<UIStack>());
        player.UiStack.Push("HUD");

        DontDestroyOnLoad(player);
        DontDestroyOnLoad(player.CameraController);

        SceneManager.RemoveSceneLoadedDelegate(CreateCharacter);
    }
}