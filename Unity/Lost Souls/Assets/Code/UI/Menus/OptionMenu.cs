﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class OptionMenu : UIElement
{
    [Header("Video")] public Dropdown qualityLevels;
    public Dropdown Shadows;
    public Dropdown AntiAliasing;

    public Toggle hideBlockingTerrain;
    public Toggle VSync;

    [Header("Audio")] public AudioMixer MainMixer;
    public Slider MasterVolume;
    public Slider MusicVolume;
    public Slider SFXVolume;

    [Header("Controls")] public GameObject KeyBindPrefab;
    public Transform buttonParrent;
    private SelectKeyButton[] keyButtons;

    public override void Enter()
    {
        if (DataManager.Config == null)
            DataManager.LoadConfig();

        Reset();
    }

    public override void Exit()
    {
        Reset();
    }

    public void Cancel()
    {
        uiStack.Pop();
    }

    public void ApplySettings()
    {
        // Video
        DataManager.Config.Video.QualityLevel = qualityLevels.value;
        DataManager.Config.Video.HideBlockingTerrain = hideBlockingTerrain.isOn;
        DataManager.Config.Video.ShadowQuality = (ShadowQuality)Shadows.value;
        DataManager.Config.Video.VSync = VSync.isOn;

        DataManager.Config.Video.AntiAliasingLevel = AntiAliasing.value;

        // Audio
        DataManager.Config.Audio.MasterVolume = MasterVolume.value;
        DataManager.Config.Audio.MusicVolume = MusicVolume.value;
        DataManager.Config.Audio.SFXVolume = SFXVolume.value;

        // Input
        for (int i = 0; i < keyButtons.Length; i++)
        {
            DataManager.Config.Input.KeyActions[i].KeyCode = keyButtons[i].Key;
        }

        Reset();

        // Save config
        DataManager.SaveConfig();
    }

    public void SetMasterVolume(float value)
    {
        //DataManager.Config.Audio.MasterVolume = value;
        SetVolume("Master", value);
    }
    public void SetMusicVolume(float value)
    {
        //DataManager.Config.Audio.MusicVolume = value;
        SetVolume("Music", value);
    }
    public void SetSFXVolume(float value)
    {
        //DataManager.Config.Audio.SFXVolume = value;
        SetVolume("SFX", value);
    }

    private void SetVolume(string name, float value)
    {
        MainMixer.SetFloat(name, value);
    }

    public void Reset()
    {
        qualityLevels.ClearOptions();
        qualityLevels.AddOptions(new List<string>(QualitySettings.names));
        qualityLevels.value = DataManager.Config.Video.QualityLevel;
        QualitySettings.SetQualityLevel(DataManager.Config.Video.QualityLevel);

        Shadows.ClearOptions();
        Shadows.AddOptions(new List<string>(Enum.GetNames(typeof(ShadowQuality))));
        Shadows.value = (int)DataManager.Config.Video.ShadowQuality;
        QualitySettings.shadows = DataManager.Config.Video.ShadowQuality;

        hideBlockingTerrain.isOn = DataManager.Config.Video.HideBlockingTerrain;
        CameraController.HideBlockingTerrain(DataManager.Config.Video.HideBlockingTerrain);

        VSync.isOn = DataManager.Config.Video.VSync;
        QualitySettings.vSyncCount = DataManager.Config.Video.VSync ? 1 : 0;

        AntiAliasing.ClearOptions();
        List<string> AAOptions = new List<string>(Enum.GetNames(typeof(AntialiasingModel.FxaaPreset)));
        AAOptions.Insert(0, "Off");
        AntiAliasing.AddOptions(AAOptions);
        AntiAliasing.value = DataManager.Config.Video.AntiAliasingLevel;
        AntialiasingModel.Settings AA = CameraController.GetPostProcessingBehaviour().antialiasing.settings;
        AA.fxaaSettings.preset = (AntialiasingModel.FxaaPreset)DataManager.Config.Video.AntiAliasingLevel - 1;
        CameraController.GetPostProcessingBehaviour().antialiasing.settings = AA;
        CameraController.GetPostProcessingBehaviour().antialiasing.enabled = DataManager.Config.Video.AntiAliasingLevel != 0;

        // Audio
        MasterVolume.value = DataManager.Config.Audio.MasterVolume;
        MusicVolume.value = DataManager.Config.Audio.MusicVolume;
        SFXVolume.value = DataManager.Config.Audio.SFXVolume;

        MainMixer.SetFloat("Master", DataManager.Config.Audio.MasterVolume);
        MainMixer.SetFloat("Music", DataManager.Config.Audio.MusicVolume);
        MainMixer.SetFloat("SFX", DataManager.Config.Audio.SFXVolume);

        // Input
        InputManager.ResetKeys();

        if (keyButtons != null)
            for (int i = 0; i < keyButtons.Length; i++)
            {
                Destroy(keyButtons[i].transform.parent.gameObject);
            }

        KeyBinding[] bindings = InputManager.KeyMap.KeyBindings;
        keyButtons = new SelectKeyButton[bindings.Length];

        for (int i = 0; i < bindings.Length; i++)
        {
            GameObject keybutton = Instantiate(KeyBindPrefab, buttonParrent);
            keyButtons[i] = keybutton.GetComponentInChildren<SelectKeyButton>();
            keyButtons[i].Action = (InputAction)i;
            if (bindings[i].KeyCode != KeyCode.None)
                keyButtons[i].Key = bindings[i].KeyCode;
            else
                keyButtons[i].interactable = false;
        }
    }

    public void ResetToDefault()
    {
        DataManager.Config.LoadDefault();
        Reset();
    }
}
