﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Resources;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utility;

[System.Serializable]
public struct CharacterInfo {
    public string Name;
    public int Level;
    public float Experience;
    public PlayerClass Class;
    public Equipment Equipment;
    public PassiveSkillNode[] PassiveSkills;
    public GameObject ClassModel;
}

public class CharacterSelect : MenuElement {
    public GameObject GhostPrefab;
    public Socket[] SpawnPoints;

    public Text CharacterName;
    public Text CharacterLevel;
    public Text CharacterClass;
    public List<Text> PassiveSkills;

    public Button PlayButton;
    public Text PlayButtonText;

    private List<CharacterInfo> characterInfo;
    private GameSave[] saves;

    private int selectedCharacterIndex;

    public override void Enter() {
        characterInfo   = new List<CharacterInfo>();
        saves           = DataManager.LoadAllSaves();

        if (saves.Length != 0) 
            foreach (GameSave gameSave in saves) {
                characterInfo.Add(new CharacterInfo {
                    Name            = DataManager.GetDataFromSave<string>(gameSave, "name"),
                    Class           = DataManager.GetDataFromSave<PlayerClass>(gameSave, "class"),
                    Level           = DataManager.GetDataFromSave<int>(gameSave, "level"),
                    Experience      = DataManager.GetDataFromSave<float>(gameSave, "experience"),
                    Equipment       = DataManager.GetDataFromSave<Equipment>(gameSave, "equipment"),
                    PassiveSkills   = DataManager.GetDataFromSave<PassiveSkillNode[]>(gameSave, "passiveskills"),
                    ClassModel      = Resources.Load<GameObject>($"ClassModels/{PlayerClass.Mage}")
                });
            }

        if (saves.Length < SpawnPoints.Length) {
            for (int i = 0; i < SpawnPoints.Length - saves.Length; i++) {
                characterInfo.Add(new CharacterInfo {
                    Name = "New Character",
                    ClassModel = GhostPrefab
                });
            }
        }

        CharacterName.text = characterInfo[selectedCharacterIndex].Name;
        CharacterClass.text = characterInfo[selectedCharacterIndex].Class.ToString();
        CharacterLevel.text = characterInfo[selectedCharacterIndex].Level.ToString();

        NextCharacter(0);
    }

    public override void Exit() {
        
    }

    public void Play() {
        DataManager.SetGameActive(saves[selectedCharacterIndex]);

        SceneManager.AddSceneLoadedDelegate(CreatePlayer);
        SceneManager.LoadSceneAsync((int)GameScenes.ForestEncampment);
    }

    public new void Back() {
        uiStack.PopAll();

        MainMenuCamera.Instance.RotateCamera(-90, () => {
            uiStack.Push("MainMenu");
        });
    }

    private static void CreatePlayer(Scene scene, LoadSceneMode loadSceneMode) {
        KeyMap activeKeyMap = InputManager.KeyMap;
        PassiveSkillNode baseStats =
            PassiveSkillList.Instance.FindPassiveSkill($"Base{DataManager.GetDataFromCurrentSave<PlayerClass>("class")}");

        Player player = Instantiate(GameManager.Instance.Player, new Vector3(0, 1.3f, 0),
            Quaternion.identity);

        player.SetKeyMap(ref activeKeyMap);
        player.PassiveSkillSystem.ClassStats = baseStats;

        player.CameraController = Instantiate(player.CameraPrefab);
        player.CameraController.Target = player.transform;

        player.SetUIStack(FindObjectOfType<UIStack>());
        player.UiStack.Push("HUD");

        DontDestroyOnLoad(player);
        DontDestroyOnLoad(player.CameraController);

        SceneManager.RemoveSceneLoadedDelegate(CreatePlayer);
    }

    public void CreateCharacter() {
        MainMenuCamera.Instance.RotateCamera(90, () => {
            uiStack.Push("CharacterCreation");
        });
    }
    
    public void NextCharacter(int value) {
        selectedCharacterIndex = (int)Mathf.Repeat(selectedCharacterIndex, characterInfo.Count);

        characterInfo.Shift(value);

        if (characterInfo[selectedCharacterIndex + value].Name == "New Character") {
            PlayButton.onClick.RemoveAllListeners();
            PlayButton.onClick.AddListener(CreateCharacter);
            PlayButtonText.text = "Create";
        }
        else {
            PlayButton.onClick.RemoveAllListeners();
            PlayButton.onClick.AddListener(Play);
            PlayButtonText.text = "Play";
        }

        DisplayCharacterGraphics();
        DisplayData(characterInfo[selectedCharacterIndex]);

        selectedCharacterIndex += value;
    }

    private void DisplayCharacterGraphics() {
        foreach (Socket spawnPoint in SpawnPoints)
            spawnPoint.Delete();

        for (int i = 0; i < SpawnPoints.Length; i++) {
            SpawnPoints[i].Spawn(characterInfo[i].ClassModel,
                new Vector3(0, 11, 0));
        }
    }

    private void DisplayData(CharacterInfo info) {
        CharacterName.text = info.Name;
        CharacterLevel.text = info.Level.ToString();
        CharacterClass.text = info.Class.ToString();


    }
}
