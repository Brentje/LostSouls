﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SelectKeyButton : Button
{
    public InputAction Action;
    public KeyCode Key;

    public bool isPressed;

    public Text ActionText;
    public Text OutputText;

    protected override void Awake()
    {
        base.Awake();
        onClick.AddListener(OnClick);
    }

    protected override void Start()
    {
        base.Start();

        ActionText.text = Action.ToString();
        if (Key != KeyCode.None)
            OutputText.text = Key.ToString();
        else OutputText.text = "";
    }

    public void Update()
    {
        if (isPressed)
        {
            if (Input.anyKeyDown)
            {
                KeyCode[] buttons = (KeyCode[])Enum.GetValues(typeof(KeyCode));
                for (int i = 0; i < buttons.Length; i++)
                {
                    if (Input.GetKeyDown(buttons[i]))
                    {
                        Key = buttons[i];
                        OutputText.text = buttons[i].ToString();
                        isPressed = false;
                    }
                }
            }
        }
    }

    public void OnClick()
    {
        isPressed = true;
        OutputText.text = "Press any key";
    }
}
