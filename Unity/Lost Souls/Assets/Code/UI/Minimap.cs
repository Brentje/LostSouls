﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    private Camera minimapCamera;
    private GameObject minimapCameraGameObject;
    private Player player;
    public RenderTexture minimapRenderTexture;
    public Vector3 offset;
    public LayerMask CullingMask;

    // Use this for initialization
    void Start()
    {
        player = ActorManager.GetActivePlayer();
        minimapCameraGameObject = new GameObject("MinimapCamera");
        minimapCamera = minimapCameraGameObject.AddComponent<Camera>();
        minimapCamera.cullingMask = CullingMask;
        minimapCameraGameObject.transform.rotation = Quaternion.Euler(90, 0, 0);

        if (player != null)
            minimapCameraGameObject.transform.position = player.transform.position + offset;

        minimapCamera.targetTexture = minimapRenderTexture;
        minimapCamera.orthographic = true;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player == null)
            return;

        minimapCameraGameObject.transform.position = player.transform.position + offset;
    }

    private void OnDestroy()
    {
        Destroy(minimapCameraGameObject);
    }
}
