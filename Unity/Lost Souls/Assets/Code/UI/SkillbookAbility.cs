﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillbookAbility : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
{
    public Ability Ability;
    [HideInInspector]
    public Skillbook Skillbook;
    private Player player;

    [SerializeField]
    private Image abilityIconImage;

    // Use this for initialization
    void Start()
    {
        player = ActorManager.GetActivePlayer();
        abilityIconImage.sprite = Resources.Load<Sprite>(Ability.IconSourcePath);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Skillbook.ActivateTooltip(Ability);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Skillbook.DeactivateTooltip();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (eventData.button)
        {
            case PointerEventData.InputButton.Right:
                player.AbilitySystem.AddSelectedAbility(Ability);
                break;

            case PointerEventData.InputButton.Left:
                if (eventData.clickCount == 2)
                    player.AbilitySystem.RemoveSelectedAbility(Ability);
                break;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }

    void OnDestroy()
    {
        Skillbook.DeactivateTooltip();
    }
}