﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : UIElement
{
    private static ItemContainer nextContainer;

    public Transform ContentObject;

    public ItemSlotUI ItemSlotPrefab;

    private ItemContainer itemContainer;
    private List<ItemSlotUI> itemslots;

    public static void OpenContainer(ItemContainer container)
    {
        nextContainer = container;
        UIStack stack = GameObject.FindObjectOfType<UIStack>();
        if (stack)
            stack.Push("ItemContainer");
    }

    public static void CloseContainer(ItemContainer container)
    {
        UIStack stack = GameObject.FindObjectOfType<UIStack>();
        if (stack)
            stack.Remove("ItemContainer");
    }

    public void OnClick(int buttonIndex)
    {
        Debug.Log(CursorItemSlot.Instance);
        Debug.Log(CursorItemSlot.Instance.slot);
        if (CursorItemSlot.Instance.slot.IsOccupied())
        {
            if (buttonIndex < itemContainer.items.Count)
                itemContainer.items[buttonIndex] = CursorItemSlot.Instance.slot.SwitchItem(itemContainer.items[buttonIndex]);
            else
                itemContainer.AddItemToInventory(CursorItemSlot.Instance.slot.SwitchItem(null));
        }
        else
        {
            if (buttonIndex < itemContainer.items.Count)
            {
                CursorItemSlot.Instance.slot.SwitchItem(itemContainer.items[buttonIndex]);
                itemContainer.RemoveItemFromInventory(itemContainer.items[buttonIndex]);
                //itemContainer.items.RemoveAt(buttonIndex);
            }
        }
        UpdateItems();
    }

    public void AddFromCursor()
    {
        if (CursorItemSlot.Instance.slot.IsOccupied())
        {
            if (itemContainer.CheckForSpaceForItem(CursorItemSlot.Instance.slot.GetCurrentItem()))
            {
                itemContainer.AddItemToInventory(CursorItemSlot.Instance.slot.SwitchItem(null));
                UpdateItems();
            }
        }
    }

    public void UpdateItems()
    {
        List<Item> items = itemContainer.items;
        int requiredSlots = items.Count;
        requiredSlots += 8 - requiredSlots % 8 + 8;
        if (requiredSlots >= items.Count + 16)
            requiredSlots -= 8;
        while (itemslots.Count < requiredSlots)
        {
            ItemSlotUI temp = Instantiate(ItemSlotPrefab, ContentObject);
            int i1 = itemslots.Count;
            temp.SetItem(null, () => { OnClick(i1); });
            itemslots.Add(temp);
        }
        for (int i = 0; i < items.Count; i++)
        {
            int i1 = i;
            itemslots[i].SetItem(items[i], () => { OnClick(i1); });
        }
        for (int i = items.Count; i < itemslots.Count; i++)
        {
            int i1 = i;
            itemslots[i].SetItem(null, () => { OnClick(i1); });
        }
        while (itemslots.Count > requiredSlots)
        {
            Destroy(itemslots[items.Count].gameObject);
            itemslots.RemoveAt(items.Count);
        }
    }

    public override void Enter()
    {
        CursorItemSlot x = CursorItemSlot.Instance;
        if (nextContainer != null)
        {
            itemContainer = nextContainer;
            nextContainer = null;
        }
        else
        {
            itemContainer = ActorManager.GetActivePlayer().Inventory;
        }

        itemslots = new List<ItemSlotUI>();

        UpdateItems();
    }

    public override void Exit()
    {
    }
}