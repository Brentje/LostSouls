﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// A button for itemslots
/// </summary>
public class ItemSlotUI : Button
{
    // Image that displays the item
    public Image ItemImage;

    protected override void Awake()
    {
        // Get the Image Component from the child
        if (transform.childCount > 0)
            ItemImage = transform.GetChild(0).GetComponent<Image>();
    }

    /// <summary>
    /// Set the item and callback
    /// </summary>
    public void SetItem(Item item, UnityAction clickAction)
    {
        // Get the Image Component from the child
        if (transform.childCount > 0)
            ItemImage = transform.GetChild(0).GetComponent<Image>();

        // Update the Gra
        UpdateGraphics(item);

        // Clear the onClick and add the new Action
        onClick.RemoveAllListeners();
        onClick.AddListener(clickAction);
    }

    /// <summary>
    /// Set the graphics of the image
    /// </summary>
    /// <param name="item"></param>
    public void UpdateGraphics(Item item)
    {
        // If Item is null set sprite to null (invisible)
        if (item == null)
        {
            ItemImage.sprite = null;
        }
        else // Else set the sprite equal to the items sprite
        {
            ItemImage.sprite = item.GetSprite();
        }
    }
}
