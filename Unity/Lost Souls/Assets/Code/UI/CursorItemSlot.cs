﻿using UnityEngine;
using UnityEngine.UI;
using Utility;

public class CursorItemSlot : SingletonWithAction<CursorItemSlot>
{
    public ItemSlotWithEvent slot;
    public Image ItemImage;

    private UIStack UIStack;

    public void Start()
    {
        UIStack = FindObjectOfType<UIStack>();
        ItemImage = gameObject.AddComponent<Image>();
        ItemImage.raycastTarget = false;
        ItemImage.color = Color.clear;
        slot.OnSwitch += SlotOnSwitch;
    }

    public void Update()
    {
        transform.position = Input.mousePosition;
        UpdatePos();
    }

    public CursorItemSlot()
    {
        slot = new ItemSlotWithEvent();
    }

    private void SlotOnSwitch(ItemSlot item)
    {
        if (item.GetCurrentItem() != null)
        {
            ItemImage.sprite = item.GetCurrentItem().GetSprite();
            ItemImage.color = Color.white;
        }
        else
            ItemImage.color = Color.clear;
    }

    public void UpdatePos()
    {
        if (UIStack)
        {
            transform.SetParent(null);
            transform.SetParent(UIStack.transform);
        }
    }

    protected override void OnGet()
    {
        UpdatePos();
    }
}
