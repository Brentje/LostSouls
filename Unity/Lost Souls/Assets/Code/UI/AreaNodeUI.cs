﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaNodeUI : MonoBehaviour
{
    public uint Act = 0;
    public uint AreaID = 0;
    [SerializeField]
    private GameObject unlockedWaypointButton;

    void Start()
    {
        unlockedWaypointButton.SetActive(WorldConfig.Instance.Acts[(int)Act].Areas[(int)AreaID].WaypointUnlocked);
        //unlockedWaypointButton.SetActive(GameManager.Instance.Acts[(int)Act].Areas[(int)AreaID].WaypointUnlocked);

        SceneManager.AddSceneLoadedDelegate(WorldConfig.Instance.Acts[(int)Act].Areas[(int)AreaID].WaypointCallback);
    }


    public void TeleportToWaypoint()
    {
        SceneManager.LoadSceneAsync(WorldConfig.Instance.Acts[(int)Act].Areas[(int)AreaID].Name);
        //SceneManager.LoadSceneAsync(GameManager.Instance.Acts[(int)Act].Areas[(int)AreaID].Name);
    }
}
