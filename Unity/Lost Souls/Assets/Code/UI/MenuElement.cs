﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MenuElement : UIElement
{
    public abstract override void Enter();


    public void QuitGame()
    {
        Application.Quit();
    }

    public abstract override void Exit();
}
