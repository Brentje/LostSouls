﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class MainMenuCamera : MonoBehaviour {
    public float Duration;
    public OptionMenu Options;

    private Coroutine routine;

    public static MainMenuCamera Instance { get; private set; }

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        Options.Reset();
    }

    public void RotateCamera(float angle, UnityAction success) {
        routine = StartCoroutine(RotateCamera_Internal(angle, success));
    }

    private IEnumerator RotateCamera_Internal(float angle, UnityAction success) {
        float elapsedTime = 0;
        Vector3 target = new Vector3(0, angle, 0);
        Quaternion start = transform.rotation;
        Quaternion final = Quaternion.Euler(transform.rotation.eulerAngles + target);

        while (elapsedTime <= Duration) {
            transform.rotation = Quaternion.Slerp(start, final, Mathf.SmoothStep(0, 1, elapsedTime / Duration));
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
        transform.rotation = final;

        success?.Invoke();
    }
}