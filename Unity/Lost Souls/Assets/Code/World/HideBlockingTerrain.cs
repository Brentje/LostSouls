﻿using UnityEngine;

[ExecuteInEditMode]
public class HideBlockingTerrain : MonoBehaviour
{
    public Material mat;

    public void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
            Graphics.Blit(src, dest, mat);
    }
}