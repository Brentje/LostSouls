﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct ActData {
    public string Name;
    public Sprite ActMap;
    public GameObject WaypointUI;
    public bool Unlocked;
    public List<Area> Areas;
}

[System.Serializable]
public class Area {
    public string Name;
    public string SceneName;
    public bool Unlocked;
    public bool WaypointUnlocked;
    [HideInInspector]
    public Waypoint Waypoint;
    public List<int> ConnectedAreas;

    //This is executed 3 times?
    public void WaypointCallback(Scene scene, LoadSceneMode mode) {
        if (scene.name == SceneName)
            Waypoint = GameObject.FindObjectOfType<Waypoint>();

        Waypoint target = GameObject.FindObjectOfType<Waypoint>();

        float height = ActorManager.GetActorHeight(ActorManager.GetActivePlayer());
        Vector3 final = new Vector3(target.transform.position.x + Random.Range(1, 2), target.transform.position.y + height, target.transform.position.z + Random.Range(1, 2));
        Player player = ActorManager.GetActivePlayer();
        player.transform.position = final;
        player.Agent.enabled = false;
        player.Agent.enabled = true;
        SceneManager.RemoveSceneLoadedDelegate(WaypointCallback);
    }
}

[CreateAssetMenu(fileName = "AreaConfig", menuName = "Data/AreaConfig")]
public class WorldConfig : ScriptableObject {
    public List<ActData> Acts;

    private static WorldConfig _instance;

    public static WorldConfig Instance {
        get {
            if (!_instance)
                _instance = Resources.Load<WorldConfig>("Configs/AreaConfig");

            return _instance;
        }
    }
}