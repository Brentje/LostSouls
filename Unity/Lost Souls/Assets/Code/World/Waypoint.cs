﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : Interactable
{
    private UIStack uiStack;
    public Color HighlightColor = Color.white;
    private Color DefaultColor;
    private Material material;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
        DefaultColor = material.color;
    }

    private void Start()
    {
        uiStack = FindObjectOfType<UIStack>();
        SubscribeToInteraction(OnInteract);
    }

    public void OnInteract(Actor actor)
    {
        
        uiStack.Push("WorldMap");
    }

    public void OnMouseEnter()
    {
        material.color = HighlightColor;
    }

    public void OnMouseExit()
    {
        material.color = DefaultColor;
    }
}
