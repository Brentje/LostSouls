﻿Shader "LostSoulsCostum/HideMiddle"
{
	Properties
	{
		_MainTex ("Main texture", 2D) = "white" {}
		_Overlay ("Overlay", 2D) = "white" {}
		_Depth ("Depth", 2D) = "white" {}
		_DepthOverlay ("Depth overlay", 2D) = "white" {}
		_AlphaTex ("Alpha texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _Overlay;
			sampler2D _Depth;
			sampler2D _DepthOverlay;
			sampler2D _AlphaTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col1 = tex2D(_MainTex, i.uv);
				fixed4 col2 = tex2D(_Overlay, i.uv);

				col1.a = 1;
				col2.a = 1;

				fixed depth1 = tex2D(_Depth, i.uv);
				fixed depth2 = tex2D(_DepthOverlay, i.uv);

				if(depth1 < depth2)
					return  lerp (col2, col1, tex2D(_AlphaTex, i.uv).r);
				return col1;
			}
			ENDCG
		}
	}
}
