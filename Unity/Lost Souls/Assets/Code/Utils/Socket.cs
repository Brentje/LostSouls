﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Socket : MonoBehaviour {
    private GameObject instance;

    public void Spawn(GameObject gameObject, Vector3 offset) {
        instance = Instantiate(gameObject, transform.position, Quaternion.identity);
    }

    public void Delete() {
        Destroy(instance);
    }

}