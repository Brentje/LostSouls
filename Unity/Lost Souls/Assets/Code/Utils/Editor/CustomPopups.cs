﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public struct CustomPopups
{
    public class TextInputPopup : PopupWindowContent
    {
        public string text = "";

        private UnityAction<string> onSave;

        public TextInputPopup(UnityAction<string> onSave)
        {
            this.onSave = onSave;
        }

        public override void OnGUI(Rect rect)
        {
            GUILayout.Label("Enter key", EditorStyles.boldLabel);
            GUILayout.Label("Name:");
            text = EditorGUILayout.TextField(text);
            if (GUILayout.Button("Save"))
                onSave(text);
        }
    }

    public class SelectInputPopup : PopupWindowContent
    {
        private string[] options;
        private int index;

        private UnityAction<string> onSelect;

        public SelectInputPopup(UnityAction<string> onSelect, string[] options)
        {
            this.onSelect = onSelect;
            this.options = options;
        }

        public override void OnGUI(Rect rect)
        {
            GUILayout.Label("Enter key", EditorStyles.boldLabel);
            index = EditorGUILayout.Popup(index, options);
            if (GUILayout.Button("Load"))
                onSelect(options[index]);
        }
    }
}
