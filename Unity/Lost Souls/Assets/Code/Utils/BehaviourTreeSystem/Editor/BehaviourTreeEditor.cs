﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

public class BehaviourTreeEditor : EditorWindow
{
    //List of the window nodes
    private List<Rect> windows = new List<Rect>();

    //List of actual nodes of the behaviour tree
    private List<BTNode> btNodes = new List<BTNode>();

    //The position in x,y and the size in width and height
    private Rect nodeRect = new Rect(300, 150, 200, 200);

    //The behaviour tree being made
    private BehaviourTree behaviourTree;

    //The node that is creating a transition
    private int transitionNodeID = -1;

    //Is there a transition being made?
    private bool isMakingTransition = false;

    //Drag offset
    private Vector2 offset;

    //Initialize the editor window
    [MenuItem("Window/Tools/BehaviourTreeEditor")]
    private static void Init()
    {
        //Get the editor window
        BehaviourTreeEditor window = (BehaviourTreeEditor)GetWindow(typeof(BehaviourTreeEditor));

        //Set the title of the window
        window.titleContent = new GUIContent("Behaviour Tree Editor");

        //Show the window
        window.Show();

        window.offset = Vector2.zero;
    }

    //Initializing
    private void Awake()
    {
        //Create a new behaviour tree
        behaviourTree = new BehaviourTree(new BTSelector());

        btNodes.Add(behaviourTree.RootNode);
        windows.Add(nodeRect);
    }



    //OnGUI calls
    private void OnGUI()
    {

        //Draw the grid
        DrawGrid(nodeRect.width / 10, 0.3f, Color.gray);
        DrawGrid(nodeRect.width, 0.5f, Color.gray);

        //Only run when there are windows
        if (windows.Count > 0)
        {
            //Manage the connections between nodes
            ManageConnections();

            //Draw the windows
            DrawWindows();
        }

        if (btNodes.Count > 0 && behaviourTree.RootNode == null)
        {
            for (int i = 0; i < btNodes.Count; i++)
            {
                btNodes[i].Descendants().ToList().Contains(behaviourTree.RootNode);
            }
        }

        //Check events
        CheckEvents(Event.current);

        //Draw the buttons
        DrawUI();

        //Repaint the editor each frame 
        Repaint();
    }

    //Get the input
    public void CheckEvents(Event ev)
    {
        switch (ev.type)
        {
            case EventType.MouseDown:
            //Check button 1
            if (ev.button == 1)
            {
                for (int i = 0; i < windows.Count; i++)
                {
                    if (windows[i].Contains(ev.mousePosition))
                    {
                        GenericMenuOnNode(ev.mousePosition, i);
                        return;
                    }
                    else
                        GenericMenuOnGrid(Event.current.mousePosition);
                }

                if (windows.Count == 0)
                    GenericMenuOnGrid(Event.current.mousePosition);
            }
            break;

            case EventType.MouseDrag:
            Panning(ev.delta);
            Repaint();
            Event.current.Use();
            break;

            case EventType.KeyDown:
            if (ev.keyCode == KeyCode.P)
                SortTree(behaviourTree.RootNode);
            break;

        }
    }

    //Allow to pan around the grid by dragging around with the mouse
    private void Panning(Vector2 delta)
    {
        //Panning around the grid
        offset += delta;

        //Apply the delta to the position of the windows
        for (int i = 0; i < windows.Count; i++)
        {
            Rect currentWindow = windows[i];
            currentWindow.position += delta;
            windows[i] = currentWindow;
        }
    }

    //Manages the lines between nodes
    private void ManageConnections()
    {
        //Draw the connection of the cursor to the node that started the bind
        DrawCursorConnection(transitionNodeID);

        //Loop through all the nodes
        for (int i = 0; i < btNodes.Count; i++)
        {
            //If the node is a composite node, go through all childs
            if (btNodes[i].GetType().IsSubclassOf(typeof(BTComposite)))
            {
                BTComposite currentCompNode = (BTComposite)btNodes[i];
                for (int j = 0; j < currentCompNode.children.Count; j++)
                    DrawNodeConnection(windows[i], windows[btNodes.IndexOf(currentCompNode.children[j])], i, btNodes.IndexOf(currentCompNode.children[j]));
            }
            //If the node is a decorator node, 
            else if (btNodes[i].GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                BTDecorator currentDecorNode = (BTDecorator)btNodes[i];
                if (currentDecorNode.child != null)
                    DrawNodeConnection(windows[i], windows[btNodes.IndexOf(currentDecorNode.child)], i, 0);
            }
        }
    }

    //Draw the windows
    private void DrawWindows()
    {
        //Draw the windows
        BeginWindows();
        for (int i = 0; i < windows.Count; i++)
            windows[i] = GUI.Window(i, windows[i], DrawNodeWindow, btNodes[i].GetType().ToString());

        EndWindows();
    }

    //Draw the buttons in the left upper corner
    private void DrawUI()
    {
        //Start horizontal layout
        GUILayout.BeginHorizontal();

        //Create a save node button
        if (GUILayout.Button("Save Node Grid", GUILayout.Height(20), GUILayout.Width(150)))
        {
            var pops = new CustomPopups.TextInputPopup(SaveGrid);
            PopupWindow.Show(GUILayoutUtility.GetRect(200, 30), pops);
        }

        //Create a load node button
        if (GUILayout.Button("Load Node Grid", GUILayout.Height(20), GUILayout.Width(150)))
        {
            string[] keys = AbilityBehaviourDatabase.Database.Abilities.Keys.ToArray();
            var pops = new CustomPopups.SelectInputPopup(LoadGrid, keys);
            PopupWindow.Show(GUILayoutUtility.GetRect(200, 30), pops);
        }

        GUILayout.EndHorizontal();
    }

    private void SaveGrid(string key)
    {
        Debug.Log(key);
        if (!AbilityBehaviourDatabase.Database.Abilities.ContainsKey(key))
            AbilityBehaviourDatabase.Database.Abilities.Add(key, DataManager.SerializeToXml((BTNode)behaviourTree));
        else
        {
            bool shouldAdd = EditorUtility.DisplayDialog("Warning",
                "The database already has the key: " + key + ". Continue?",
                "yes", "no");
            if (shouldAdd)
            {
                AbilityBehaviourDatabase.Database.Abilities[key] =
                    DataManager.SerializeToXml((BTNode)behaviourTree);
            }
        }
        EditorUtility.SetDirty(AbilityBehaviourDatabase.Database);
    }

    //Load the grid and clear the grid if wanted
    private void LoadGrid(string key)
    {
        btNodes.Clear();
        windows.Clear();

        if (!AbilityBehaviourDatabase.Database.Abilities.ContainsKey(key))
        {
            Debug.LogError("key not found. (" + key + ")");
        }
        //Load the behaviour tree
        behaviourTree =
            (BehaviourTree)DataManager.DeSerializeFromXml<BTNode>(AbilityBehaviourDatabase.Database.Abilities[key]);

        List<BTNode> nodeList = behaviourTree.RootNode.Descendants().ToList();

        //Reset the offset
        offset = Vector2.zero;

        //Loop through the node list and create them
        for (int i = 0; i < nodeList.Count; i++)
            AddNode(nodeList[i], nodeRect.position);

        SortTree(behaviourTree.RootNode);
    }

    //Sort the tree for a nice display of the grid NOTE: branches should move to the side based on the branch to the rights nodes going out on the left side
    private void SortTree(BTNode node)
    {
        //Get the descendants of the node
        BTNode[] descendants = node.Descendants().ToArray();

        //loop through the descending nodes
        for (int i = 0; i < descendants.Length; i++)
        {
            //Check the type of node we are dealing with
            if (descendants[i].GetType().IsSubclassOf(typeof(BTComposite)))
            {
                //Create an instance of the type node
                BTComposite comp = (BTComposite)descendants[i];

                //Loop through the children
                for (int j = 0; j < comp.children.Count; j++)
                {
                    //Create the index
                    float posIndex = j;

                    ////If its an even number give the pos a offset
                    //if (comp.children.Count % 2 == 0)
                    //    posIndex += 0.5f;

                    //Set the position of the window
                    windows[btNodes.IndexOf(comp.children[j])] = new Rect(windows[btNodes.IndexOf(descendants[i])].x
                        + (((-comp.children.Count / 2 + posIndex) * comp.children[j].Descendants().Count()) * nodeRect.width * 2),              //x
                        windows[btNodes.IndexOf(descendants[i])].y + nodeRect.width * 2,                                                        //y
                        windows[btNodes.IndexOf(descendants[i])].width,                                                                         //width
                        windows[btNodes.IndexOf(descendants[i])].height);                                                                       //Height
                }
            }
            else if (descendants[i].GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                //Create an instance of the type node
                BTDecorator decorator = (BTDecorator)descendants[i];

                if (decorator.child == null)
                    continue;

                //Set the position of the window
                windows[btNodes.IndexOf(decorator.child)] = new Rect(windows[btNodes.IndexOf(descendants[i])].x,
                 windows[btNodes.IndexOf(descendants[i])].y + nodeRect.width * 2,
                 windows[btNodes.IndexOf(descendants[i])].width,
                 windows[btNodes.IndexOf(descendants[i])].height);
            }
        }
    }

    //Create a right click menu when clicking on a node
    private void GenericMenuOnNode(Vector2 mousePosition, int nodeID)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Make transition"), false, () =>
        {
            if (!btNodes[nodeID].GetType().IsSubclassOf(typeof(BTLeaf)))
            {
                isMakingTransition = true;
                transitionNodeID = nodeID;
            }
        });

        genericMenu.AddItem(new GUIContent("Delete Node"), false, () =>
        {
            DeleteNode(nodeID);
        });

        genericMenu.AddItem(new GUIContent("Set as Root Node"), false, () =>
        {
            behaviourTree.RootNode = btNodes[nodeID];
        });
        genericMenu.ShowAsContext();
    }

    //Create a right click menu when clicking on the grid
    private void GenericMenuOnGrid(Vector2 mousePosition)
    {
        //Get the types that derive from BTNode
        Type[] types = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(assembly => assembly.GetTypes())
                        .Where(currentType => currentType.IsSubclassOf(typeof(BTNode))).ToArray();

        //Create a generic menu
        GenericMenu genericMenu = new GenericMenu();

        //Loop through the types and create a menu item
        foreach (Type type in types)
        {
            if (type == typeof(BehaviourTree))
                continue;

            BTNode node = (BTNode)Activator.CreateInstance(type);

            if (type.IsSubclassOf(typeof(BTComposite)))
            {
                genericMenu.AddItem(new GUIContent("Composite Nodes/Add " + type + " Node"),
                    false, () => AddNode(node, mousePosition));
            }
            else if (type.IsSubclassOf(typeof(BTDecorator)))
            {
                genericMenu.AddItem(new GUIContent("Decorator Nodes/Add " + type + " Node"),
                    false, () => AddNode(node, mousePosition));
            }
            else if (type.IsSubclassOf(typeof(BTLeaf)))
            {
                genericMenu.AddItem(new GUIContent("Leaf Nodes/Add " + type + " Node"),
                    false, () => AddNode(node, mousePosition));
            }
        }

        //Show the menu as context
        genericMenu.ShowAsContext();
    }

    //Add a node on the spot the player right clicked
    private void AddNode(BTNode node, Vector2 mousePosition)
    {
        btNodes.Add(node);
        windows.Add(new Rect(mousePosition.x, mousePosition.y, nodeRect.width, nodeRect.height));
    }

    //Delete a Node
    private void DeleteNode(int id)
    {
        //Loop through the nodes
        for (int i = 0; i < btNodes.Count; i++)
        {
            //Check the type of node we are currently at
            if (btNodes[i].GetType().IsSubclassOf(typeof(BTComposite)))
            {
                //Delete the node from the child list
                BTComposite comp = (BTComposite)btNodes[i];
                if (comp.children.Contains(btNodes[id]))
                    comp.RemoveChild(btNodes[id]);
            }
            else if (btNodes[i].GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                //Set the child to null
                BTDecorator deco = (BTDecorator)btNodes[i];
                if (deco.child == btNodes[id])
                    deco.child = null;
            }
        }

        if (btNodes[id] == behaviourTree.RootNode)
            for (int i = 0; i < btNodes.Count; i++)
                if (btNodes[i].Descendants().ToList().Contains(behaviourTree.RootNode))
                    behaviourTree.RootNode = btNodes[i];

        //Remove the node from both lists
        windows.Remove(windows[id]);
        btNodes.Remove(btNodes[id]);
    }

    //Draw the editor content
    public void DrawNodeWindow(int id)
    {
        //ID 0 makes it the root node, root node specifics should be in this if
        if (btNodes[id] == behaviourTree.RootNode)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Root Node", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();
        }
        else if (isMakingTransition && Event.current.button == 0 && Event.current.type == EventType.MouseDown && id != transitionNodeID)
        {
            if (btNodes[transitionNodeID].GetType().IsSubclassOf(typeof(BTComposite)))
            {
                BTComposite composite = (BTComposite)btNodes[transitionNodeID];
                composite.AddChild(btNodes[id]);
            }
            else if (btNodes[transitionNodeID].GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                BTDecorator decorator = (BTDecorator)btNodes[transitionNodeID];
                decorator.child = btNodes[id];
            }
            AssetDatabase.Refresh();
            // EditorUtility.SetDirty(behaviourTree);
            AssetDatabase.SaveAssets();
            isMakingTransition = false;
            transitionNodeID = -1;
        }

        //Get the fieldinfo of every field in the current nodes type
        System.Reflection.FieldInfo[] info = btNodes[id].GetType().GetFields();

        //Loop through the field infos
        for (int i = 0; i < info.Length; i++)
        {
            //Draw the field based on its type within the current node
            if (info[i].FieldType == typeof(int))
            {
                //This will draw a label with the name of the variable and an input field to set the value
                GUILayout.BeginHorizontal();
                GUILayout.Label(info[i].Name);
                info[i].SetValue(btNodes[id], EditorGUILayout.IntField((int)info[i].GetValue(btNodes[id])));
                GUILayout.EndHorizontal();
            }
            else if (info[i].FieldType == typeof(float))
            {
                //This will draw a label with the name of the variable and an input field to set the value
                GUILayout.BeginHorizontal();
                GUILayout.Label(info[i].Name);
                info[i].SetValue(btNodes[id], EditorGUILayout.FloatField((float)info[i].GetValue(btNodes[id])));
                GUILayout.EndHorizontal();
            }
            else if (info[i].FieldType == typeof(string))
            {
                //This will draw a label with the name of the variable and a textarea to set the message
                GUILayout.BeginVertical();
                GUILayout.Label(info[i].Name);
                object value = info[i].GetValue(btNodes[id]);
                info[i].SetValue(btNodes[id], GUILayout.TextArea(value?.ToString() ?? ""));
                GUILayout.EndVertical();
            }
            else if (info[i].FieldType == typeof(bool))
            {
                //This will draw a label with the name of the variable and a toggle to set the bool
                GUILayout.BeginHorizontal();
                GUILayout.Label(info[i].Name);
                info[i].SetValue(btNodes[id], GUILayout.Toggle((bool)info[i].GetValue(btNodes[id]), ""));
                GUILayout.EndHorizontal();
            }
            else if (info[i].FieldType == typeof(Vector3))
            {
                //This will draw a label with the name of the variable and a toggle to set the bool
                GUILayout.BeginVertical();
                GUILayout.Label(info[i].Name);
                GUILayout.EndVertical();

                GUILayout.BeginHorizontal();
                Vector3 vec3 = Vector3.zero;

                float.TryParse(GUILayout.TextArea(((Vector3)info[i].GetValue(btNodes[id])).x.ToString()), out vec3.x);
                float.TryParse(GUILayout.TextArea(((Vector3)info[i].GetValue(btNodes[id])).y.ToString()), out vec3.y);
                float.TryParse(GUILayout.TextArea(((Vector3)info[i].GetValue(btNodes[id])).z.ToString()), out vec3.z);

                info[i].SetValue(btNodes[id], vec3);

                GUILayout.EndHorizontal();
            }
            //else if (info[i].FieldType == typeof(BTParallel.Policy))
            //{
            //    //This will draw a label with the name of the variable and a enum popup to set its value
            //    GUILayout.BeginVertical();
            //    GUILayout.Label(info[i].Name);
            //    info[i].SetValue(btNodes[id],
            //        EditorGUILayout.EnumPopup((Enum)Enum.Parse(typeof(BTParallel.Policy),
            //        info[i].GetValue(btNodes[id]).ToString())));
            //    GUILayout.EndVertical();
            //}
            else if (info[i].FieldType.IsSubclassOf(typeof(Enum)))
            {
                //This will draw a label with the name of the variable and a enum popup to set its value
                GUILayout.BeginVertical();
                GUILayout.Label(info[i].Name);
                info[i].SetValue(btNodes[id],
                    EditorGUILayout.EnumPopup((Enum)Enum.Parse(info[i].FieldType,
                        info[i].GetValue(btNodes[id]).ToString())));
                GUILayout.EndVertical();
            }
        }

        //Allow dragging the window if it is in focus
        if (Event.current.button == 0)
            GUI.DragWindow();


    }

    //Draw a grid using the GUI handles
    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        //Calculate how many lines would fit based on the spacing
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        //Create a offset based on the offset of the mouse delta
        Vector3 newOffSet = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        //Create the lines of the grid that are vertical
        for (int i = 0; i <= widthDivs; i++)
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffSet, new Vector3(gridSpacing * i, position.height + gridSpacing, 0f) + newOffSet);

        //Create the lines of the grid that are horizontal
        for (int j = 0; j <= heightDivs; j++)
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffSet, new Vector3(position.width + gridSpacing, gridSpacing * j, 0f) + newOffSet);

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    //Draw the beziercurves between the rects
    private void DrawNodeConnection(Rect start, Rect end, int parentID, int childID)
    {
        Vector3 startPos = new Vector3(start.x + start.width / 2, start.y + start.height, 0);
        Vector3 endPos = new Vector3(end.x + end.width / 2, end.y, 0);
        Vector3 startTan = startPos + Vector3.up * 100;
        Vector3 endTan = endPos + Vector3.down * 100;
        Color shadowCol = new Color(0, 0, 0, 0.06f);

        //Draw shadows
        for (int i = 0; i < 3; i++)
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);

        //Draw the line
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.red, null, 1);

        //Draw the button on the line for removing the child
        if (Handles.Button((start.center + end.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
        {
            //If the node is a composite node, go through all childs
            if (btNodes[parentID].GetType().IsSubclassOf(typeof(BTComposite)))
            {
                BTComposite currentCompNode = (BTComposite)btNodes[parentID];
                if (currentCompNode.children.Count > 0)
                    currentCompNode.RemoveChild(btNodes[childID]);
            }
            //If the node is a decorator node, 
            else if (btNodes[parentID].GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                BTDecorator currentDecorNode = (BTDecorator)btNodes[parentID];
                if (currentDecorNode.child != null)
                    currentDecorNode.child = null;
            }
        }
    }

    //Draw the line to the cursor 
    private void DrawCursorConnection(int nodeID)
    {
        if (nodeID < 0 || nodeID >= windows.Count)
            return;

        Vector3 startPos = new Vector3(windows[nodeID].x + windows[nodeID].width / 2, windows[nodeID].y + windows[nodeID].height, 0);
        Vector3 endPos = Event.current.mousePosition;
        Vector3 startTan = startPos + Vector3.up * 100;
        Vector3 endTan = endPos + Vector3.down * 100;
        Color shadowCol = new Color(0, 0, 0, 0.06f);

        //Draw shadows
        for (int i = 0; i < 3; i++)
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);

        //Draw the line
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.red, null, 1);

    }
}
