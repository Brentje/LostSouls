﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackboard
{
    //The actor of this blackboard
    protected Actor actor;

    //Dictionary of ints
    protected Dictionary<string, int> ints = new Dictionary<string, int>();

    //Dictionary of bools
    protected Dictionary<string, bool> bools = new Dictionary<string, bool>();

    //Dictionary of strings
    protected Dictionary<string, string> strings = new Dictionary<string, string>();

    //Dictionary of floats
    protected Dictionary<string, float> floats = new Dictionary<string, float>();

    //Dictionary of doubles
    protected Dictionary<string, double> doubles = new Dictionary<string, double>();

    //Dictionary of gameobjects
    protected Dictionary<string, GameObject> gameObjects = new Dictionary<string, GameObject>();

    //Dictionary of sprites
    protected Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

    //Dictionary of vector3s
    protected Dictionary<string, Vector3> vector3s = new Dictionary<string, Vector3>();

    //Dictionary of actors
    protected Dictionary<string, Actor> actors = new Dictionary<string, Actor>();

    //Default constructor
    public Blackboard()
    {

    }

    //Set the actor in the constructor
    public Blackboard(Actor actor)
    {
        this.actor = actor;
    }

    //Get the actor of this blackboar
    public Actor GetActor()
    {
        return actor;
    }

    public void SetOwner(Actor actor)
    {
        if (actor != null)
            this.actor = actor;
    }

    //Set an Int
    public void SetInt(string key, int value)
    {
        if (HasInt(key))
        {
            ints[key] = value;
            return;
        }
        ints.Add(key, value);
    }

    //Check to see if the dictionary contains the int
    public bool HasInt(string key)
    {
        return ints.ContainsKey(key);
    }

    //Get the int
    public int GetInt(string key)
    {
        if (HasInt(key))
            return ints[key];
        else
            return 0;
    }

    //Set a bool
    public void SetBool(string key, bool value)
    {
        if (HasBool(key))
        {
            bools[key] = value;
            return;
        }
        bools.Add(key, value);
    }

    //Check if the dictionary contains the bool
    public bool HasBool(string key)
    {
        return bools.ContainsKey(key);
    }

    //Get the bool
    public bool GetBool(string key)
    {
        if (HasBool(key))
            return bools[key];
        else
            return false;
    }

    //Set a string
    public void SetString(string key, string value)
    {
        if (HasString(key))
        {
            strings[key] = value;
            return;
        }
        strings.Add(key, value);
    }

    //Check if the dictionary contains the string
    public bool HasString(string key)
    {
        return strings.ContainsKey(key);
    }

    //Get the string
    public string GetString(string key)
    {
        if (HasString(key))
            return strings[key];
        else
            return "";
    }

    //Set a float
    public void SetFloat(string key, float value)
    {
        if (HasFloat(key))
        {
            floats[key] = value;
            return;
        }
        floats.Add(key, value);
    }

    //Check if the dictionary contains the float
    public bool HasFloat(string key)
    {
        return floats.ContainsKey(key);
    }

    //Get the float
    public float GetFloat(string key)
    {
        if (HasFloat(key))
            return floats[key];
        else
            return 0.0f;
    }

    //Set a double
    public void SetDouble(string key, double value)
    {
        if (HasDouble(key))
        {
            doubles[key] = value;
            return;
        }
        doubles.Add(key, value);
    }

    //Check if the dictionary contains the double
    public bool HasDouble(string key)
    {
        return doubles.ContainsKey(key);
    }

    //Get a double
    public double GetDouble(string key)
    {
        if (HasDouble(key))
            return doubles[key];
        else
            return 0.0;
    }

    //Set a gameobject
    public void SetGameObject(string key, GameObject value)
    {
        if (HasGameObject(key))
        {
            gameObjects[key] = value;
            return;
        }

        gameObjects.Add(key, value);
    }

    //Check if the dictionary contains the gameobject
    public bool HasGameObject(string key)
    {
        return gameObjects.ContainsKey(key);
    }

    //Get a gameobject
    public GameObject GetGameObject(string key)
    {
        if (HasGameObject(key))
            return gameObjects[key];
        else
            return null;
    }

    //Set a vector 3
    public void SetVector3(string key, Vector3 value)
    {
        if (HasVector3(key))
        {
            vector3s[key] = value;
            return;
        }
        vector3s.Add(key, value);
    }

    //Check if the dictionary contains the vector3
    public bool HasVector3(string key)
    {
        return vector3s.ContainsKey(key);
    }

    //Get the vector3
    public Vector3 GetVector3(string key)
    {
        if (HasVector3(key))
            return vector3s[key];
        else
            return Vector3.zero;
    }

    //Set an Actor
    public void SetActor(string key, Actor value)
    {
        if (HasActor(key))
        {
            actors[key] = value;
            return;
        }
        actors.Add(key, value);
    }

    //Check to see if the dictionary contains the Actor
    public bool HasActor(string key)
    {
        return actors.ContainsKey(key);
    }

    //Get the Actor
    public Actor GetActor(string key)
    {
        if (HasActor(key))
            return actors[key];

        Debug.Log("Key \"" + key + "\" not found");
        return null;
    }
}
