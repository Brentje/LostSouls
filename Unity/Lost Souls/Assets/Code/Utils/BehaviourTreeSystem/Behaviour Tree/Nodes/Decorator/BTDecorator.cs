﻿[System.Serializable]
public class BTDecorator : BTNode
{
    //The child of this decorator
    public BTNode child;

    //Decorator default constructor for XML
    public BTDecorator()
    {

    }

    //Initialize the child
    public BTDecorator(BTNode node)
    {
        //Set the child equal to the node
        child = node;
    }

    //Update the child and return its status
    public override NodeStatus Update()
    {
        return status;
    }
}
