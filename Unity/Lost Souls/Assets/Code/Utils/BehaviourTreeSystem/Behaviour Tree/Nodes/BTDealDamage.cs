﻿using System.Xml.Serialization;
using UnityEngine;

[System.Serializable]
public class BTDealDamage : BTLeaf
{
    public float Damage;
    public AttackType Type;
    public string TargetName = "Target";

    public override NodeStatus Update()
    {
        Actor target = blackboard.GetActor(TargetName);
        if (target)
        {
            target.TakeDamage(Damage, Type);
            return NodeStatus.SUCCESS;
        }
        return NodeStatus.FAILURE;
    }
}
