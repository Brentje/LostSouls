﻿using UnityEngine;
[System.Serializable]
public class BTLog : BTLeaf
{
    public string Message = "";

    public override NodeStatus Update()
    {
        Debug.Log("LOG: " + Message);
        return NodeStatus.SUCCESS;
    }
}