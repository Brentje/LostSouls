﻿//The sequence node runs its children at once*.   *if its multithreaded then it does.
//Our implementation will just loop within 1 frame through its childs
//It returns based on policy

using UnityEngine;

[System.Serializable]
public class BTParallel : BTComposite
{
    //The policy by which the parallel node runs
    public enum Policy
    {
        RequireOne,
        RequireAll
    }

    //The fail policy
    public Policy failurePolicy;

    //The succes policy
    public Policy succesPolicy;

    //The default constructor for XML
    public BTParallel()
    {

    }

    //The policy initializing constructor
    public BTParallel(Policy succeedPolicy, Policy failPolicy)
    {
        //Set the policies of this parallel node
        failurePolicy = failPolicy;
        succesPolicy = succeedPolicy;
    }

    //Update the child nodes and return the status
    public override NodeStatus Update()
    {
        //The success and fail rate of this update cycle
        int succeededChilds = 0;
        int failedChilds = 0;

        //Loop through the children and run their tick
        for (int i = 0; i < children.Count; i++)
        {
            NodeStatus status = children[i].Tick();

            //Check the child status increment the success/fail rate
            switch (status)
            {
                case NodeStatus.SUCCESS:

                    //Increment the succeeded childs
                    succeededChilds++;
                
                    //Return if the policy is require one for succes
                    if (succesPolicy == Policy.RequireOne)
                        return NodeStatus.SUCCESS;

                    break;

                case NodeStatus.FAILURE:

                    //Increment failedchilds
                    failedChilds++;

                    //Return if the policy is require one for failure
                    if (failurePolicy == Policy.RequireOne)
                        return NodeStatus.FAILURE;

                    break;

            }
        }

        //if the policy requires all to succeed and the succeeded childs are all the childs return
        if (succeededChilds == children.Count && succesPolicy == Policy.RequireAll)
            return NodeStatus.SUCCESS;

        //if the policy requires all to fail and the failed childs are all the childs return
        if (failedChilds == children.Count && failurePolicy == Policy.RequireAll)
            return NodeStatus.FAILURE;

        //If non of the requirements are met, return running
        return NodeStatus.RUNNING;
    }
}
