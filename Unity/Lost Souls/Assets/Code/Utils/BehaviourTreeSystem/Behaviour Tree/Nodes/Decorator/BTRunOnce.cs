﻿//This node will run once. If the child returns running. it will run until it does not
[System.Serializable]
public class BTRunOnce : BTDecorator
{
    //Has the child run?
    private bool hasRun = false;

    //The child status
    private NodeStatus childStatus;

    //Run the child once or until it doenst return Running
    public override NodeStatus Update()
    {
        //Check if the child is null, if so return failure
        if (child == null)
            return NodeStatus.FAILURE;

        //If the child hasnt ran yet or is still running
        if (!hasRun)
        {
            //Update the child
            childStatus = child.Tick();

            //If the child isnt running set hasrun to true
            if (childStatus != NodeStatus.RUNNING)
                hasRun = true;
        }

        //Return the childstatus of the child
        return childStatus;
    }
}
