﻿//The repeater node will repeat within a single frame
//Until the node returns succes the amount of times the limit wants it to
[System.Serializable]
public class BTRepeater : BTDecorator
{
    //The limit of this repeater node
    public int limit = 0;

    //The counter that holds the number of times the child has succeeded since the last reset
    private int counter;

    //Default Constructor
    public BTRepeater()
    {

    }

    //Contructor with limit set
    public BTRepeater(int limit)
    {
        this.limit = limit;
    }


    //Initialize the counter to be 0
    public override void OnInit()
    {
        counter = 0;
    }

    //Update the child until it succeeds the amount the limits wants it to.
    //If the child returns anything else, return its status.
    public override NodeStatus Update()
    {
        //Fail if there is no child
        if (child == null)
            return NodeStatus.FAILURE;

        //Loop the amount the limit wants it to
        while (limit != counter)
        {
            NodeStatus childStatus = child.Tick();

            if (childStatus == NodeStatus.RUNNING)
                return NodeStatus.RUNNING;

            if (childStatus == NodeStatus.FAILURE)
                return NodeStatus.FAILURE;

            if (limit > 0 && ++counter == limit)
                return NodeStatus.SUCCESS;

        }

        //This should only happen if the limit has been set to 0
        return NodeStatus.FAILURE;
    }
}
