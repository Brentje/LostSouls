﻿using UnityEngine;
[System.Serializable]
public class BTWait : BTLeaf
{
    public float SecondsToWait = 0.0f;
    private float timer;
    private float timePassed = 0.0f;

    public override void OnInit()
    {
        base.OnInit();
        timer = SecondsToWait;
        timePassed = 0.0f;
    }

    public override NodeStatus Update()
    {
        if (timePassed >= timer)
            return NodeStatus.SUCCESS;

        timer -= Time.deltaTime;
        return NodeStatus.RUNNING;
    }
}