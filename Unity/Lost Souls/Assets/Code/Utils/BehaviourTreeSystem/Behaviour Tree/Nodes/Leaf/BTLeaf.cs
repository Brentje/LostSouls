﻿using System.Xml.Serialization;
using UnityEngine;

[System.Serializable]
public class BTLeaf : BTNode
{
    //Blackboard used by this node
    [XmlIgnore]
    public Blackboard blackboard;

    //Default constructor
    public BTLeaf()
    {

    }

    //Constructor that sets the blackboard
    public BTLeaf(Blackboard board)
    {
        blackboard = board;
    }

    //Update the leaf node
    public override NodeStatus Update()
    {
        return NodeStatus.FAILURE;
    }

}

