﻿//The sequence node runs its children 1 by 1 in order.
//When 1 child fails or is still running it returns.

public class BTSequence : BTComposite
{
    //The current index of the children that is being updated 
    protected int childIndex;

    //Initialize the childindex to 0
    public override void OnInit()
    {
        childIndex = 0;    
    }

    //Update the childs and return the status
    public override NodeStatus Update()
    {
        while (childIndex < children.Count)
        {
            NodeStatus currentChildStatus = children[childIndex].Tick();

            if (currentChildStatus != NodeStatus.SUCCESS)
                return currentChildStatus;

            childIndex++;
        }
                    
        //If all the children equal succes return succes
        return NodeStatus.SUCCESS;
    }
}
