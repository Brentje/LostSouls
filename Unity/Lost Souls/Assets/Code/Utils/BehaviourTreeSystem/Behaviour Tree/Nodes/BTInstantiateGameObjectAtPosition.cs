﻿using UnityEngine;

public class BTInstantiateGameObjectAtPosition : BTLeaf
{
    [Tooltip("Relative to resource folder")]
    public string prefabFilePath = "";

    public string instanceName = "";
    public Vector3 position;
    private GameObject gameObjectToInstantiate;

    public override void OnInit()
    {
        base.OnInit();

        if (gameObjectToInstantiate == null)
            gameObjectToInstantiate = Resources.Load<GameObject>(prefabFilePath);
    }

    public override NodeStatus Update()
    {
        if (gameObjectToInstantiate == null)
            return NodeStatus.FAILURE;

        blackboard.SetGameObject(instanceName, Object.Instantiate(gameObjectToInstantiate, position, gameObjectToInstantiate.transform.rotation));
        return NodeStatus.SUCCESS;
    }
}

public class BTInstantiateGameObjectAtTransform : BTLeaf
{
    public string prefabFilePath = "";
    public string instanceName = "";
    private GameObject gameObjectToInstantiate;

    public override void OnInit()
    {
        base.OnInit();

        if (gameObjectToInstantiate == null)
            gameObjectToInstantiate = Resources.Load<GameObject>(prefabFilePath);

    }

    public override NodeStatus Update()
    {
        if (gameObjectToInstantiate == null)
            return NodeStatus.FAILURE;
   
        blackboard.SetGameObject(instanceName, Object.Instantiate(gameObjectToInstantiate, blackboard.GetActor().transform.position, gameObjectToInstantiate.transform.rotation));

        return NodeStatus.SUCCESS;
    }
}

public class BTMoveTowardsDirection : BTLeaf
{
    public string instanceName = "";
    public float range = 0.0f;
    private GameObject gameObjectToMove;
    private Vector3 direction;
    private Vector3 startPosition;

    public override void OnInit()
    {
        base.OnInit();
        gameObjectToMove = blackboard.GetGameObject(instanceName);
        startPosition = gameObjectToMove.transform.position;
        Vector3 heading = Camera.main.WorldToScreenPoint(blackboard.GetActor().transform.position) - Input.mousePosition;
        direction = new Vector3(-heading.x, 0, -heading.y).normalized;
    }   

    public override NodeStatus Update()
    {
        gameObjectToMove.transform.Translate(direction * Time.deltaTime,Space.World);
        if ((gameObjectToMove.transform.position - startPosition).sqrMagnitude >= range * range)
            return NodeStatus.SUCCESS;

        return NodeStatus.RUNNING;
    }
}