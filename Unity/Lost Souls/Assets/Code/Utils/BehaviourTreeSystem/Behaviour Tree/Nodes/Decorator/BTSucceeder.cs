﻿//The succeeder node will return success no matter the child state
[System.Serializable]
public class BTSucceeder : BTDecorator
{
    //Update the child and return success regardless of the child status
    public override NodeStatus Update()
    {
        //Dont update the child if there is none
        if (child != null)
            child.Tick();

        return NodeStatus.SUCCESS;
    }
}
