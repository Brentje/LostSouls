﻿//The until fail node will update its child until it returns failure, then return success
[System.Serializable]
public class BTUntilFail : BTDecorator
{
    //Update the child until it returns failure, then return success
    public override NodeStatus Update()
    {
        //Fail if the child is null
        if (child == null)
            return NodeStatus.FAILURE;

        while(true)
        {
            NodeStatus childStatus = child.Tick();

            if (childStatus == NodeStatus.FAILURE)
                return NodeStatus.SUCCESS;
        }
    }
}
