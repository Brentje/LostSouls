﻿
using System;
using System.Xml.Serialization;
using UnityEngine;

//The status a node can return
public enum NodeStatus
{
    SUCCESS,
    FAILURE,
    RUNNING
}

//Base class for nodes

public abstract class BTNode 
{
    //The status of this node
    protected NodeStatus status;

    //The initialize method for this node
    public virtual void OnInit() { }

    //The tick method 
    public NodeStatus Tick()
    {
        //If the node isnt running, run the init method
        if (status != NodeStatus.RUNNING)
            OnInit();

        //Call the update
        status = Update();

        //Return the status
        return status;
    }

    //Update method for a node
    public abstract NodeStatus Update();

    //Returns the status of this node
    public NodeStatus GetStatus()
    {
        //Return the status
        return status;
    }
}