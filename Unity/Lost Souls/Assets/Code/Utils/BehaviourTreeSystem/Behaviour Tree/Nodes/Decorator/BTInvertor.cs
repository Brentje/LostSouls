﻿//The invertor decorator inverts the childs return status
//The invertor also only has one child
[System.Serializable]
public class BTInvertor : BTDecorator
{
    //Update the child and return its inverted status
    public override NodeStatus Update()
    {
        //Return null if there is no child
        if (child == null)
            return NodeStatus.FAILURE;

        //Get the child status
        NodeStatus childStatus = child.Tick();

        //Invert the childstatus
        switch (childStatus)
        {
            case NodeStatus.SUCCESS:
                return NodeStatus.FAILURE;
            case NodeStatus.RUNNING:
                return NodeStatus.RUNNING;
            case NodeStatus.FAILURE:
                return NodeStatus.SUCCESS;
        }

        return NodeStatus.FAILURE;
    }
}
