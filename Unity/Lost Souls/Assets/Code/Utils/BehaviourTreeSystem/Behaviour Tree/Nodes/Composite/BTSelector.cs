﻿//The sequence node runs its children 1 by 1 in order.
//When 1 child succeeds or is still running it returns.

public class BTSelector : BTComposite
{
    //The current index of the children that is being updated 
    protected int childIndex;

    //Initialize the childindex to 0
    public override void OnInit()
    {
        childIndex = 0;
    }

    //Update the childs and return the status
    public override NodeStatus Update()
    {
        //Loop through the children if a child isnt failing return
        while (childIndex < children.Count)
        {
            NodeStatus currentChildStatus = children[childIndex].Tick();

            if (currentChildStatus != NodeStatus.FAILURE)
                return currentChildStatus;

            childIndex++;
        }

        //If all the children equal failure return failure
        return NodeStatus.FAILURE;
    }
}
