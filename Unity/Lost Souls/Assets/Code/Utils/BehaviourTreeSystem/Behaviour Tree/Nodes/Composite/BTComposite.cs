﻿using System.Collections.Generic;
[System.Serializable]
public class BTComposite : BTNode
{
    //The list of children of this node
    public List<BTNode> children;

    //Initialize the children list
    public BTComposite()
    {
        children = new List<BTNode>();
    }

    //Update children and return the returned status
    public override NodeStatus Update()
    {
        return status;
    }

    //Add a child to the children list
    public void AddChild(BTNode nodeToAdd)
    {
        //Check if the list or the node is null
        if (children == null && nodeToAdd == null)
            return;

        //Add the node to the children list
        children.Add(nodeToAdd);
    }

    //Remove children from the children list
    public void RemoveChild(BTNode nodeToRemove)
    {
        //Check if the list or the node is null
        if (children == null && nodeToRemove == null)
            return;

        //Remove the node from the list
        children.Remove(nodeToRemove);
    }

    //Cleat the children list
    public void ClearChildren()
    {
        //Check if the children list is null
        if (children == null)
            return;

        //Clear the list
        children.Clear();
    }
}
