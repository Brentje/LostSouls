﻿//The until success node will update its childs until it succeeds, then returns success
[System.Serializable]
public class BTUntilSuccess : BTDecorator
{
    //Update the child until it returns success, then return success
    public override NodeStatus Update()
    {
        //Fail if there is no child
        if (child == null)
            return NodeStatus.FAILURE;

        while (true)
        {
            NodeStatus childStatus = child.Tick();

            if (childStatus == NodeStatus.SUCCESS)
                return NodeStatus.SUCCESS;
        }
    }
}
