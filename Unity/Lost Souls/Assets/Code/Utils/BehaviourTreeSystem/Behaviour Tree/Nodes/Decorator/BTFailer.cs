﻿//The failer node will return failure no matter the child status
[System.Serializable]
public class BTFailer : BTDecorator
{
    //Update the child and return failure no matter the child status
    public override NodeStatus Update()
    {
        //Only update the child when he isnt null
        if (child != null)
            child.Tick();

        return NodeStatus.FAILURE;
    }
}
