﻿using UnityEngine;
[System.Serializable]
public class BTWaitIterations : BTLeaf
{
    public int itterationsToWaitFor = 5;
    private int currentItteration;

    public override void OnInit()
    {
        base.OnInit();
        currentItteration = 0;
    }

    public override NodeStatus Update()
    {
        if (currentItteration >= itterationsToWaitFor)
            return NodeStatus.SUCCESS;

        currentItteration++;
        return NodeStatus.RUNNING;
    }
}