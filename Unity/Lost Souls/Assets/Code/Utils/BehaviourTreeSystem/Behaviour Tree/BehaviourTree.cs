﻿//This behaviour tree will hold the root node of the tree, the blackboard and it will run the tree.
//Though, the entire tree can be used as a node.
//Allowing reusability of before made tree. (this can be done with the editor and within code)

using System.Linq;
using UnityEngine;


public class BehaviourTree : BTNode
{
    //The root node of the behaviour tree
    public BTNode RootNode;

    //The blackboard of this tree
    private Blackboard blackboard;

    private string XMLBehaviourTree;

    public BehaviourTree()
    {
        //Create a new blackboard
        blackboard = new Blackboard();
    }

    public BehaviourTree(string behaviourTreeXML)
    {
        if (string.IsNullOrEmpty(behaviourTreeXML))
            return;

        BTNode nodeTree = DataManager.DeSerializeFromXml<BTNode>(behaviourTreeXML);

        if (nodeTree != null)
            RootNode = nodeTree;
    }

    //Constructor with rootnode
    public BehaviourTree(BTNode root)
    {
        //Set the root node
        RootNode = root;

        //Create a new blackboard
        SetBlackboard(new Blackboard());
    }

    //Actor and root node constructor
    public BehaviourTree(Actor actor, BTNode root)
    {
        //Set the root node
        RootNode = root;
        //Create a new blackboard
        SetBlackboard(new Blackboard(actor));
    }

    //Actor and root node constructor
    public BehaviourTree(Actor actor, string behaviourTreeXML)
    {
        if (string.IsNullOrEmpty(behaviourTreeXML))
            return;

        BTNode nodeTree = ((BehaviourTree)DataManager.DeSerializeFromXml<BTNode>(behaviourTreeXML)).RootNode;

        if (nodeTree != null)
            RootNode = nodeTree;

        //Create a new blackboard
        SetBlackboard(new Blackboard(actor));
    }

    //Update the blackboards
    private void UpdateBlackboard()
    {
        //Get all the nodes
        BTNode[] nodes = RootNode.Descendants().ToArray();

        //Loop through the nodes and set the blackboard
        for (int i = 0; i < nodes.Length; i++)
        {
            if (nodes[i].GetType().IsSubclassOf(typeof(BTLeaf)))
                ((BTLeaf)nodes[i]).blackboard = blackboard;
        }

    }

    //Set the blackboard
    public void SetBlackboard(Blackboard board)
    {
        //Check if the board is null
        if (board == null)
            return;

        //Set the board
        blackboard = board;
        // if(RootNode != null)
        UpdateBlackboard();
    }

    //Get the blackboard
    public Blackboard GetBlackboard()
    {
        return blackboard;
    }


    //Get the actor
    public Actor GetActor()
    {
        return blackboard.GetActor();
    }

    public override void OnInit()
    {
        base.OnInit();
        //UpdateBlackboard(); // errors start apearing when you do this
    }

    //Update the root node
    public override NodeStatus Update()
    {
        return RootNode.Tick();
    }

}
