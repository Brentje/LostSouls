﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Create an subclass of this for the serialization to work and if you want a better editor add that subclass to DictionaryPropertyDrawer
/// </summary>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TValue"></typeparam>
[Serializable]
public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField]
    private List<TKey> keys = new List<TKey>();

    [SerializeField]
    private List<TValue> values = new List<TValue>();

    public void OnBeforeSerialize()
    {
        keys.Clear();
        values.Clear();
        foreach (KeyValuePair<TKey, TValue> pair in this)
        {
            keys.Add(pair.Key);
            values.Add(pair.Value);
        }
    }

    public void OnAfterDeserialize()
    {
        this.Clear();

        if (keys.Count != values.Count)
            throw new Exception(
                $"there are {keys.Count} keys and {values.Count} values after deserialization");

        for (int i = 0; i < keys.Count; i++)
            this.Add(keys[i], values[i]);
    }
}