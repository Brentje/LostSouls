﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BehaviourTreeNodeExtension
{
    //Extension method to get all the descendant nodes of a behaviour tree
    public static IEnumerable<BTNode> Descendants(this BTNode root)
    {
        //Create a stack of nodes
        Stack<BTNode> nodes = new Stack<BTNode>(new[] { root });

        //Loop through the stack
        while (nodes.Any())
        {
            //Pop the stack
            BTNode node = nodes.Pop();

            yield return node;

            //Check the node type
            if (node.GetType().IsSubclassOf(typeof(BTComposite)))
            {
                //Create a composite instance
                BTComposite tempComposite = (BTComposite)node;
                //Loop through the childs and push the onto the stack
                foreach (BTNode n in tempComposite.children)
                    nodes.Push(n);
            }
            else if (node.GetType().IsSubclassOf(typeof(BTDecorator)))
            {
                //Create a decorator instance
                BTDecorator tempDecorator = (BTDecorator) node;

                //If the child isnt null push the child onto the stack
                if (tempDecorator.child != null)
                    nodes.Push(tempDecorator.child);
            }

        }
    }
}
