﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utility {
    public static class Extensions {
        /// <summary>
        /// Append two lists together.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source list.</param>
        /// <param name="target">The target list.</param>
        public static void AppendList<T>(this List<T> source, List<T> target) {
            if (source == null)
                source = new List<T>();
            
            foreach (T item in target) 
                source.Add(item);
        }
        
        public static T[] ExpandArray<T>(this T[] array, ref T item) {
            if (array == null)
                array = new T[0];

            T[] newArray = new T[array.Length + 1];
            
            for (int i = 0; i < array.Length; i++) 
                newArray[i] = array[i];
         
            newArray[newArray.Length - 1] = item;

            return newArray;
        }

        /// <summary>
        /// Check if a value is present in an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The specified array.</param>
        /// <param name="obj">The value to search for.</param>
        /// <returns></returns>
        public static bool ArrayContains<T>(this T[] array, T obj) {
            foreach (T item in array) 
                if (item.Equals(obj)) 
                    return true;

            return false;
        }

        /// <summary>
        /// Shuffle all the values in a list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list you want to shuffle.</param>
        /// <param name="seed">The seed.</param>
        public static void ShuffleList<T>(this List<T> list, int seed = 123456) {
            System.Random random = new System.Random(seed);

            for (int i = 0; i < list.Count; i++) {
                int k = random.Next(i + 1);
                T value = list[k];
                list[k] = list[k];
                list[i] = value;
            }
        }

        /// <summary>
        /// Shifts the values of a List by the amount of positions specified.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list which you want to shift.</param>
        /// <param name="positions">The amount of positions to shift.</param>
        public static void Shift<T>(this List<T> list, int positions) {
            for (int i = 0; i < list.Count; i++) {
                float index = i + positions;
                index = Mathf.Repeat(index, list.Count);
 
                list[i] = list[(int)index];
            }
        }
    }
}

