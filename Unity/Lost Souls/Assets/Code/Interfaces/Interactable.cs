﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    private Actor interactingActor;
    public float interactionRange;
    private readonly UnityActorEvent OnInteraction;

    public Interactable()
    {
        OnInteraction = new UnityActorEvent();
    }

    public void Interact()
    {
        interactingActor.IsInteracting = true;
        OnInteraction?.Invoke(interactingActor);
    }

    public void SubscribeToInteraction(UnityAction<Actor> method)
    {
        if (method != null)
            OnInteraction.AddListener(method);
    }

    public void UnsubscribeToInteraction(UnityAction<Actor> method)
    {
        if (method != null)
            OnInteraction.RemoveListener(method);
    }

    public void TrackActor(Actor actor)
    {
        if (actor == null)
            return;

        interactingActor = actor;

        StartCoroutine(Track());
    }

    public void StopTrackingActor()
    {
        interactingActor = null;
    }

    private IEnumerator Track()
    {
        while (interactingActor != null && !interactingActor.IsInteracting)
        {
            float distance = Vector3.Distance(transform.position, interactingActor.transform.position);

            if (distance <= interactionRange)
            {
                Interact();
                EndInteraction();
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public void EndInteraction()
    {
        if (interactingActor != null)
            interactingActor.IsInteracting = false;
    }
}

// Unity is just probably laughing at all developers that have to do this...
public class UnityActorEvent : UnityEvent<Actor> { }