﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : Interactable
{
    private Material material;

    public Color HighlightColor = Color.white;
    public Color DefaultColor = Color.black;

    public GameScenes SceneToLoad;
    public LoadSceneMode LoadSceneMode;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
        material.color = DefaultColor;
    }

    // Use this for initialization
    void Start()
    {
        SubscribeToInteraction(OnInteract);
    }

    public void OnInteract(Actor actor)
    {
        SceneManager.LoadSceneAsync((int)SceneToLoad, LoadSceneMode);
    }

    public void OnMouseEnter()
    {
        material.color = HighlightColor;
    }

    public void OnMouseExit()
    {
        material.color = DefaultColor;
    }
}
